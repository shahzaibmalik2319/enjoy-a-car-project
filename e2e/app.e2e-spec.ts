import { EnjoyACarProjectPage } from './app.po';

describe('enjoy-a-car-project App', () => {
  let page: EnjoyACarProjectPage;

  beforeEach(() => {
    page = new EnjoyACarProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
