webpackJsonp([1,5],{

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__public_variables_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GetNotificationsCountService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//redux

var GetNotificationsCountService = (function () {
    function GetNotificationsCountService(userService, loader, variableService, _store) {
        this.userService = userService;
        this.loader = loader;
        this.variableService = variableService;
        this._store = _store;
    }
    GetNotificationsCountService.prototype.getNotificationsCount = function () {
        var _this = this;
        this.userService.getNotifications().subscribe(function (data) {
            _this.loader.stop();
            _this.variableService.notificationsCount = data.count;
            _this._store.dispatch({ type: 'GET_NOTIFICATIONS-SUCCESS', payload: data.data });
        }, function (error) {
            _this.loader.stop();
            _this._store.dispatch({ type: 'GET_NOTIFICATIONS-FAILED' });
            console.log(error);
        });
    };
    return GetNotificationsCountService;
}());
GetNotificationsCountService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__api_service__["a" /* ApiService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__loader_service__["a" /* LoaderService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__public_variables_service__["a" /* PublicVariablesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__public_variables_service__["a" /* PublicVariablesService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["b" /* Store */]) === "function" && _d || Object])
], GetNotificationsCountService);

var _a, _b, _c, _d;
//# sourceMappingURL=get-notifications-count.service.js.map

/***/ }),

/***/ 188:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(21);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthGraudService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthGraudService = (function () {
    function AuthGraudService(router) {
        this.router = router;
    }
    AuthGraudService.prototype.canActivate = function (route, state) {
        if (localStorage.getItem('searcherUserID')) {
            console.log('login');
            // logged in so return true
            return true;
        }
        console.log('not login');
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    };
    return AuthGraudService;
}());
AuthGraudService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object])
], AuthGraudService);

var _a;
//# sourceMappingURL=auth-graud.service.js.map

/***/ }),

/***/ 189:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Services_get_notifications_count_service__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__Services_public_variables_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_completer__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FindNewCarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








//redux

var FindNewCarComponent = (function () {
    function FindNewCarComponent(route, userService, loader, fb, variableService, _store, completerService, notifications) {
        this.route = route;
        this.userService = userService;
        this.loader = loader;
        this.fb = fb;
        this.variableService = variableService;
        this._store = _store;
        this.completerService = completerService;
        this.notifications = notifications;
        this.equipGourp = [];
        this.Features = [];
        this.discounts = [];
        this.showMoreOptions = false;
        this.hideBrands = true;
        this.addFeature = true;
        this.addFeature2 = false;
        this.features = false;
        this.searchbuttons = true;
        this.discountOptions = false;
        this.car = {};
        this.carData = {};
        this.newStatus = [];
        this.brands = [
            { name: 'Alfa Romeo' },
            { name: 'Alpine' },
            { name: 'Aston Martin' },
            { name: 'Audi' },
            { name: 'Bently' },
            { name: 'BMW' },
            { name: 'Bugatti' },
            { name: 'Ferrari' },
            { name: 'Fiat' },
            { name: 'Limborghini' },
            { name: 'Lincia' },
            { name: 'Land Rover' },
            { name: 'Meserati' },
            { name: 'McLaren' },
            { name: 'Mercedes-Benz' },
            { name: 'Mini' },
            { name: 'Opel' },
            { name: 'Peugeot' },
            { name: 'Porsche' },
            { name: 'Renault' },
            { name: 'Rolls-Royce' },
            { name: 'Vauxhall' },
            { name: 'Volkswagen' },
        ];
        this.form = this.fb.group({
            'zip_code': [this.car.zip_code || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'year': [this.car.year || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'brand': [this.car.brand || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'model': [this.car.model || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'time': [this.car.time || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'exterior': [this.car.exterior],
            'interior': [this.car.interior],
            'transmission': [this.car.transmission],
            'engine': [this.car.engine],
            'body_style': [this.car.body_style],
        });
        this._store.dispatch({ type: 'SEARCH-CAR_INIT' });
        this._store.dispatch({ type: 'GET_BRAND_NAME_INIT' });
        this._store.dispatch({ type: 'GET_BRAND_MODEL_INIT' });
        this._store.dispatch({ type: 'GET_BRAND_YEAR_INIT' });
    }
    FindNewCarComponent.prototype.moreOptions = function () {
        this.showMoreOptions = true;
        this.addFeature = false;
        this.hideBrands = false;
        this.addFeature2 = true;
    };
    FindNewCarComponent.prototype.addFeatures = function () {
        this.addFeature = false;
    };
    FindNewCarComponent.prototype.moreFeatures = function () {
        this.addFeature2 = false;
        this.features = true;
        this._store.dispatch({ type: 'SHOW_ALL_OPTIONS' });
    };
    FindNewCarComponent.prototype.hideFeatures = function () {
        this.features = false;
        this.showMoreOptions = false;
        this.hideBrands = true;
        this.addFeature = true;
        this._store.dispatch({ type: 'HIDE_OPTIONS' });
    };
    FindNewCarComponent.prototype.searchCar = function () {
        var _this = this;
        this.carData = {
            zip_code: this.car.zip_code,
            year: this.car.year,
            brand: this.car.brand,
            model: this.car.model,
            time: this.car.time,
            exterior: this.car.exterior,
            interior: this.car.interior,
            transmission: this.car.transmission,
            engine: this.car.engine,
            body_style: this.car.body_style,
        };
        var equips = [];
        var features = [];
        for (var i_1 = 0; i_1 < this.equipGourp.length; i_1++) {
            if (this.equipGourp[i_1].status) {
                var newItem = this.equipGourp[i_1].name;
                equips.push(newItem);
            }
        }
        for (var i_2 = 0; i_2 < this.Features.length; i_2++) {
            if (this.Features[i_2].status) {
                var newItem = this.Features[i_2].name;
                features.push(newItem);
            }
        }
        if (features.length !== 0) {
            this.carData.features = features;
        }
        if (equips.length !== 0) {
            this.carData.equipments = equips;
        }
        console.log("Car data", this.carData);
        var query = {};
        for (var i in this.carData) {
            if (this.carData[i] && this.carData[i].length > 0) {
                query[i] = this.carData[i];
            }
        }
        console.log("Query", query);
        this.userService.querySession(query).subscribe(function (data) {
            _this.loader.stop();
            console.log(data);
            _this.querySessionId = data.data;
            console.log(_this.querySessionId);
            _this.route.navigate(['/search-car', { cacheID: _this.querySessionId }]);
        }, function (error) {
            console.log(error);
            var err = JSON.parse(error._body);
            console.log(err);
            _this.loader.stop();
        });
    };
    FindNewCarComponent.prototype.showAllOptions = function () {
        this.features = true;
        this.showMoreOptions = true;
        this.discountOptions = true;
    };
    FindNewCarComponent.prototype.getBrandNames = function () {
        var _this = this;
        this.userService.getBrand().subscribe(function (data) {
            _this.loader.stop();
            _this.brandsList = data.data;
            _this.brandsList = _this.completerService.local(_this.brandsList, 'make', 'make');
            _this._store.dispatch({ type: 'GET_BRAND_NAME_SUCCESS', payload: _this.brandsList });
            //console.log(this.brandsList);
        }, function (error) {
            _this._store.dispatch({ type: 'GET_BRAND_NAME_FAILD' });
            _this.loader.stop();
            console.log(error);
        });
    };
    FindNewCarComponent.prototype.selectedBrand = function () {
        var _this = this;
        this.userService.getCarList(this.car.brand).subscribe(function (data) {
            _this.loader.stop();
            _this.modelList = data.data;
            _this.modelList = _this.completerService.local(_this.modelList, 'model', 'model');
            _this.car.model = "";
            _this._store.dispatch({ type: 'GET_BRAND_MODEL_SUCCESS', payload: _this.modelList });
            //console.log(this.modelList);
        }, function (error) {
            _this._store.dispatch({ type: 'GET_BRAND_MODEL_FAILD' });
            _this.loader.stop();
            console.log(error);
        });
    };
    FindNewCarComponent.prototype.selectedModel = function () {
        var _this = this;
        this.userService.getModelYearsList(this.car.model).subscribe(function (data) {
            _this.loader.stop();
            _this.modelYears = data.data;
            console.log(_this.modelYears);
            _this._store.dispatch({ type: 'GET_BRAND_YEAR_SUCCESS', payload: _this.modelYears });
        }, function (error) {
            _this._store.dispatch({ type: 'GET_BRAND_YEAR_FAILD' });
            _this.loader.stop();
            console.log(error);
        });
    };
    FindNewCarComponent.prototype.getBrand = function (brand) {
        var carName = { brandName: brand };
        console.log(carName);
        this.route.navigate(['search-car', carName]);
    };
    FindNewCarComponent.prototype.getFeatures = function () {
        var _this = this;
        this.userService.getFeaturesList().subscribe(function (data) {
            _this.loader.stop();
            _this.Features = data.data;
            _this._store.dispatch({ type: 'GET_FEATURES_SUCCESS', payload: data.data });
        }, function (error) {
            _this._store.dispatch({ type: 'GET_FEATURES_FAILD' });
            console.log(error);
            var err = JSON.parse(error._body);
            console.log(err);
            _this.loader.stop();
        });
    };
    FindNewCarComponent.prototype.getEquipments = function () {
        var _this = this;
        this.userService.getEquipmentsList().subscribe(function (data) {
            _this.loader.stop();
            _this.equipGourp = data.data;
            _this._store.dispatch({ type: 'GET_EQUIPMENTS_SUCCESS', payload: data.data });
        }, function (error) {
            _this._store.dispatch({ type: 'GET_EQUIPMENTS_FAILD' });
            console.log(error);
            var err = JSON.parse(error._body);
            console.log(err);
            _this.loader.stop();
        });
    };
    FindNewCarComponent.prototype.getOnTheTop = function () {
        window.scrollTo(0, 0);
    };
    FindNewCarComponent.prototype.ngOnInit = function () {
        this.getOnTheTop();
        this.notifications.getNotificationsCount();
        this.getBrandNames();
        this.getFeatures();
        this.getEquipments();
    };
    return FindNewCarComponent;
}());
FindNewCarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-find-new-car',
        template: __webpack_require__(509),
        styles: [__webpack_require__(489)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* FormBuilder */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_6__Services_public_variables_service__["a" /* PublicVariablesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__Services_public_variables_service__["a" /* PublicVariablesService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_8__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__ngrx_store__["b" /* Store */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_7_ng2_completer__["b" /* CompleterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7_ng2_completer__["b" /* CompleterService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_5__Services_get_notifications_count_service__["a" /* GetNotificationsCountService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__Services_get_notifications_count_service__["a" /* GetNotificationsCountService */]) === "function" && _h || Object])
], FindNewCarComponent);

var _a, _b, _c, _d, _e, _f, _g, _h;
//# sourceMappingURL=find-new-car.component.js.map

/***/ }),

/***/ 190:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgetpasswordComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//redux

var ForgetpasswordComponent = (function () {
    function ForgetpasswordComponent(fb, userService, loader, _store) {
        this.fb = fb;
        this.userService = userService;
        this.loader = loader;
        this._store = _store;
        this.user = {};
        this.form = this.fb.group({
            'email': [null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* Validators */].required],
        });
        this._store.dispatch({ type: 'FORGETPASSWORD_INIT' });
    }
    ForgetpasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        this.userData = {
            email: this.user.email,
        };
        console.log(this.user.email);
        this.userService.forgetPassword(this.userData).subscribe(function (data) {
            console.log(data);
            _this.Msg = data.message;
            _this._store.dispatch({ type: 'FORGETPASSWORD_SUCCESS', payload: data });
            _this.user.email = "";
            _this.loader.stop();
        }, function (error) {
            _this._store.dispatch({ type: 'FORGETPASSWORD_FAILD' });
            console.log(JSON.parse(error._body));
            var msg = JSON.parse(error._body);
            _this.Msg = msg.message;
            _this.user.email = "";
            _this.loader.stop();
        });
    };
    ForgetpasswordComponent.prototype.ngOnInit = function () {
    };
    return ForgetpasswordComponent;
}());
ForgetpasswordComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-forgetpassword',
        template: __webpack_require__(511),
        styles: [__webpack_require__(491)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__Services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__Services_api_service__["a" /* ApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__Services_loader_service__["a" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["b" /* Store */]) === "function" && _d || Object])
], ForgetpasswordComponent);

var _a, _b, _c, _d;
//# sourceMappingURL=forgetpassword.component.js.map

/***/ }),

/***/ 191:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Services_public_variables_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//redux

var LoginComponent = (function () {
    function LoginComponent(route, userService, loader, fb, variableService, _store) {
        this.route = route;
        this.userService = userService;
        this.loader = loader;
        this.fb = fb;
        this.variableService = variableService;
        this._store = _store;
        this.user = {};
        this.form = this.fb.group({
            'email': [this.user.email || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'password': [this.user.password || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required]
        });
        this.form.patchValue({
            email: this.user.email,
            password: this.user.password
        });
        this._store.dispatch({ type: 'LOGIN_INITIALS' });
    }
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.userData = {
            email: this.user.email,
            password: this.user.password,
            role: "buyers",
        };
        this.userService.doLogin(this.userData).subscribe(function (data) {
            _this.loader.stop();
            localStorage.setItem('searcherUserID', data.user.token_id);
            localStorage.setItem('SearcherUser', JSON.stringify(data.user));
            _this.variableService.isLogin = true;
            _this.route.navigate(['']);
            _this._store.dispatch({ type: 'LOGIN_SUCCESS', payload: data });
        }, function (error) {
            console.log(error);
            var err = JSON.parse(error._body);
            console.log(err);
            _this._store.dispatch({ type: 'LOGIN_FAILD' });
            _this.Msg = err.message;
            _this.loader.stop();
        });
    };
    LoginComponent.prototype.isEmailValid = function () {
        var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
        return EMAIL_REGEXP.test(this.user.email);
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(513),
        styles: [__webpack_require__(493)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* FormBuilder */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__Services_public_variables_service__["a" /* PublicVariablesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__Services_public_variables_service__["a" /* PublicVariablesService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_6__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__ngrx_store__["b" /* Store */]) === "function" && _f || Object])
], LoginComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=login.component.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_public_variables_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ng2_completer__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__Services_get_notifications_count_service__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificationsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









//redux

var NotificationsComponent = (function () {
    function NotificationsComponent(route, userService, loader, variableService, fb, connectFb, completerService, routeToNavigate, notifications, _store) {
        this.route = route;
        this.userService = userService;
        this.loader = loader;
        this.variableService = variableService;
        this.fb = fb;
        this.connectFb = connectFb;
        this.completerService = completerService;
        this.routeToNavigate = routeToNavigate;
        this.notifications = notifications;
        this._store = _store;
        //pagination
        this.totalPages = [];
        this.currentPage = 1;
    }
    NotificationsComponent.prototype.allNotifications = function () {
        var _this = this;
        this.userService.getNotifications().subscribe(function (data) {
            _this.loader.stop();
            _this.notificationList = data.notification;
            _this.totalNotifications = _this.notificationList.length;
            _this.totalServepage = data.totalPages;
            for (var i = 1; i <= data.totalPages; i++) {
                var page = { page: i };
                _this.totalPages.push(page);
            }
            _this.serverCount = data.count;
            console.log(data);
            _this._store.dispatch({ type: 'GET_NOTIFICATIONS-SUCCESS', payload: data.data });
        }, function (error) {
            _this.loader.stop();
            _this._store.dispatch({ type: 'GET_NOTIFICATIONS-FAILED' });
            console.log(error);
        });
    };
    NotificationsComponent.prototype.getPage = function (pageNum) {
        var _this = this;
        this.currentPage = pageNum;
        this.userService.getNotificationsPage(pageNum).subscribe(function (data) {
            _this.loader.stop();
            _this.notificationList = data.notification;
            _this.totalNotifications = _this.notificationList.length;
            _this.totalServepage = data.totalPages;
            _this.serverCount = data.count;
            _this.currentPage = data.page;
            _this._store.dispatch({ type: 'GET_PAGE', payload: data });
            _this.getOnTheTop();
            console.log(data);
        }, function (error) {
            _this.loader.stop();
            console.log(error);
        });
    };
    NotificationsComponent.prototype.nextPage = function () {
        var _this = this;
        this.currentPage += 1;
        this.userService.getNotificationsPage(this.currentPage).subscribe(function (data) {
            _this.loader.stop();
            _this.notificationList = data.notification;
            _this.totalNotifications = _this.notificationList.length;
            _this.totalServepage = data.totalPages;
            _this.serverCount = data.count;
            _this._store.dispatch({ type: 'NEXT_PAGE', payload: data });
            _this.getOnTheTop();
            console.log(data);
        }, function (error) {
            _this.loader.stop();
            console.log(error);
        });
    };
    NotificationsComponent.prototype.previousPage = function () {
        var _this = this;
        this.currentPage -= 1;
        this.userService.getNotificationsPage(this.currentPage).subscribe(function (data) {
            _this.loader.stop();
            _this.notificationList = data.notification;
            _this.totalNotifications = _this.notificationList.length;
            _this.totalServepage = data.totalPages;
            _this.serverCount = data.count;
            _this._store.dispatch({ type: 'PREVIOUS_PAGE', payload: data });
            _this.getOnTheTop();
            console.log(data);
        }, function (error) {
            _this.loader.stop();
            console.log(error);
        });
    };
    NotificationsComponent.prototype.active = function (page) {
        if (this.currentPage === page) {
            return true;
        }
    };
    NotificationsComponent.prototype.allRead = function () {
        var _this = this;
        this.userService.readNotifications().subscribe(function (data) {
            _this.loader.stop();
            console.log(data);
            _this.totalPages = [];
            _this.ngOnInit();
        }, function (error) {
            _this.loader.stop();
            console.log(error);
        });
    };
    NotificationsComponent.prototype.getOnTheTop = function () {
        console.log('top');
        window.scrollTo(0, 0);
    };
    NotificationsComponent.prototype.ngOnInit = function () {
        this.allNotifications();
        this.notifications.getNotificationsCount();
        this.getOnTheTop();
    };
    return NotificationsComponent;
}());
NotificationsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-notifications',
        template: __webpack_require__(516),
        styles: [__webpack_require__(496)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__Services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__Services_api_service__["a" /* ApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__Services_loader_service__["a" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__Services_public_variables_service__["a" /* PublicVariablesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__Services_public_variables_service__["a" /* PublicVariablesService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["g" /* FormBuilder */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__angular_forms__["g" /* FormBuilder */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_6_ng2_completer__["b" /* CompleterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6_ng2_completer__["b" /* CompleterService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_7__Services_get_notifications_count_service__["a" /* GetNotificationsCountService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7__Services_get_notifications_count_service__["a" /* GetNotificationsCountService */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_8__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__ngrx_store__["b" /* Store */]) === "function" && _k || Object])
], NotificationsComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
//# sourceMappingURL=notifications.component.js.map

/***/ }),

/***/ 193:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrivacyPolicyComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrivacyPolicyComponent = (function () {
    function PrivacyPolicyComponent() {
    }
    PrivacyPolicyComponent.prototype.getOnTheTop = function () {
        window.scrollTo(0, 0);
    };
    PrivacyPolicyComponent.prototype.ngOnInit = function () {
        this.getOnTheTop();
    };
    return PrivacyPolicyComponent;
}());
PrivacyPolicyComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-privacy-policy',
        template: __webpack_require__(517),
        styles: [__webpack_require__(497)]
    }),
    __metadata("design:paramtypes", [])
], PrivacyPolicyComponent);

//# sourceMappingURL=privacy-policy.component.js.map

/***/ }),

/***/ 194:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Services_public_variables_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RedirectingComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//redux

var RedirectingComponent = (function () {
    function RedirectingComponent(variableService, userService, routerToNavigate, route, loader, _store) {
        this.variableService = variableService;
        this.userService = userService;
        this.routerToNavigate = routerToNavigate;
        this.route = route;
        this.loader = loader;
        this._store = _store;
        this.variableService.navBar = false;
        this._store.dispatch({ type: 'CONFIRMATION_INIT' });
    }
    RedirectingComponent.prototype.subscribe = function () {
        var _this = this;
        this.sub = this.route
            .params
            .subscribe(function (params) {
            _this.token = params['token'];
            console.log(_this.token);
        });
    };
    RedirectingComponent.prototype.ngOnInit = function () {
        this.subscribe();
        var self = this;
        setTimeout(function () {
            var _this = this;
            self.userService.confirmation(self.token).subscribe(function (data) {
                console.log(data);
                localStorage.setItem('searcherUserID', data.user.token_id);
                localStorage.setItem('SearcherUser', JSON.stringify(data.user));
                self.loader.stop();
                self.variableService.currentUser = data.user;
                self.variableService.navBar = true;
                self.variableService.isLogin = true;
                self._store.dispatch({ type: 'CONFIRMATION_SUCCESS', payload: data });
                self.routerToNavigate.navigate(['']);
            }, function (error) {
                _this._store.dispatch({ type: 'CONFIRMATION_FAILD' });
                self.loader.stop();
                console.log(error);
            });
        }, 3000);
    };
    RedirectingComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    return RedirectingComponent;
}());
RedirectingComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-redirecting',
        template: __webpack_require__(518),
        styles: [__webpack_require__(498)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__Services_public_variables_service__["a" /* PublicVariablesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__Services_public_variables_service__["a" /* PublicVariablesService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__Services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__Services_api_service__["a" /* ApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */]) === "function" && _f || Object])
], RedirectingComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=redirecting.component.js.map

/***/ }),

/***/ 195:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ResetPasswordComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//redux

var ResetPasswordComponent = (function () {
    function ResetPasswordComponent(fb, userService, loader, route, router, _store) {
        this.fb = fb;
        this.userService = userService;
        this.loader = loader;
        this.route = route;
        this.router = router;
        this._store = _store;
        this.newPasswordObj = {};
        this.passwordObj = {};
        this._store.dispatch({ type: 'RESETPASSWORD_INITIALS' });
    }
    ResetPasswordComponent.prototype.subscribe = function () {
        var _this = this;
        this.sub = this.route
            .params
            .subscribe(function (params) {
            _this.token = params['token'];
            console.log(params);
            console.log(_this.token);
        });
    };
    ResetPasswordComponent.prototype.onChange = function () {
        var _this = this;
        this.passwordObj = {
            newPassword: this.newPasswordObj.pass1,
            verifyPassword: this.newPasswordObj.pass2
        };
        this.userService.restPassword(this.token, this.passwordObj).subscribe(function (data) {
            _this.Msg = data.message;
            _this._store.dispatch({ type: 'RESETPASSWORD_SUCCESS', payload: data });
            _this.router.navigate[('/login')];
        }, function (error) {
            _this._store.dispatch({ type: 'FORGETPASSWORD_FAILD' });
            _this.Msg = error.message;
        });
    };
    ResetPasswordComponent.prototype.ngOnInit = function () {
        this.subscribe();
    };
    ResetPasswordComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    return ResetPasswordComponent;
}());
ResetPasswordComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-reset-password',
        template: __webpack_require__(519),
        styles: [__webpack_require__(499)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* FormBuilder */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__Services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__Services_api_service__["a" /* ApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__Services_loader_service__["a" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["c" /* ActivatedRoute */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* Router */]) === "function" && _e || Object, typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */]) === "function" && _f || Object])
], ResetPasswordComponent);

var _a, _b, _c, _d, _e, _f;
//# sourceMappingURL=reset-password.component.js.map

/***/ }),

/***/ 196:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__Services_public_variables_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_forms__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ng2_completer__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SearchCarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









//redux

var SearchCarComponent = (function () {
    function SearchCarComponent(route, userService, loader, variableService, fb, connectFb, requestFb, completerService, routeToNavigate, _store) {
        this.route = route;
        this.userService = userService;
        this.loader = loader;
        this.variableService = variableService;
        this.fb = fb;
        this.connectFb = connectFb;
        this.requestFb = requestFb;
        this.completerService = completerService;
        this.routeToNavigate = routeToNavigate;
        this._store = _store;
        this.connectFormData = {};
        this.connectData = {};
        this.showMoreOptions = false;
        this.hideBrands = true;
        this.addFeature = true;
        this.addFeature2 = false;
        this.features = false;
        this.searchbuttons = true;
        this.carNameAndModel = "FORD MUSTANG - 78223";
        this.discountOptions = false;
        this.searchQuery = {};
        this.searchResult = [{}];
        this.matchRadioBox = false;
        this.currentYear = new Date().getFullYear() - 1;
        this.expireYears = [];
        this.expire = {};
        this.isDataLoaded = false;
        this.carEquipGourp = [];
        this.carFeatures = [];
        this.carDiscounts = [];
        this.requestModalForm = {};
        this.requetuserName = 'Guest';
        this.SeacherData = {};
        //pagination
        this.totalPages = [];
        this.currentPage = 1;
        this.form = this.connectFb.group({
            'nameOnCard': [this.connectFormData.nameOnCard || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'card': [this.connectFormData.card || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'email': [this.connectFormData.email || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'card_number': [this.connectFormData.card_number || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'phone': [this.connectFormData.phone || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'expire': [this.connectFormData.expire || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'pin_code': [this.connectFormData.pin_code || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'name': [this.connectFormData.name || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'postal_code': [this.connectFormData.postal_code || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'zip_code': [this.connectFormData.zip_code || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'month': [this.connectFormData.month || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'year': [this.connectFormData.year || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
            'reqEmail': [this.requestModalForm.email || null, __WEBPACK_IMPORTED_MODULE_6__angular_forms__["h" /* Validators */].required],
        });
        this.newcurrentYear = this.currentYear.toString();
        this.newcurrentYear = this.newcurrentYear.slice(2, 4);
        for (var i = 0; i < 14; i++) {
            this.newcurrentYear = ++this.newcurrentYear;
            this.expireYears.push(this.newcurrentYear);
        }
        this._store.dispatch({ type: 'CONNECT_CAR_INT' });
        this.emailError = false;
        this.NameError = false;
        this.phoneError = false;
        this.cvcError = false;
        this.cardNumError = false;
    }
    SearchCarComponent.prototype.connectPayBtton = function (id) {
        var _this = this;
        if (localStorage.getItem('searcherUserID')) {
            this.connectData = this.connectFormData;
            this.connectData.car = this.carId;
            this.connectData.dealer = this.dealerId;
            this.connectData.expire = this.expire;
            console.log(this.connectData);
            this.userService.payConnect(this.connectData).subscribe(function (data) {
                _this.loader.stop();
                _this.modal.close().then(function () {
                    _this.successModal.open();
                });
                console.log(data);
                _this._store.dispatch({ type: 'GET_STRIP_FORM_DATA_SUCCESS', payload: data.data });
            }, function (error) {
                _this._store.dispatch({ type: 'GET_STRIP_FORM_DATA_FAILD' });
                _this.loader.stop();
                console.log(error);
            });
        }
        else {
            this.connectData = this.connectFormData;
            this.connectData.expire = this.expire;
            this.connectData.car = this.carId;
            this.connectData.dealer = this.dealerId;
            console.log(this.connectData);
            this.userService.guest(this.connectData).subscribe(function (data) {
                _this.loader.stop();
                _this.modal.close().then(function () {
                    _this.successModal.open();
                });
                console.log(data);
                _this._store.dispatch({ type: 'GET_STRIP_FORM_DATA_SUCCESS', payload: data.data });
            }, function (error) {
                _this._store.dispatch({ type: 'GET_STRIP_FORM_DATA_FAILD' });
                _this.loader.stop();
                console.log(error);
            });
        }
    };
    SearchCarComponent.prototype.moreOptions = function () {
        this.showMoreOptions = true;
        this.addFeature = false;
        this.hideBrands = false;
        this.addFeature2 = true;
    };
    SearchCarComponent.prototype.addFeatures = function () {
        this.addFeature = false;
    };
    SearchCarComponent.prototype.moreFeatures = function () {
        this.addFeature2 = false;
        this.features = true;
        this._store.dispatch({ type: 'SHOW_ALL_OPTIONS' });
    };
    SearchCarComponent.prototype.hideFeatures = function () {
        this.features = false;
        this.showMoreOptions = false;
        this.hideBrands = true;
        this.addFeature = true;
        this._store.dispatch({ type: 'HIDE_OPTIONS' });
    };
    SearchCarComponent.prototype.showAllOptions = function () {
        this.features = true;
        this.showMoreOptions = true;
        this.discountOptions = true;
    };
    SearchCarComponent.prototype.showDetails = function (index) {
        this.listOfCarSearch[index].isCollapse = false;
    };
    SearchCarComponent.prototype.cancelConnect = function (index) {
        console.log(index);
        this.listOfCarSearch[index].isCollapse = true;
    };
    SearchCarComponent.prototype.openPaypal = function (id, dealer) {
        var _this = this;
        this.userService.recentSearch(id).subscribe(function (data) {
            _this.loader.stop();
            _this.carId = id;
            _this.dealerId = dealer;
            _this.modal.open();
            console.log(data);
            _this._store.dispatch({ type: 'CONNECT_CAR_SUCCESS', payload: data.data });
        }, function (error) {
            _this._store.dispatch({ type: 'CONNECT_CAR_SUCCESS' });
            console.log(error);
            _this.loader.stop();
            var err = JSON.parse(error._body);
            console.log(err);
        });
    };
    SearchCarComponent.prototype.getSearchData = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            if (params['brandName']) {
                _this.searchQuery.brand = params['brandName'];
                console.log("Brand Name", _this.searchQuery);
                _this.getSearchCars();
            }
            else if (params['cacheID']) {
                _this.cacheID = params['cacheID'];
                console.log(_this.cacheID);
                _this.userService.getQuerySession(_this.cacheID).subscribe(function (data) {
                    _this.loader.stop();
                    _this.searchQuery = data.data;
                    console.log("Search Data", _this.searchQuery);
                    _this.getSearchCars();
                }, function (error) {
                    _this.loader.stop();
                    console.log(error);
                });
            }
        });
    };
    SearchCarComponent.prototype.getSearchCars = function () {
        var _this = this;
        console.log("Serach Query", this.searchQuery);
        this.userService.searchCar(this.searchQuery).subscribe(function (_data) {
            //console.log("search car", _data);
            _this.loader.stop();
            _this.totalServepage = _data.totalPages;
            _this.serverCount = _data.count;
            _this.listOfCarSearch = _data.data;
            _this.totalRecentSearch = _this.listOfCarSearch.length;
            for (var i = 1; i <= _data.totalPages; i++) {
                var page = { page: i };
                _this.totalPages.push(page);
            }
            for (var i = 0; i < _this.listOfCarSearch.length; i++) {
                _this.listOfCarSearch[i].isCollapse = true;
            }
            _this.isDataLoaded = true;
            _this._store.dispatch({ type: 'SEARCH-CAR_SUCCESS', payload: _data });
        }, function (error) {
            _this._store.dispatch({ type: 'SEARCH-CAR_FAILD' });
            console.log(error);
            var err = JSON.parse(error._body);
            console.log(err);
            _this.loader.stop();
        });
    };
    SearchCarComponent.prototype.getBrandNames = function () {
        var _this = this;
        this.userService.getBrand().subscribe(function (data) {
            _this.loader.stop();
            _this.brandsList = data.data;
            _this.brandsList = _this.completerService.local(_this.brandsList, 'make', 'make');
            console.log(_this.brandsList);
            _this.isDataLoaded = true;
            _this._store.dispatch({ type: 'GET_BRAND_NAME_SUCCESS', payload: _this.brandsList });
        }, function (error) {
            _this._store.dispatch({ type: 'GET_BRAND_NAME_FAILD' });
            _this.loader.stop();
            console.log(error);
        });
    };
    SearchCarComponent.prototype.selectedBrand = function () {
        var _this = this;
        this.userService.getCarList(this.searchQuery.brand).subscribe(function (data) {
            _this.loader.stop();
            _this.modelList = data.data;
            _this.modelList = _this.completerService.local(_this.modelList, 'model', 'model');
            _this.searchQuery.model = "";
            console.log(_this.modelList);
            _this.isDataLoaded = true;
            _this._store.dispatch({ type: 'GET_BRAND_MODEL_SUCCESS', payload: _this.modelList });
        }, function (error) {
            _this._store.dispatch({ type: 'GET_BRAND_MODEL_FAILD' });
            _this.loader.stop();
            console.log(error);
        });
    };
    SearchCarComponent.prototype.selectedModel = function () {
        var _this = this;
        this.userService.getModelYearsList(this.searchQuery.model).subscribe(function (data) {
            _this.loader.stop();
            _this.modelYears = data.data;
            console.log(_this.modelYears);
            _this._store.dispatch({ type: 'GET_BRAND_YEAR_SUCCESS', payload: _this.modelYears });
        }, function (error) {
            _this._store.dispatch({ type: 'GET_BRAND_YEAR_FAILD' });
            _this.loader.stop();
            console.log(error);
        });
    };
    SearchCarComponent.prototype.carSearchUpdate = function () {
        var _this = this;
        var equips = [];
        var features = [];
        var discounts = [];
        for (var i = 0; i < this.carEquipGourp.length; i++) {
            if (this.carEquipGourp[i].status) {
                var newItem = this.carEquipGourp[i].name;
                equips.push(newItem);
            }
        }
        for (var i = 0; i < this.carFeatures.length; i++) {
            if (this.carFeatures[i].status) {
                var newItem = this.carFeatures[i].name;
                features.push(newItem);
            }
        }
        for (var i = 0; i < this.carDiscounts.length; i++) {
            if (this.carDiscounts[i].status) {
                var newItem = this.carDiscounts[i].name;
                discounts.push(newItem);
            }
        }
        if (features.length !== 0) {
            this.searchQuery.features = features;
        }
        if (equips.length !== 0) {
            this.searchQuery.equipments = equips;
        }
        if (discounts.length !== 0) {
            this.searchQuery.discounts = discounts;
        }
        console.log(this.searchQuery);
        this.userService.searchCarUpdate(this.searchQuery).subscribe(function (data) {
            _this.loader.stop();
            _this.totalServepage = data.totalPages;
            _this.serverCount = data.count;
            _this.listOfCarSearch = data.data;
            _this.totalRecentSearch = _this.listOfCarSearch.length;
            _this.totalPages = [];
            for (var i = 1; i <= data.totalPages; i++) {
                var page = { page: i };
                _this.totalPages.push(page);
            }
            for (var i = 0; i < _this.listOfCarSearch.length; i++) {
                _this.listOfCarSearch[i].isCollapse = true;
            }
            console.log(_this.totalRecentSearch);
            console.log(data);
            console.log('TotalPages', _this.totalPages);
            _this._store.dispatch({ type: 'SEARCH-CAR_UPDATE_SUCCESS', payload: data.data });
        }, function (error) {
            _this._store.dispatch({ type: 'SEARCH-CAR_UPDATE_FAILD' });
            console.log(error);
            var err = JSON.parse(error._body);
            console.log(err);
            _this.loader.stop();
        });
    };
    SearchCarComponent.prototype.isEmailValid = function () {
        var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
        if (EMAIL_REGEXP.test(this.connectFormData.email)) {
            this.emailError = false;
            return true;
        }
        else {
            this.emailError = true;
            return false;
        }
    };
    SearchCarComponent.prototype.isEmailRequestValid = function () {
        var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
        if (EMAIL_REGEXP.test(this.requestModalForm.email)) {
            return true;
        }
        else {
            return false;
        }
    };
    SearchCarComponent.prototype.isCardNumValid = function () {
        var card_type = this.connectFormData.card;
        var isVisaValid = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/.test(this.connectFormData.card_number);
        var isMasterValid = /^(?:5[1-5][0-9]{14})$/.test(this.connectFormData.card_number);
        if (card_type == "Master") {
            if (isMasterValid) {
                this.cardNumError = false;
                return true;
            }
            else {
                this.cardNumError = true;
                return false;
            }
        }
        if (card_type == "Visa") {
            if (isVisaValid) {
                this.cardNumError = false;
                return true;
            }
            else {
                this.cardNumError = true;
                return false;
            }
        }
    };
    SearchCarComponent.prototype.isNameValid = function () {
        var NAME_REGEXP = new RegExp(/^[a-z]([-']?[a-z]+)*( [a-z]([-']?[a-z]+)*)+$/);
        if (NAME_REGEXP.test(this.connectFormData.name)) {
            this.NameError = false;
            return true;
        }
        else {
            this.NameError = true;
            return false;
        }
    };
    SearchCarComponent.prototype.isPhoneNumValid = function () {
        var isValid = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(this.connectFormData.phone);
        if (isValid) {
            this.phoneError = false;
            return true;
        }
        else {
            this.phoneError = true;
            return false;
        }
    };
    SearchCarComponent.prototype.isCVCValid = function () {
        var isValid = /^[1-9][0-9]{2}$/.test(this.connectFormData.pin_code);
        if (isValid) {
            this.cvcError = false;
            return true;
        }
        else {
            this.cvcError = true;
            return false;
        }
    };
    SearchCarComponent.prototype.getFeatures = function () {
        var _this = this;
        this.userService.getFeaturesList().subscribe(function (data) {
            _this.loader.stop();
            _this.carFeatures = data.data;
            _this._store.dispatch({ type: 'GET_FEATURES_SUCCESS', payload: data.data });
            console.log(_this.searchQuery.features);
            console.log(_this.carFeatures);
            for (var i = 0; i < _this.carFeatures.length; i++) {
                if (_this.searchQuery.features[i] === _this.carFeatures[i].name) {
                    _this.carFeatures[i].status = true;
                    console.log(_this.carFeatures[i].name + "true");
                }
            }
            console.log(_this.carFeatures);
        }, function (error) {
            console.log(error);
            _this._store.dispatch({ type: 'GET_FEATURES_FAILD' });
            var err = JSON.parse(error._body);
            console.log(err);
            _this.loader.stop();
        });
    };
    SearchCarComponent.prototype.getEquipments = function () {
        var _this = this;
        this.userService.getEquipmentsList().subscribe(function (data) {
            _this.loader.stop();
            _this.carEquipGourp = data.data;
            console.log(data);
            for (var i = 0; i < _this.carEquipGourp.length; i++) {
                if (_this.searchQuery.equipments[i] === _this.carEquipGourp[i].name) {
                    _this.carEquipGourp[i].status = true;
                }
            }
            _this._store.dispatch({ type: 'GET_EQUIPMENTS_SUCCESS', payload: data.data });
        }, function (error) {
            _this._store.dispatch({ type: 'GET_EQUIPMENTS_FAILD' });
            console.log(error);
            var err = JSON.parse(error._body);
            console.log(err);
            _this.loader.stop();
        });
    };
    SearchCarComponent.prototype.getDiscounts = function () {
        var _this = this;
        this.userService.getDiscountsList().subscribe(function (data) {
            _this.loader.stop();
            _this.carDiscounts = data.data;
            console.log(data);
            _this._store.dispatch({ type: 'GET_DISCOUNTS_SUCCESS', payload: data.data });
        }, function (error) {
            _this._store.dispatch({ type: 'GET_DISCOUNTS_FAILD' });
            console.log(error);
            var err = JSON.parse(error._body);
            console.log(err);
            _this.loader.stop();
        });
    };
    SearchCarComponent.prototype.getPage = function (pageNum) {
        var _this = this;
        this.currentPage = pageNum;
        this.userService.getMyRecentCarsPage(pageNum).subscribe(function (data) {
            _this.loader.stop();
            _this.listOfCarSearch = data.data;
            _this.totalRecentSearch = _this.listOfCarSearch.length;
            for (var i = 0; i < _this.listOfCarSearch.length; i++) {
                _this.listOfCarSearch[i].isCollapse = true;
            }
            console.log(data);
            _this._store.dispatch({ type: 'GET_PAGE', payload: data });
        }, function (error) {
            _this.loader.stop();
            console.log(error);
        });
    };
    SearchCarComponent.prototype.nextPage = function () {
        var _this = this;
        this.currentPage += 1;
        this.userService.getMyRecentCarsPage(this.currentPage).subscribe(function (data) {
            _this.loader.stop();
            _this.listOfCarSearch = data.data;
            _this.totalRecentSearch = _this.listOfCarSearch.length;
            for (var i = 0; i < _this.listOfCarSearch.length; i++) {
                _this.listOfCarSearch[i].isCollapse = true;
            }
            console.log(data);
            _this._store.dispatch({ type: 'NEXT_PAGE', payload: data });
        }, function (error) {
            _this.loader.stop();
            console.log(error);
        });
    };
    SearchCarComponent.prototype.previousPage = function () {
        var _this = this;
        this.currentPage -= 1;
        this.userService.getMyRecentCarsPage(this.currentPage).subscribe(function (data) {
            _this.loader.stop();
            _this.listOfCarSearch = data.data;
            _this.totalRecentSearch = _this.listOfCarSearch.length;
            for (var i = 0; i < _this.listOfCarSearch.length; i++) {
                _this.listOfCarSearch[i].isCollapse = true;
            }
            console.log(data);
            _this._store.dispatch({ type: 'PREVIOUS_PAGE', payload: data });
        }, function (error) {
            _this.loader.stop();
            console.log(error);
        });
    };
    SearchCarComponent.prototype.active = function (page) {
        if (this.currentPage === page) {
            return true;
        }
    };
    SearchCarComponent.prototype.reqOfferModal = function () {
        var _this = this;
        if (localStorage.getItem('searcherUserID')) {
            this.SeacherData = localStorage.getItem('SearcherUser');
            this.SeacherData = JSON.parse(this.SeacherData);
            var name = this.SeacherData.firstName + " " + this.SeacherData.lastName;
            this.userService.userSubcribes(this.requestModalForm.email, name).subscribe(function (data) {
                _this.loader.stop();
                _this.reqModal.close().then(function () {
                    _this.successRequest.open();
                });
                console.log(data);
                _this.requestModalForm = {};
            }, function (error) {
                console.log(error);
                _this.loader.stop();
                var err = JSON.parse(error._body);
                if (err.error.code === 11000) {
                    _this.reqModal.close().then(function () {
                        _this.alreadySubscribeModal.open();
                    });
                }
            });
        }
        else {
            this.userService.userSubcribes(this.requestModalForm.email, this.requetuserName).subscribe(function (data) {
                _this.loader.stop();
                _this.reqModal.close().then(function () {
                    _this.successRequest.open();
                });
                console.log(data);
                _this.requestModalForm = {};
            }, function (error) {
                console.log(error);
                _this.loader.stop();
                var err = JSON.parse(error._body);
                if (err.error.code === 11000) {
                    _this.reqModal.close().then(function () {
                        _this.alreadySubscribeModal.open();
                    });
                }
            });
        }
    };
    SearchCarComponent.prototype.getOnTheTop = function () {
        window.scrollTo(0, 0);
    };
    SearchCarComponent.prototype.ngOnInit = function () {
        this.getOnTheTop();
        this.getSearchData();
        this.getBrandNames();
        this.getFeatures();
        this.getDiscounts();
        this.getEquipments();
    };
    SearchCarComponent.prototype.ngOnDestroy = function () {
    };
    return SearchCarComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('connectModal'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"]) === "function" && _a || Object)
], SearchCarComponent.prototype, "modal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('success'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"]) === "function" && _b || Object)
], SearchCarComponent.prototype, "successModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('alreadySubscribeModal'),
    __metadata("design:type", typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"]) === "function" && _c || Object)
], SearchCarComponent.prototype, "alreadySubscribeModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('reqModal'),
    __metadata("design:type", typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"]) === "function" && _d || Object)
], SearchCarComponent.prototype, "reqModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('successRequest'),
    __metadata("design:type", typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"]) === "function" && _e || Object)
], SearchCarComponent.prototype, "successRequest", void 0);
SearchCarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-search-car',
        template: __webpack_require__(520),
        styles: [__webpack_require__(500)]
    }),
    __metadata("design:paramtypes", [typeof (_f = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["c" /* ActivatedRoute */]) === "function" && _f || Object, typeof (_g = typeof __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */]) === "function" && _g || Object, typeof (_h = typeof __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */]) === "function" && _h || Object, typeof (_j = typeof __WEBPACK_IMPORTED_MODULE_5__Services_public_variables_service__["a" /* PublicVariablesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__Services_public_variables_service__["a" /* PublicVariablesService */]) === "function" && _j || Object, typeof (_k = typeof __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* FormBuilder */]) === "function" && _k || Object, typeof (_l = typeof __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* FormBuilder */]) === "function" && _l || Object, typeof (_m = typeof __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_6__angular_forms__["g" /* FormBuilder */]) === "function" && _m || Object, typeof (_o = typeof __WEBPACK_IMPORTED_MODULE_7_ng2_completer__["b" /* CompleterService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_7_ng2_completer__["b" /* CompleterService */]) === "function" && _o || Object, typeof (_p = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _p || Object, typeof (_q = typeof __WEBPACK_IMPORTED_MODULE_8__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_8__ngrx_store__["b" /* Store */]) === "function" && _q || Object])
], SearchCarComponent);

var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q;
//# sourceMappingURL=search-car.component.js.map

/***/ }),

/***/ 197:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





//redux

var SignupComponent = (function () {
    function SignupComponent(route, userService, loader, fb, _store) {
        this.route = route;
        this.userService = userService;
        this.loader = loader;
        this.fb = fb;
        this._store = _store;
        this.user = {};
        this.userData = {};
        this.form = this.fb.group({
            'firstname': [this.user.firstname || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'lastname': [this.user.lastname || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'email': [this.user.email || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'age': [this.user.age || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'gender': [this.user.gender || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'password': [this.user.password || null, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["h" /* Validators */].required],
            'zip_code': [this.user.zip_code || null],
        });
        this._store.dispatch({ type: 'SIGNUP_INIT' });
        this.zipError = false;
        this.FirstNameError = false;
        this.LastNameError = false;
        this.AgeError = false;
    }
    SignupComponent.prototype.onSubmit = function () {
        var _this = this;
        this.userData = {
            firstName: this.user.firstname,
            lastName: this.user.lastname,
            email: this.user.email,
            age: this.user.age,
            gender: this.user.gender,
            password: this.user.password,
            role: 'buyers'
        };
        this.userService.doSignUp(this.userData)
            .subscribe(function (data) {
            _this.loader.stop();
            console.log(data);
            _this.Msg = data.message;
            _this._store.dispatch({ type: 'SIGNUP_SUCCESS', payload: data });
            setTimeout(function () {
                _this.route.navigate(['/login']);
            }, 2000);
        }, function (error) {
            console.log(error);
            _this._store.dispatch({ type: 'SIGNUP_FAILD' });
            var err = JSON.parse(error._body);
            console.log(err);
            _this.Msg = err.error;
            _this.loader.stop();
        });
    };
    SignupComponent.prototype.isEmailValid = function () {
        var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
        return EMAIL_REGEXP.test(this.user.email);
    };
    SignupComponent.prototype.isNameValid = function () {
        var NAME_REGEXP = new RegExp(/^[A-Za-z]+$/);
        if (NAME_REGEXP.test(this.user.firstname)) {
            this.FirstNameError = false;
        }
        else {
            this.FirstNameError = true;
        }
    };
    SignupComponent.prototype.isLastNameValid = function () {
        var NAME_REGEXP = new RegExp(/^[A-Za-z]+$/);
        if (NAME_REGEXP.test(this.user.lastname)) {
            this.LastNameError = false;
        }
        else {
            this.LastNameError = true;
        }
    };
    SignupComponent.prototype.isAgeValid = function () {
        var NAME_REGEXP = new RegExp(/^\d+$/);
        if (NAME_REGEXP.test(this.user.age)) {
            this.AgeError = false;
        }
        else {
            this.AgeError = true;
        }
    };
    SignupComponent.prototype.isZipCodeValid = function () {
        var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(this.user.zip_code);
        if (isValid) {
            this.zipError = false;
        }
        else {
            this.zipError = true;
        }
    };
    SignupComponent.prototype.ngOnInit = function () {
    };
    return SignupComponent;
}());
SignupComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-signup',
        template: __webpack_require__(521),
        styles: [__webpack_require__(501)],
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* FormBuilder */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_5__ngrx_store__["b" /* Store */]) === "function" && _e || Object])
], SignupComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=signup.component.js.map

/***/ }),

/***/ 20:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_share__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_share___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_share__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoaderService = (function () {
    function LoaderService() {
        // public status:any = new Subject();
        this.status = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
        this._active = false;
    }
    Object.defineProperty(LoaderService.prototype, "active", {
        get: function () {
            return this._active;
        },
        set: function (v) {
            this._active = v;
            this.status.next(v);
        },
        enumerable: true,
        configurable: true
    });
    LoaderService.prototype.start = function () {
        this.active = true;
    };
    LoaderService.prototype.stop = function () {
        this.active = false;
    };
    return LoaderService;
}());
LoaderService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], LoaderService);

//# sourceMappingURL=loader.service.js.map

/***/ }),

/***/ 22:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__ = __webpack_require__(419);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngrx_store__ = __webpack_require__(17);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//redux

var ApiService = (function () {
    function ApiService(_http, _store) {
        this._http = _http;
        this._store = _store;
        this.loginApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'users/login';
        this.signUpApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'users';
        this.UpdateUserApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'users/dealers';
        this.forgetPasswordApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'users/forgotpassword';
        this.resetPasswordApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'users/resetpassword/';
        this.confimationApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'users/confirmation/';
        this.searchAPI = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'all/cars';
        this.searchUpdateAPI = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'all/cars';
        this.brandsNameApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'brand';
        this.carListApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'carlist';
        this.modelYearsListApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'modelyear/';
        this.recentSearchAddApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'recentsearchadd';
        this.payConnectApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'connect/submitrequest';
        this.getFeaturesApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'features';
        this.getDiscountsApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'discounts';
        this.getEquipmentsApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'equipments';
        this.notificationsApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'notification/searcher';
        this.querySessionApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'querySession';
        this.querySessionDataApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'querySession';
        this.subscribeUserApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'subscribeusers';
        this.guestApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'connect/guest/submitrequest';
        this.readNotificationsApi = __WEBPACK_IMPORTED_MODULE_1__global_api_address_service__["a" /* GlobalApiAddressService */].API_ENDPOINT + 'readnotification/searcher';
    }
    ApiService.prototype.doLogin = function (data) {
        return this._http.post(this.loginApi, data)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.doSignUp = function (data) {
        return this._http.post(this.signUpApi, data)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.doUpdateUser = function (data) {
        return this._http.post(this.UpdateUserApi, data)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.forgetPassword = function (email) {
        return this._http.post(this.forgetPasswordApi, { email: email }).map(function (data) {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.restPassword = function (token, data) {
        return this._http.post(this.resetPasswordApi + token, data).map(function (data) {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.confirmation = function (token) {
        return this._http.get(this.confimationApi + token).map(function (data) {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.querySession = function (data) {
        var _data = {
            query: data
        };
        return this._http.post(this.querySessionApi, _data)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE QUERY SESSION ID  HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getQuerySession = function (cacheID) {
        return this._http.get(this.querySessionDataApi + '?cacheID=' + cacheID)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE QUERY SESSION DATA  HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.searchCar = function (data) {
        var query = '?limit=5&page=1&';
        for (var key in data) {
            if (data) {
                if (data[key] && data[key] != null && data[key] != '' && data[key] != 'undefined') {
                    query += key + '=' + data[key] + '&';
                }
            }
        }
        return this._http.get(this.searchAPI + query)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.searchCarUpdate = function (data) {
        var query = '?limit=5&page=1&';
        for (var key in data) {
            if (data) {
                if (data[key] && data[key] != null && data[key] != '' && data[key] != 'undefined') {
                    query += key + '=' + data[key] + '&';
                }
            }
        }
        return this._http.get(this.searchAPI + query)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.userSubcribes = function (email, name) {
        return this._http.post(this.subscribeUserApi, { email: email, name: name })
            .map(function (data) {
            data.json();
            console.log("I CAN SEE QUERY SUBSCRIBE DATA  HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.recentSearch = function (id) {
        return this._http.post(this.recentSearchAddApi + "?id=" + id, id)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getBrand = function () {
        return this._http.get(this.brandsNameApi)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE CARS HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getCarList = function (carName) {
        return this._http.get(this.carListApi + '?search=' + carName)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE MODELS HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getModelYearsList = function (model) {
        return this._http.get(this.modelYearsListApi + '?search=' + model)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE YEARS HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.payConnect = function (connect) {
        return this._http.post(this.payConnectApi, connect)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE CONNECT HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getFeaturesList = function () {
        return this._http.get(this.getFeaturesApi)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE FEATURES HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getDiscountsList = function () {
        return this._http.get(this.getDiscountsApi)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE DISCOUNTS HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getEquipmentsList = function () {
        return this._http.get(this.getEquipmentsApi)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE EQUIPMENTS HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getNotifications = function () {
        return this._http.get(this.notificationsApi)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE NOTIFICATIONS HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getNotificationsPage = function (page) {
        return this._http.get(this.notificationsApi + "?limit=10&page=" + page)
            .map(function (data) {
            data.json();
            console.log("I CAN SEE NOTIFICATIONS HERE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getMyRecentCars = function () {
        return this._http.get(this.searchAPI + '?limit=5&page=1').map(function (data) {
            data.json();
            console.log("I CAN SEE MY RECENT CARS: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.getMyRecentCarsPage = function (page) {
        return this._http.get(this.searchAPI + '?limit=5&' + 'page=' + page).map(function (data) {
            data.json();
            console.log("I CAN SEE MY RECENT CARS PAGE: ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.guest = function (data) {
        return this._http.post(this.guestApi, data).map(function (data) {
            data.json();
            console.log("I CAN SEE GUEST CONNECT : ", data.json());
            return data.json();
        });
    };
    ApiService.prototype.readNotifications = function () {
        return this._http.get(this.readNotificationsApi).map(function (data) {
            data.json();
            console.log("I CAN SEE MY READ NOTIFICATIONS: ", data.json());
            return data.json();
        });
    };
    return ApiService;
}());
ApiService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["b" /* Store */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__ngrx_store__["b" /* Store */]) === "function" && _b || Object])
], ApiService);

var _a, _b;
//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PublicVariablesService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PublicVariablesService = (function () {
    function PublicVariablesService() {
        this.navBar = true;
        this.isLogin = false;
    }
    PublicVariablesService.prototype.getIsLogin = function () {
        return this.isLogin;
    };
    PublicVariablesService.prototype.getNav = function () {
        return this.navBar;
    };
    PublicVariablesService.prototype.getUser = function () {
        return this.currentUser;
    };
    PublicVariablesService.prototype.getSeacrhRecord = function () {
        return this.searchRecord;
    };
    return PublicVariablesService;
}());
PublicVariablesService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], PublicVariablesService);

//# sourceMappingURL=public-variables.service.js.map

/***/ }),

/***/ 396:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 396;


/***/ }),

/***/ 397:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(420);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(429);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 418:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_throw__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_throw___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_throw__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthInterceptorService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthInterceptorService = (function () {
    function AuthInterceptorService(loader) {
        this.loader = loader;
    }
    AuthInterceptorService.prototype.before = function (request) {
        this.loader.start();
        //do something ...
        var currentUser = JSON.parse(localStorage.getItem('SearcherUser'));
        if (currentUser && currentUser.token_id) {
            request.headers.set('Authorization', currentUser.token_id);
        }
        console.log(request);
        return request;
    };
    return AuthInterceptorService;
}());
AuthInterceptorService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__Services_loader_service__["a" /* LoaderService */]) === "function" && _a || Object])
], AuthInterceptorService);

var _a;
//# sourceMappingURL=auth-interceptor.service.js.map

/***/ }),

/***/ 419:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalApiAddressService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GlobalApiAddressService = (function () {
    function GlobalApiAddressService() {
    }
    return GlobalApiAddressService;
}());
GlobalApiAddressService.API_ENDPOINT = "https://easycar.herokuapp.com/api/v1/"; //url
GlobalApiAddressService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [])
], GlobalApiAddressService);

//# sourceMappingURL=global-api-address.service.js.map

/***/ }),

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_app_component_app_component__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_navbar_navbar_component__ = __webpack_require__(425);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_find_new_car_find_new_car_component__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_bs3_modal_ng2_bs3_modal__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_ng2_bs3_modal_ng2_bs3_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_ng2_bs3_modal_ng2_bs3_modal__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_angular2_http_interceptor__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_moment__ = __webpack_require__(432);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ng2_completer__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__Services_auth_graud_service__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__Services_auth_interceptor_service__ = __webpack_require__(418);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__Services_loader_service__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__Services_public_variables_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__Services_get_notifications_count_service__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ngrx_store__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__reducers_authReducer__ = __webpack_require__(427);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__reducers_projectReducer__ = __webpack_require__(428);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ngrx_store_devtools__ = __webpack_require__(414);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__app_routing__ = __webpack_require__(421);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__components_news_letter_news_letter_component__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__components_footer_links_footer_links_component__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__components_login_login_component__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__components_signup_signup_component__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__components_forgetpassword_forgetpassword_component__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__components_redirecting_redirecting_component__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__components_loader_loader_component__ = __webpack_require__(424);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__components_reset_password_reset_password_component__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__components_search_car_search_car_component__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__components_notifications_notifications_component__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__components_privacy_policy_privacy_policy_component__ = __webpack_require__(193);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













// Services






// Redux




// Routing Module

// Components











var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__components_app_component_app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_6__components_navbar_navbar_component__["a" /* NavbarComponent */],
            __WEBPACK_IMPORTED_MODULE_7__components_find_new_car_find_new_car_component__["a" /* FindNewCarComponent */],
            __WEBPACK_IMPORTED_MODULE_23__components_news_letter_news_letter_component__["a" /* NewsLetterComponent */],
            __WEBPACK_IMPORTED_MODULE_24__components_footer_links_footer_links_component__["a" /* FooterLinksComponent */],
            __WEBPACK_IMPORTED_MODULE_25__components_login_login_component__["a" /* LoginComponent */],
            __WEBPACK_IMPORTED_MODULE_26__components_signup_signup_component__["a" /* SignupComponent */],
            __WEBPACK_IMPORTED_MODULE_27__components_forgetpassword_forgetpassword_component__["a" /* ForgetpasswordComponent */],
            __WEBPACK_IMPORTED_MODULE_28__components_redirecting_redirecting_component__["a" /* RedirectingComponent */],
            __WEBPACK_IMPORTED_MODULE_29__components_loader_loader_component__["a" /* LoaderComponent */],
            __WEBPACK_IMPORTED_MODULE_30__components_reset_password_reset_password_component__["a" /* ResetPasswordComponent */],
            __WEBPACK_IMPORTED_MODULE_31__components_search_car_search_car_component__["a" /* SearchCarComponent */],
            __WEBPACK_IMPORTED_MODULE_32__components_notifications_notifications_component__["a" /* NotificationsComponent */],
            __WEBPACK_IMPORTED_MODULE_33__components_privacy_policy_privacy_policy_component__["a" /* PrivacyPolicyComponent */],
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */],
            __WEBPACK_IMPORTED_MODULE_22__app_routing__["a" /* AppRoutingModule */],
            __WEBPACK_IMPORTED_MODULE_8_ng2_bs3_modal_ng2_bs3_modal__["Ng2Bs3ModalModule"],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* ReactiveFormsModule */],
            __WEBPACK_IMPORTED_MODULE_11_ng2_completer__["a" /* Ng2CompleterModule */],
            __WEBPACK_IMPORTED_MODULE_9_angular2_http_interceptor__["a" /* HttpInterceptorModule */].withInterceptors([{
                    provide: __WEBPACK_IMPORTED_MODULE_9_angular2_http_interceptor__["b" /* HttpInterceptor */],
                    useClass: __WEBPACK_IMPORTED_MODULE_13__Services_auth_interceptor_service__["a" /* AuthInterceptorService */],
                    multi: true
                }]),
            __WEBPACK_IMPORTED_MODULE_18__ngrx_store__["a" /* StoreModule */].provideStore({ authReducer: __WEBPACK_IMPORTED_MODULE_19__reducers_authReducer__["a" /* authReducer */], projectReducer: __WEBPACK_IMPORTED_MODULE_20__reducers_projectReducer__["a" /* projectReducer */] }),
            __WEBPACK_IMPORTED_MODULE_21__ngrx_store_devtools__["a" /* StoreDevtoolsModule */].instrumentOnlyWithExtension(),
            __WEBPACK_IMPORTED_MODULE_10_angular2_moment__["MomentModule"]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_12__Services_auth_graud_service__["a" /* AuthGraudService */], __WEBPACK_IMPORTED_MODULE_13__Services_auth_interceptor_service__["a" /* AuthInterceptorService */], __WEBPACK_IMPORTED_MODULE_14__Services_loader_service__["a" /* LoaderService */], __WEBPACK_IMPORTED_MODULE_15__Services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_16__Services_public_variables_service__["a" /* PublicVariablesService */], __WEBPACK_IMPORTED_MODULE_17__Services_get_notifications_count_service__["a" /* GetNotificationsCountService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__components_app_component_app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_find_new_car_find_new_car_component__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_login_login_component__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_signup_signup_component__ = __webpack_require__(197);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_forgetpassword_forgetpassword_component__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__components_redirecting_redirecting_component__ = __webpack_require__(194);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__components_reset_password_reset_password_component__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_search_car_search_car_component__ = __webpack_require__(196);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_notifications_notifications_component__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__components_privacy_policy_privacy_policy_component__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__Services_auth_graud_service__ = __webpack_require__(188);
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppRoutingModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


// Components









//Auth Graud

// Routes
var routes = [
    { path: '', component: __WEBPACK_IMPORTED_MODULE_2__components_find_new_car_find_new_car_component__["a" /* FindNewCarComponent */] },
    { path: 'login', component: __WEBPACK_IMPORTED_MODULE_3__components_login_login_component__["a" /* LoginComponent */] },
    { path: 'signup', component: __WEBPACK_IMPORTED_MODULE_4__components_signup_signup_component__["a" /* SignupComponent */] },
    { path: 'forgetpassword', component: __WEBPACK_IMPORTED_MODULE_5__components_forgetpassword_forgetpassword_component__["a" /* ForgetpasswordComponent */] },
    { path: 'confirmation', component: __WEBPACK_IMPORTED_MODULE_6__components_redirecting_redirecting_component__["a" /* RedirectingComponent */] },
    { path: 'reset-password/:token', component: __WEBPACK_IMPORTED_MODULE_7__components_reset_password_reset_password_component__["a" /* ResetPasswordComponent */] },
    { path: 'confirmation/:token', component: __WEBPACK_IMPORTED_MODULE_6__components_redirecting_redirecting_component__["a" /* RedirectingComponent */] },
    { path: 'search-car', component: __WEBPACK_IMPORTED_MODULE_8__components_search_car_search_car_component__["a" /* SearchCarComponent */] },
    { path: 'notification', component: __WEBPACK_IMPORTED_MODULE_9__components_notifications_notifications_component__["a" /* NotificationsComponent */], canActivate: [__WEBPACK_IMPORTED_MODULE_11__Services_auth_graud_service__["a" /* AuthGraudService */]] },
    { path: 'policy', component: __WEBPACK_IMPORTED_MODULE_10__components_privacy_policy_privacy_policy_component__["a" /* PrivacyPolicyComponent */] },
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */].forRoot(routes)],
        exports: [__WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* RouterModule */]]
    })
], AppRoutingModule);

//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ 422:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AppComponent = (function () {
    function AppComponent() {
        this.title = 'app works!';
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(508),
        styles: [__webpack_require__(488)],
    }),
    __metadata("design:paramtypes", [])
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 423:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterLinksComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterLinksComponent = (function () {
    function FooterLinksComponent() {
    }
    FooterLinksComponent.prototype.ngOnInit = function () {
    };
    return FooterLinksComponent;
}());
FooterLinksComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-footer-links',
        template: __webpack_require__(510),
        styles: [__webpack_require__(490)]
    }),
    __metadata("design:paramtypes", [])
], FooterLinksComponent);

//# sourceMappingURL=footer-links.component.js.map

/***/ }),

/***/ 424:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Services_loader_service__ = __webpack_require__(20);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoaderComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var LoaderComponent = (function () {
    function LoaderComponent(loader) {
        var _this = this;
        loader.status.subscribe(function (status) {
            _this.active = status;
        });
    }
    LoaderComponent.prototype.ngOnInit = function () {
    };
    return LoaderComponent;
}());
LoaderComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-loader',
        template: __webpack_require__(512),
        styles: [__webpack_require__(492)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__Services_loader_service__["a" /* LoaderService */]) === "function" && _a || Object])
], LoaderComponent);

var _a;
//# sourceMappingURL=loader.component.js.map

/***/ }),

/***/ 425:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Services_public_variables_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(21);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavbarComponent = (function () {
    function NavbarComponent(variableService, route) {
        this.variableService = variableService;
        this.route = route;
        this.isLogin = false;
        if (!localStorage.getItem('searcherUserID')) {
            this.isLogin = this.variableService.getIsLogin();
            ;
        }
        else {
            this.variableService.isLogin = true;
        }
        this.user = localStorage.getItem('SearcherUser');
        this.user = JSON.parse(this.user);
        console.log(this.user);
    }
    NavbarComponent.prototype.logout = function () {
        console.log('logout');
        localStorage.removeItem('SearcherUser');
        localStorage.removeItem('searcherUserID');
        this.variableService.isLogin = false;
        this.route.navigate(['/login']);
    };
    NavbarComponent.prototype.ngOnInit = function () { };
    return NavbarComponent;
}());
NavbarComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-navbar',
        template: __webpack_require__(514),
        styles: [__webpack_require__(494)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__Services_public_variables_service__["a" /* PublicVariablesService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__Services_public_variables_service__["a" /* PublicVariablesService */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === "function" && _b || Object])
], NavbarComponent);

var _a, _b;
//# sourceMappingURL=navbar.component.js.map

/***/ }),

/***/ 426:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__ = __webpack_require__(118);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Services_api_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__ = __webpack_require__(20);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsLetterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var NewsLetterComponent = (function () {
    function NewsLetterComponent(fb, userService, loader) {
        this.fb = fb;
        this.userService = userService;
        this.loader = loader;
        this.user = {};
        this.form = this.fb.group({
            'email': [this.user.email || null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* Validators */].required],
            'name': [this.user.name || null, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["h" /* Validators */].required],
        });
    }
    NewsLetterComponent.prototype.isEmailValid = function () {
        var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
        if (EMAIL_REGEXP.test(this.user.email)) {
            return true;
        }
        else {
            return false;
        }
    };
    NewsLetterComponent.prototype.onsubmit = function () {
        var _this = this;
        this.userService.userSubcribes(this.user.email, this.user.name).subscribe(function (data) {
            _this.loader.stop();
            _this.successModal.open();
            console.log(data);
            _this.user = {};
        }, function (error) {
            console.log(error);
            _this.loader.stop();
            var err = JSON.parse(error._body);
            console.log(err);
            if (err.error.code === 11000) {
                _this.alreadySubscribeModal.open();
            }
            _this.user = {};
        });
    };
    NewsLetterComponent.prototype.ngOnInit = function () {
    };
    return NewsLetterComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('success'),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"]) === "function" && _a || Object)
], NewsLetterComponent.prototype, "successModal", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('alreadySubscribeModal'),
    __metadata("design:type", typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2_ng2_bs3_modal_ng2_bs3_modal__["ModalComponent"]) === "function" && _b || Object)
], NewsLetterComponent.prototype, "alreadySubscribeModal", void 0);
NewsLetterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-news-letter',
        template: __webpack_require__(515),
        styles: [__webpack_require__(495)]
    }),
    __metadata("design:paramtypes", [typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* FormBuilder */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* FormBuilder */]) === "function" && _c || Object, typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__Services_api_service__["a" /* ApiService */]) === "function" && _d || Object, typeof (_e = typeof __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_4__Services_loader_service__["a" /* LoaderService */]) === "function" && _e || Object])
], NewsLetterComponent);

var _a, _b, _c, _d, _e;
//# sourceMappingURL=news-letter.component.js.map

/***/ }),

/***/ 427:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return authReducer; });
var authReducer = function (state, action) {
    if (state === void 0) { state = []; }
    switch (action.type) {
        case 'LOGIN_INITIALS':
            return state.concat([
                //Object.assign({}, { isLogin:true ,payload:action.payload })
                Object.assign({}, { isLogin: false })
            ]);
        case 'LOGIN_FAILD':
            return state.concat([
                //Object.assign({}, { isLogin:true ,payload:action.payload })
                Object.assign({}, { isLogin: false, loginFaild: true })
            ]);
        case 'LOGIN_SUCCESS':
            return state.concat([
                Object.assign({}, { isLogin: true, payload: action.payload })
            ]);
        case 'SIGNUP_INIT':
            return state.concat([
                Object.assign({}, { isSignUp: false })
            ]);
        case 'SIGNUP_FAILD':
            return state.concat([
                Object.assign({}, { isSignUp: false })
            ]);
        case 'SIGNUP_SUCCESS':
            return state.concat([
                Object.assign({}, { isSignUp: true, payload: action.payload })
            ]);
        case 'FORGETPASSWORD_INIT':
            return state.concat([
                Object.assign({}, { forgetPasswordCallSubmit: false })
            ]);
        case 'FORGETPASSWORD_FAILD':
            return state.concat([
                Object.assign({}, { forgetPasswordCallSubmit: false })
            ]);
        case 'FORGETPASSWORD_SUCCESS':
            return state.concat([
                Object.assign({}, { forgetPasswordCallSubmit: true, payload: action.payload })
            ]);
        case 'RESETPASSWORD_INIT':
            return state.concat([
                Object.assign({}, { resetPassword: false })
            ]);
        case 'RESETPASSWORD_FAILD':
            return state.concat([
                Object.assign({}, { resetPassword: false })
            ]);
        case 'RESETPASSWORD_SUCCESS':
            return state.concat([
                Object.assign({}, { resetPassword: true, payload: action.payload })
            ]);
        case 'CONFIRMATION_INIT':
            return state.concat([
                Object.assign({}, { tokenConfirmation: false })
            ]);
        case 'CONFIRMATION_FAILD':
            return state.concat([
                Object.assign({}, { tokenConfirmation: false })
            ]);
        case 'CONFIRMATION_SUCCESS':
            return state.concat([
                Object.assign({}, { tokenConfirmation: true, payload: action.payload })
            ]);
        default:
            return state;
    }
};
//# sourceMappingURL=authReducer.js.map

/***/ }),

/***/ 428:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return projectReducer; });
var projectReducer = function (state, action) {
    if (state === void 0) { state = []; }
    switch (action.type) {
        case 'SEARCH-CAR_INIT':
            return state.concat([
                Object.assign({}, { findcar: false })
            ]);
        case 'SEARCH-CAR_FAILD':
            return state.concat([
                Object.assign({}, { findcar: false })
            ]);
        case 'SEARCH-CAR_SUCCESS':
            return state.concat([
                Object.assign({}, { findcar: true, payload: action.payload })
            ]);
        case 'SHOW_ALL_OPTIONS':
            return state.concat([
                Object.assign({}, { Features: false, Equipments: false })
            ]);
        case 'HIDE_OPTIONS':
            return state.concat([
                Object.assign({}, { ShowFeatures: false, ShowEquipments: false })
            ]);
        case 'GET_BRAND_NAME_INIT':
            return state.concat([
                Object.assign({}, { getBrandName: false })
            ]);
        case 'GET_BRAND_NAME_FAILD':
            return state.concat([
                Object.assign({}, { getBrandName: false })
            ]);
        case 'GET_BRAND_NAME_SUCCESS':
            return state.concat([
                Object.assign({}, { getBrandName: true, payload: action.payload })
            ]);
        case 'GET_BRAND_MODEL_INIT':
            return state.concat([
                Object.assign({}, { getBrandModel: false })
            ]);
        case 'GET_BRAND_MODEL_FAILD':
            return state.concat([
                Object.assign({}, { getBrandModel: false })
            ]);
        case 'GET_BRAND_MODEL_SUCCESS':
            return state.concat([
                Object.assign({}, { getBrandModel: true, payload: action.payload })
            ]);
        case 'GET_BRAND_YEAR_INIT':
            return state.concat([
                Object.assign({}, { getBrandYear: false })
            ]);
        case 'GET_BRAND_YEAR_FAILD':
            return state.concat([
                Object.assign({}, { getBrandYear: false })
            ]);
        case 'GET_BRAND_YEAR_SUCCESS':
            return state.concat([
                Object.assign({}, { getBrandYear: true, payload: action.payload })
            ]);
        //searchCar Component
        case 'SEARCH-CAR_UPDATE_FAILD':
            return state.concat([
                Object.assign({}, { updateFindcar: false })
            ]);
        case 'SEARCH-CAR_UPDATE_SUCCESS':
            return state.concat([
                Object.assign({}, { updateFindcar: true, payload: action.payload })
            ]);
        case 'CONNECT_CAR_INT':
            return state.concat([
                Object.assign({}, { connectToCar: false })
            ]);
        case 'CONNECT_CAR_FAILD':
            return state.concat([
                Object.assign({}, { connectToCar: false })
            ]);
        case 'CONNECT_CAR_SUCCESS':
            return state.concat([
                Object.assign({}, { connectToCar: true, payload: action.payload })
            ]);
        case 'GET_STRIP_FORM_DATA_INIT':
            return state.concat([
                Object.assign({}, { getStripFormData: false })
            ]);
        case 'GET_STRIP_FORM_DATA_FAILD':
            return state.concat([
                Object.assign({}, { getStripFormData: false })
            ]);
        case 'GET_STRIP_FORM_DATA_SUCCESS':
            return state.concat([
                Object.assign({}, { getStripFormData: true })
            ]);
        case 'GET_FEATURES_FAILD':
            return state.concat([
                Object.assign({}, { getFeatures: false })
            ]);
        case 'GET_FEATURES_SUCCESS':
            return state.concat([
                Object.assign({}, { getFeatures: true, payload: action.payload })
            ]);
        case 'GET_EQUIPMENTS_FAILD':
            return state.concat([
                Object.assign({}, { getEquipments: false })
            ]);
        case 'GET_EQUIPMENTS_SUCCESS':
            return state.concat([
                Object.assign({}, { getEquipments: true, payload: action.payload })
            ]);
        case 'GET_DISCOUNTS_FAILD':
            return state.concat([
                Object.assign({}, { getDiscounts: false })
            ]);
        case 'GET_DISCOUNTS_SUCCESS':
            return state.concat([
                Object.assign({}, { getDiscounts: true, payload: action.payload })
            ]);
        case 'CONFIRMATION_INIT':
            return state.concat([
                Object.assign({}, { confirmation: false })
            ]);
        case 'CONFIRMATION_FAILD':
            return state.concat([
                Object.assign({}, { confirmation: false })
            ]);
        case 'CONFIRMATION_SUCCESS':
            return state.concat([
                Object.assign({}, { confirmation: true, payload: action.payload })
            ]);
        case 'GET_PAGE':
            return state.concat([
                Object.assign({}, { payload: action.payload })
            ]);
        case 'NEXT_PAGE':
            return state.concat([
                Object.assign({}, { payload: action.payload })
            ]);
        case 'PREVIOUS_PAGE':
            return state.concat([
                Object.assign({}, { payload: action.payload })
            ]);
        default:
            return state;
    }
};
//# sourceMappingURL=projectReducer.js.map

/***/ }),

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 488:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, "button:hover{\n    cursor: pointer\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 489:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".seacrchButton{\n    border-top-right-radius: 50px;\n    border-bottom-right-radius: 50px;\n    border-top-left-radius: 50px;\n    border-bottom-left-radius: 50px;\n    background-color: #414042;\n    color: white;\n    font-weight: bold;\n    width: 90%;\n    text-align: center;\n    margin-left: 18px;\n}\nbutton:hover{\n    cursor: pointer\n}\n.form-group>input{\n    border-top-right-radius: 50px;\n    border-bottom-right-radius: 50px;\n    border-top-left-radius: 50px;\n    border-bottom-left-radius: 50px;\n    text-align: center;\n    padding: 10px;\n}\nng2-completer>input{\n    border-top-right-radius: 50px;\n    border-bottom-right-radius: 50px;\n    border-top-left-radius: 50px;\n    border-bottom-left-radius: 50px;\n    text-align: center;\n    padding: 10px;\n}\n\nselect{\n    border-top-right-radius: 50px;\n    border-bottom-right-radius: 50px;\n    border-top-left-radius: 50px;\n    border-bottom-left-radius: 50px;\n    padding: 10px;\n    text-indent: 10px;\n    text-align: center;\n    padding-left: 27%\n}\n.div1{\n    background-color: rgb(236, 240, 241);\n    padding: 20px;\n    margin-top: -70px;\n}\n.carLinks{\n    /*background-color: aqua;*/\n    margin-top: 30px;\n    padding: 20px;\n}\nli{\n    list-style: none;\n    padding: 5px;\n}\n\nli>a{\n    color: black;\n    text-decoration: none;\n}\n.seacrhingOptionsDiv{\n    margin: auto auto 40px auto;\n}\nh5{\n    font-size: 24px;\n    \n}\nul{\n    padding: 20px;\n}\nsub{\n    font-weight: bold;\n}\n.carousel-inner>.imgwrapper {\n   /*width: 1000%;*/\n   background-image: url(" + __webpack_require__(771) + ");\n    /*background-position: */\n    background-repeat: no-repeat;\n    background-size: cover;\n    background-position: 50%;\n    height: 44em;\n}\n/* Carousel base class */\n.carousel {\n  margin-bottom: 4rem;\n}\n/* Since positioning the image, we need to help out the caption */\n.carousel-caption {\n  z-index: 10;\n  bottom: 3rem;\n}\n\n/* Declare heights because of positioning of img element */\n.carousel-item {\n  height: 32rem;\n  background-color: #777;\n}\n.carousel-item > img {\n  position: absolute;\n  top: 0;\n  left: 0;\n  min-width: 100%;\n  height: 32rem;\n}\n.caption{\n    margin: auto auto auto auto;\n    background-color: rgb(52, 152, 219);\n    padding: 10px;\n    font-family: serif;\n    font-weight: bold;\n    width: 450px;\n    /*font-size: 30px;*/\n    border-top-left-radius: 150px;\n    border-bottom-right-radius: 150px;\n}\n.caption2{\n    /*margin: 20px auto 110px auto;*/\n    font-weight: bold;\n    font-family: serif;\n    padding: 10px\n}\n.button{\n    margin: 10px auto -10px auto;\n}\n.modal-title{\n    text-align: center;\n\n}\n.modalContent{\n    text-align: center;\n    text-transform: capitalize\n}\n.modal-body>input{\n    padding: 10px;\n    margin: auto;\n    width: 50%;\n}\n.modal-body{\n    text-align: center;\n}\n.reqEmail{\n    text-align: center;\n    padding: 5px;\n    width: 50%;\n    padding: 5px;\n\n}\n.modalSlogan{\n    display: block;\n    font-size: 12px;\n    margin-top: 5px;\n}\n.row p:hover{\n    cursor: pointer\n}\n\n.moreOptionsLinks{\n    margin-top: 10px;\n}\n\n.links{\n    text-decoration: none;\n    color: #e81b23;\n    font-size: 17px;\n}\n.brandCar{\n    width: 100%;\n}\n\n.heading{\n    padding: 10px;\n    margin-top: 10px;\n    font-family: serif;\n    color: rgb(41, 128, 185);\n    font-weight: bold;\n}\n.subHeading{\n    padding: 20px;\n    color: dodgerblue;\n    text-align: center;\n}\n\n.carDiv{\n    position: absolute;\n    z-index: 10;\n    bottom: 0;\n    left:0;\n}\n.brandDiv{\n    background-color: #27aae1;\n    height: 600px;\n}\n.chooseABrandDiv{\n    background-color: #edf0f5;\n    padding: 40px;\n}\n.searchbuttons{\n    margin-top: 10px;\n    padding: 5px;\n}\n.carDiv imgwrapper{\n    width: 60%\n}\n.pageDownArrow{\n    opacity: 0.7;\n    height: 50px;\n    width: 50px;\n    margin-bottom: -20px\n}\n.arrowFontSize{\n    font-size: 30px\n}\n.userInputDiv{\n    margin: 40px auto auto auto;\n}\n@media(min-width:992px){\n    #SearchBtn{\n            width: 235px;\n    }\n    .brandCar{\n        width: 70%;\n    }\n}\n #searchText{\n     display: inherit;\n    padding-top: 10px;\n    }\na:hover{\n    cursor: pointer;\n}\n.theme-btn{\n    float: right;\n}\n@media (min-width:992px) and (max-width:1140px){\n    .theme-btn{\n        width: 220px !important;\n    }\n}\n@media (min-width:768px){\n    .theme-btn{\n        margin-right: 4%;\n    }\n}\n@media(min-width:1200px){\n    .brandCar{\n        width: 86%;\n    }\n\n}\n@media(min-width:1400px){\n    .brandCar{\n        width: 100%;\n    }\n\n}\n@media(min-width:1600px){\n    .brandCar{\n        width: 1030px;\n    }\n\n}\n.search-fields{\n     box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 490:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".footer{\n    /*background-color: #666666;*/\n    background-image: url(" + __webpack_require__(773) + ");\n     background-size: 100% 100%;\n    background-repeat: no-repeat;\n    height: auto;\n    padding-top: 70px;\n    padding-bottom: 70px;\n    color: #fff;\n    width: 100%;\n    margin: 0px;\n}\nul{\n    margin-top: 20px;\n}\n\nli{\n    list-style: none;\n    line-height: 30px;\n    font-size: 12px;\n}\n.car-col{\n    padding-left: 50px;\n}\n.footerHeading{\n    font-size: 20px;\n}\n.copyright{\n    color: #fff;\n    padding: 30px;\n    width: 100%;\n    margin: 0px;\n    background-color: #252525;\n    font-size: 12px;\n}\n.social{\n    padding-right:15px;\n}\n.footerLink{\n    text-decoration: none;\n    color: #fff;\n}\n\n/*.footLinkSec{\n    text-align: left;\n    }\n\n@media (max-width: 992px) {\n   .footLinkSec{\n    text-align: center;\n    }\n   \n    \n}*/\na:hover{\n    text-decoration: none;\n}\na{\n    color: white;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 491:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".mainDiv{\n     background-image: url(" + __webpack_require__(81) + ");\n     min-width : 100%;\n     background-size: cover;\n     background-repeat:no-repeat;\n     overflow-y: hidden;\n     overflow-x: hidden;\n     margin: auto;\n     padding-bottom: 340px;\n\n}\n.loginDiv{\n    margin: 40% 0px;\n    \n}\n.loginDiv>h1{\n    text-align: center;\n    color: white;\n    font-size: 30px;\n    margin-bottom: 20px;\n    font-family: serif;\n    font-weight: bold;\n\n}\n.form-group>input{\n    min-width: 100px;\n    text-align: center;\n    padding: 10px;\n    border-radius: 50px;\n}\n.button{\n border-radius: 50px;\n font-weight: bold;\n width: 100%;\n font-size: 25px;\n background-color: #666666;\n margin-top: 10px;\n color: white;\n}\n.button>span{\n    float: right;\n    background-color: silver;\n\n}\n.theme-btn{\n    width: 100%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 492:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".loading {\n    display: inline-block;\n    position: fixed;\n    top: 0;\n    height: 100%;\n    width: 100%;\n    text-align: center;\n    background-color: rgba(0, 0, 0, 0.5);\n    z-index: 99\n}\n.loading img{\n    position: fixed;\n    top: 0;\n    bottom: 0;\n    left: 0;\n    right: 0;\n    margin: auto;\n}\n\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 493:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".mainDiv{\n     background-image: url(" + __webpack_require__(81) + ");\n     min-width : 100%;\n     background-size: cover;\n     background-repeat:no-repeat;\n     overflow-y: hidden;\n     overflow-x: hidden;\n     margin: auto;\n     padding-bottom: 110px;\n}\n.loginDiv{\n    margin: 50% 0px;\n    \n}\n.loginDiv>h1{\n    text-align: center;\n    color: white;\n    font-size: 50px;\n    margin-bottom: 20px;\n    font-family: serif;\n    font-weight: bold;\n\n}\n.form-group>input{\n    min-width: 100px;\n    text-align: center;\n    padding: 10px;\n    border-radius: 50px;\n}\n.button{\n min-width: 185px;\n border-radius: 50px;\n font-weight: bold;\n width: 100%;\n font-size: 25px;\n background-color: #666666;\n margin-top: 10px;\n color: white;\n}\n.button>span{\n    float: right;\n    background-color: silver;\n\n}\n\n@media(min-width:992px){\n     #signupBtn{\n    margin-left: 30px;\n}\n}\n .auth-buttons .theme-btn{\n    width: 160px;\n}\n\n\n@media (max-width: 992px){\n    .auth-buttons .theme-btn{\n        width: 100%;\n    }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 494:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, "form>button{\n    font-size: 15px;\n    border-radius: 20px;\n    padding: 12px;\n    width: 200px;\n    margin: auto auto auto 5px;\n}\n.badge-notify{\n   background:red;\n   position:relative;\n   top: -20px;\n   left: -35px;\n}\n#logout{\n    margin: auto;\n}\n\n.nav-item a{\n    color: #fff;\n    font-size: 18px;\n}\na:hover{\n    cursor: pointer;\n}\n.nav-link{\n    display: block !important;\n}\n@media(max-width:992px){\n    .form-inline button {\n        width: 100%;\n        margin-top: 10px !important;\n        display: block;    \n    }\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 495:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".newsLetter{\n    background-color: #27aae1;\n    height: 100%;\n    padding-top: 30px;\n    padding-bottom: 30px;\n    color: #fff;\n    width: 100%;\n    margin: 0px;\n}\n.left-mrgin{\n    margin-left: 5px;\n    margin-right: 5px;\n}\n.newLetterInput{\n    border-radius: 0px;\n}\n.newsLetterHeading{\n    display: inline;\n    font-size: 22px;\n}\n.envelope-icon{\n    display: inline;\n    font-size: 24px;\n    margin-right: 20px;\n}\n.dumy-text{\n    font-size: 12px;\n}\n.form-newsLetter{\n    text-align: center;\n    margin-top: 5px;\n    \n}\n.subcribe-btn{\n    border-radius: 0;\n    background-color: #00466f;\n}\n#formSection{\n    margin: auto;\n}\n\n@media (min-width: 1200px) {\n   .name{\n   \n    margin-right: -25%;\n    \n    }\n    .email{\n        margin-right: -25%;\n   \n   \n      \n    }\n}\n::-webkit-input-placeholder {\n    color:    #999;\n}\n:-moz-placeholder {\n    color:    #999;\n}\n::-moz-placeholder {\n    color:    #999;\n}\n:-ms-input-placeholder {\n    color:    #999;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 496:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".mainDiv{\n    min-height: 480px;\n    margin-top: 10%;\n}\n\n.notificationsDiv{\n    margin-top: 80px;\n}\n.notificationsDiv>h1{\n    text-align: center;\n}\n.listOfNotifications{\n    margin: 20px auto 15px auto;\n    padding: 15px;\n    text-align: center;\n    border-radius: 5px;\n    width: 90%;\n    background-color: #ecf0f1;\n    box-shadow: 3px 3px 5px #888888;\n}\n.message{\n    padding: 5px;\n}\n.status{\n    padding: 5px;\n}\n.time{\n    padding: 5px;\n}\na.PageActive{\n    background-color: #3498db;\n    color: white;   \n}\n.page-link,.page-link:focus, .page-link:hover {\n    color: black;\n}\n.notificationHeaderDiv{\n        width: 100vw;\n    }\n@media(min-width:992px){\n    .notificationRead{\n        float: right ;\n    }\n}\n@media(max-width:576px){\n\n.notificationHeading{\n    text-align: center;\n}\n}\n@media(max-width:768px){\n    .notificationHeaderDiv{\n        margin: auto;\n        text-align: center;\n    }\n    .notificationHeading{\n         text-align: center;\n           font-size: 22px\n    }\n}\n@media(min-width:768px) and (max-width:992px){\n      .notificationHeaderDiv{\n        margin: auto;\n        text-align: center;\n    }\n}\n/*@media(max-width:576px){\n    .notificationHeading{\n          margin-left: 50%;\n       \n    }\n}*/", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 497:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".banner{\n    background-image: url(" + __webpack_require__(772) + ");\n    background-repeat: no-repeat;\n    background-size: cover;\n    background-position: 50%;\n    height: 30em;\n}\n.headingDiv{\n    color: #fff;\n    margin-top: 200px;\n}\n.heading{\n    font-size: 50px;\n    font-weight: bold;\n}\n.date{\n  font-size: 20px;\n  font-weight: 400;\n}\n.privacyHeading{\n    margin-top: 20px;\n    font-size: 35px;\n    font-weight: bold;\n    margin-bottom: 10px;\n\n}\n.bodyText{\n    margin-bottom: 20px;\n}\n.bodyTextFont{\n    font-size: 18px;\n}\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 498:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 499:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".mainDiv{\n    background-image: url(" + __webpack_require__(81) + ");\n    min-width : 100%;\n    background-size: cover;\n    background-repeat:no-repeat;\n    overflow-y: hidden;\n    overflow-x: hidden;\n    margin: auto;\n    padding-bottom: 210px;\n}\n.loginDiv{\n   margin: 50% 0px;\n   \n}\n.loginDiv>h1{\n   text-align: center;\n   color: white;\n   font-size: 30px;\n   margin-bottom: 20px;\n   font-family: serif;\n   font-weight: bold;\n\n}\n.form-group>input{\n   min-width: 100px;\n   text-align: center;\n   padding: 10px;\n   border-radius: 50px;\n}\n.button{\nborder-radius: 50px;\nfont-weight: bold;\nwidth: 100%;\nfont-size: 25px;\nbackground-color: #666666 ;\nmargin-top: 10px;\ncolor: white;\n}\n.button>span{\n   float: right;\n   background-color: silver;\n\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 500:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, "\n/* GLOBAL STYLES\n-------------------------------------------------- */\n/* Padding below the footer and lighter body text */\n\nbody {\n  padding-top: 3rem;\n  padding-bottom: 3rem;\n  color: #5a5a5a;\n}\n\n\n/* CUSTOMIZE THE CAROUSEL\n-------------------------------------------------- */\n\n/* Carousel base class */\n.carousel {\n  margin-bottom: 4rem;\n}\n/* Since positioning the image, we need to help out the caption */\n.carousel-caption {\n  z-index: 10;\n  bottom: 3rem;\n}\n\n/* Declare heights because of positioning of img element */\n.carousel-item {\n  height: 32rem;\n  background-color: #777;\n}\n.carousel-item > img {\n  position: absolute;\n  top: 0;\n  left: 0;\n  min-width: 100%;\n  height: 32rem;\n}\n\n\n/* MARKETING CONTENT\n-------------------------------------------------- */\n\n/* Center align the text within the three columns below the carousel */\n.marketing .col-lg-4 {\n  margin-bottom: 1.5rem;\n  text-align: center;\n}\n.marketing h2 {\n  font-weight: normal;\n}\n.marketing .col-lg-4 p {\n  margin-right: .75rem;\n  margin-left: .75rem;\n}\n\n\n/* Featurettes\n------------------------- */\n\n.featurette-divider {\n  margin: 5rem 0; /* Space out the Bootstrap <hr> more */\n}\n\n/* Thin out the marketing headings */\n.featurette-heading {\n  font-weight: 300;\n  line-height: 1;\n  letter-spacing: -.05rem;\n}\n\n\n/* RESPONSIVE CSS\n-------------------------------------------------- */\n\n@media (min-width: 40em) {\n  /* Bump up size of carousel content */\n  .carousel-caption p {\n    margin-bottom: 1.25rem;\n    font-size: 1.25rem;\n    line-height: 1.4;\n  }\n\n  .featurette-heading {\n    font-size: 50px;\n  }\n}\n\n@media (min-width: 62em) {\n  .featurette-heading {\n    margin-top: 7rem;\n  }\n}\n.button{\n    margin: 10px auto -10px auto;\n}\n.imgwrapper {\n   width: 100%;\n}\n\n\n/* find new car  */\n\n.seacrchButton{\n    border-top-right-radius: 50px;\n    border-bottom-right-radius: 50px;\n    border-top-left-radius: 50px;\n    border-bottom-left-radius: 50px;\n    background-color: #414042;\n    color: white;\n    font-weight: bold;\n    width: 90%;\n    text-align: center;\n    margin-left: 18px;\n}\n\n.form-group>input{\n    border-top-right-radius: 50px;\n    border-bottom-right-radius: 50px;\n    border-top-left-radius: 50px;\n    border-bottom-left-radius: 50px;\n    text-align: center;\n    padding: 10px;\n}\n.div1 select{\n    border-top-right-radius: 50px;\n    border-bottom-right-radius: 50px;\n    border-top-left-radius: 50px;\n    border-bottom-left-radius: 50px;\n    padding: 10px;\n}\n.div1{\n    background-color: rgb(236, 240, 241);\n    padding: 20px;\n    padding-bottom: 12%;\n    margin-top: -70px;\n}\n.carLinks{\n    /*background-color: aqua;*/\n    margin-top: 30px;\n    padding: 20px;\n}\nli{\n    list-style: none;\n    padding: 5px;\n}\n\nli>a{\n    color: black;\n    text-decoration: none;\n}\n.seacrhingOptionsDiv{\n    background-color: white;\n    padding: 10px;\n    margin: auto auto 40px auto;\n    border-radius: 12px\n}\nh5{\n    font-size: 24px;\n    \n}\nul{\n    padding: 20px;\n}\n\n.modal-title{\n    text-align: center;\n\n}\n.modalContent{\n    text-align: center;\n    text-transform: capitalize\n}\n.modal-body>input{\n    padding: 10px;\n    margin: auto;\n    width: 50%;\n}\n.modal-body{\n    text-align: center;\n}\n.reqEmail{\n    text-align: center;\n    padding: 5px;\n    width: 50%;\n    padding: 5px;\n\n}\n.modalSlogan{\n    display: block;\n    font-size: 12px;\n    margin-top: 5px;\n}\n\n.modalLink{\n    color: #fff;\n}\n.modalLink:hover{\n    color: #fff;\n}\n.row p:hover{\n    cursor: pointer\n}\n#option{\n    padding: 10px;\n\n}\n#option input{\n    margin-bottom: 15px;\n}\n.moreOptions{\n   text-decoration: none;\n   color: #e81b23;\n   margin: auto;\n   padding: 20px;\n   text-align:center\n}\n.marginAuto{\n    margin: auto;\n}\n.noRecord{\n    text-align: center;\n    font-size: 25px;\n    padding: 30px;\n}\n.carDistance{\n    background-color: rgb(52, 152, 219);\n    margin: auto;width: 60%;\n    right: 0;\n}\n.carDistanceText{\n    padding: 15px;\n    color: white;\n}\n\n.links{\n    text-decoration: none;\n    color: #e81b23;\n    font-size: 17px;\n}\n.div2 .imgwrapper{\n    width: 60%;\n}\n.carsList{\n    margin-right: 9%;\n    margin-left: 9%;\n}\n@media (max-width : 992px){\n\n    .carsList{\n    margin-right: 0%;\n    margin-left: 0%;\n    }\n    .div1{\n         padding-bottom: 16%;\n    }\n\n}\n.modal-body input{\n    border-radius: 0px !important\n}\n.form-group{\n    text-align: left !important\n}\nsup{\n    font-size: 15px;\n    color: red;\n}\n\n/*.details{\n    text-align: left;\n}*/\n.successModal{\n    background-color: rgb(46, 204, 113);\n}\n.requestModal{\n   background-color: #3498db;\n   color: white;\n}\n.connectPayBtton{\n    width: 50%;\n}\n.successFullContected{\n   font-size: 20px;\n   color: white;\n}\n.div1{\n    background-color: #f5f5f5\n}\n.heading{\n    padding: 10px;\n    margin-top: 10px;\n    font-family: serif;\n    color: rgb(41, 128, 185);\n    font-weight: bold;\n}\n.subHeading{\n    padding: 20px;\n    color: dodgerblue;\n    text-align: center\n}\n.detailsDiv1{\n    padding: 10px;\n}\n.detailsDiv2{\n    padding: 10px;\n    /*margin: 10px;*/\n\n}\n.search-fields{\n     box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);\n}\n\n\n.datailsMainDiv{\n    margin: auto;\n    margin-top: 20px;\n    background-color: white;\n    padding: 10px;\n    border-bottom: 1px solid silver;\n    margin-bottom: 20px;\n    /*box-shadow: 1px 2px 4px rgba(0, 0, 0, .5);*/\n\n    text-transform: capitalize;\n     box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);\n    transition: all 0.3s cubic-bezier(.25,.8,.25,1);\n    }\n\n    .datailsMainDiv:hover{\n        box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);\n    }\n\n    /* pagination */\n.pagination{\n    padding: 15px;\n    margin-right: 5%;\n    float: right;\n}\na:hover{\n    cursor: pointer;\n    color: #3498db;\n}\na.PageActive{\n    background-color: #3498db;\n    color: white;   \n}\n.pageTopDiv{\n    margin-top: 15px;\n}\n.marginAuto{\n    margin: auto;\n}\n\n@media (min-width:768px){\n    #nameInput{\n        margin-left:30px;\n    }\n    #month{\n      width: 65px;\n    }\n    #years{\n    width: 81px;\n    margin-left: -10px;\n    }\n   \n    \n}\n\ninput[type=number]::-webkit-inner-spin-button, \ninput[type=number]::-webkit-outer-spin-button { \n  -webkit-appearance: none; \n  margin: 0; \n}\n.error{\n   color:#F74222;\n}\nselect{\n    text-align: center;\n   text-align-last: center;\n}\n\n@media(min-width:992px){\n    #offerBtn{\n    width: 290px;\n    }\n    #updateBtn{\n    width: 290px;\n    }\n     /*.detailsDiv2{\n        position: absolute;\n        bottom: 0;\n        right: 0;\n    }*/\n}\n\n\n.search-action-btn{\n    margin-top: 10px;\n}\n\n@media(max-width:992px){\n    .connectButtons{\n        position: relative;\n        width: 100%;\n        height: 100%;\n        margin-bottom: 10px;\n    }\n    .pagination{\n    margin-right: -3%;\n \n    }\n    \n\n}\n\n@media (min-width:992px) and (max-width:1060px){\n    .theme-btn{\n        width: 200px !important;\n    }\n    .detailsDiv2{\n        position: absolute;\n        bottom: 0;\n        right: 0;\n        margin-bottom: 10px;\n    }\n}\n@media (min-width: 992px) {\n    .connectButtons{\n    position: absolute;\n    bottom: 20px;\n    right: 20px;\n}\n}\n\n\n@media(max-width:768px){\n   .div1{\n         padding-bottom: 22%;\n    }\n}\n@media(min-width:567px){\n    .estimateTime{\n        float: right;\n        text-align: right;\n    }\n}\n@media(max-width:567px){\n   .div1{\n         padding-bottom: 40%;\n    }\n}\n\n@media(max-width:340px){\n    .theme-btn .title{\n        font-size: 12px !important;\n    }\n\n}\n\n\n\n.cancelBtn{\n    margin-right: 15px;\n}\n\n.estimateTime{\n    color: #3498db;\n    }\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 501:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(11)(false);
// imports


// module
exports.push([module.i, ".mainDiv{\n     background-image: url(" + __webpack_require__(81) + ");\n     min-width : 100%;\n     background-size: cover;\n     background-repeat:no-repeat;\n     overflow-y: hidden;\n     overflow-x: hidden;\n     margin: auto;\n}\n.signupDiv{\n    margin: 39% 0px;\n}\n.signupDiv>h1{\n    text-align: center;\n    color: white;\n    font-size: 50px;\n    margin-bottom: 20px;\n    font-family: serif;\n    font-weight: bold;\n\n}\n.form-group>input,select{\n    min-width: 100px;\n    text-align: center;\n    padding: 10px;\n    border-radius: 50px;\n}\n.button{\n border-radius: 50px;\n font-weight: bold;\n width: 100%;\n font-size: 25px;\n background-color: #666666;\n margin-top: 10px;\n color: white;\n}\n.button>span{\n    float: right;\n    background-color: silver;\n\n}\nselect{\n    text-align: center;\n    text-align-last: center;\n    height: inherit;\n}\n.theme-btn{\n    width: 100% !important;\n}\n\n\n/* theme fields */\n\n.search-fields{\n    background-color: white;\n    padding: 15px 10px;\n    border-radius: 12px;\n    border-radius: 2px;\n    border: thin solid lightgray;\n}\n.search-fields .field{\n    width: 16%;\n    margin: 0 2%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 502:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 222,
	"./af.js": 222,
	"./ar": 229,
	"./ar-dz": 223,
	"./ar-dz.js": 223,
	"./ar-kw": 224,
	"./ar-kw.js": 224,
	"./ar-ly": 225,
	"./ar-ly.js": 225,
	"./ar-ma": 226,
	"./ar-ma.js": 226,
	"./ar-sa": 227,
	"./ar-sa.js": 227,
	"./ar-tn": 228,
	"./ar-tn.js": 228,
	"./ar.js": 229,
	"./az": 230,
	"./az.js": 230,
	"./be": 231,
	"./be.js": 231,
	"./bg": 232,
	"./bg.js": 232,
	"./bn": 233,
	"./bn.js": 233,
	"./bo": 234,
	"./bo.js": 234,
	"./br": 235,
	"./br.js": 235,
	"./bs": 236,
	"./bs.js": 236,
	"./ca": 237,
	"./ca.js": 237,
	"./cs": 238,
	"./cs.js": 238,
	"./cv": 239,
	"./cv.js": 239,
	"./cy": 240,
	"./cy.js": 240,
	"./da": 241,
	"./da.js": 241,
	"./de": 244,
	"./de-at": 242,
	"./de-at.js": 242,
	"./de-ch": 243,
	"./de-ch.js": 243,
	"./de.js": 244,
	"./dv": 245,
	"./dv.js": 245,
	"./el": 246,
	"./el.js": 246,
	"./en-au": 247,
	"./en-au.js": 247,
	"./en-ca": 248,
	"./en-ca.js": 248,
	"./en-gb": 249,
	"./en-gb.js": 249,
	"./en-ie": 250,
	"./en-ie.js": 250,
	"./en-nz": 251,
	"./en-nz.js": 251,
	"./eo": 252,
	"./eo.js": 252,
	"./es": 254,
	"./es-do": 253,
	"./es-do.js": 253,
	"./es.js": 254,
	"./et": 255,
	"./et.js": 255,
	"./eu": 256,
	"./eu.js": 256,
	"./fa": 257,
	"./fa.js": 257,
	"./fi": 258,
	"./fi.js": 258,
	"./fo": 259,
	"./fo.js": 259,
	"./fr": 262,
	"./fr-ca": 260,
	"./fr-ca.js": 260,
	"./fr-ch": 261,
	"./fr-ch.js": 261,
	"./fr.js": 262,
	"./fy": 263,
	"./fy.js": 263,
	"./gd": 264,
	"./gd.js": 264,
	"./gl": 265,
	"./gl.js": 265,
	"./gom-latn": 266,
	"./gom-latn.js": 266,
	"./he": 267,
	"./he.js": 267,
	"./hi": 268,
	"./hi.js": 268,
	"./hr": 269,
	"./hr.js": 269,
	"./hu": 270,
	"./hu.js": 270,
	"./hy-am": 271,
	"./hy-am.js": 271,
	"./id": 272,
	"./id.js": 272,
	"./is": 273,
	"./is.js": 273,
	"./it": 274,
	"./it.js": 274,
	"./ja": 275,
	"./ja.js": 275,
	"./jv": 276,
	"./jv.js": 276,
	"./ka": 277,
	"./ka.js": 277,
	"./kk": 278,
	"./kk.js": 278,
	"./km": 279,
	"./km.js": 279,
	"./kn": 280,
	"./kn.js": 280,
	"./ko": 281,
	"./ko.js": 281,
	"./ky": 282,
	"./ky.js": 282,
	"./lb": 283,
	"./lb.js": 283,
	"./lo": 284,
	"./lo.js": 284,
	"./lt": 285,
	"./lt.js": 285,
	"./lv": 286,
	"./lv.js": 286,
	"./me": 287,
	"./me.js": 287,
	"./mi": 288,
	"./mi.js": 288,
	"./mk": 289,
	"./mk.js": 289,
	"./ml": 290,
	"./ml.js": 290,
	"./mr": 291,
	"./mr.js": 291,
	"./ms": 293,
	"./ms-my": 292,
	"./ms-my.js": 292,
	"./ms.js": 293,
	"./my": 294,
	"./my.js": 294,
	"./nb": 295,
	"./nb.js": 295,
	"./ne": 296,
	"./ne.js": 296,
	"./nl": 298,
	"./nl-be": 297,
	"./nl-be.js": 297,
	"./nl.js": 298,
	"./nn": 299,
	"./nn.js": 299,
	"./pa-in": 300,
	"./pa-in.js": 300,
	"./pl": 301,
	"./pl.js": 301,
	"./pt": 303,
	"./pt-br": 302,
	"./pt-br.js": 302,
	"./pt.js": 303,
	"./ro": 304,
	"./ro.js": 304,
	"./ru": 305,
	"./ru.js": 305,
	"./sd": 306,
	"./sd.js": 306,
	"./se": 307,
	"./se.js": 307,
	"./si": 308,
	"./si.js": 308,
	"./sk": 309,
	"./sk.js": 309,
	"./sl": 310,
	"./sl.js": 310,
	"./sq": 311,
	"./sq.js": 311,
	"./sr": 313,
	"./sr-cyrl": 312,
	"./sr-cyrl.js": 312,
	"./sr.js": 313,
	"./ss": 314,
	"./ss.js": 314,
	"./sv": 315,
	"./sv.js": 315,
	"./sw": 316,
	"./sw.js": 316,
	"./ta": 317,
	"./ta.js": 317,
	"./te": 318,
	"./te.js": 318,
	"./tet": 319,
	"./tet.js": 319,
	"./th": 320,
	"./th.js": 320,
	"./tl-ph": 321,
	"./tl-ph.js": 321,
	"./tlh": 322,
	"./tlh.js": 322,
	"./tr": 323,
	"./tr.js": 323,
	"./tzl": 324,
	"./tzl.js": 324,
	"./tzm": 326,
	"./tzm-latn": 325,
	"./tzm-latn.js": 325,
	"./tzm.js": 326,
	"./uk": 327,
	"./uk.js": 327,
	"./ur": 328,
	"./ur.js": 328,
	"./uz": 330,
	"./uz-latn": 329,
	"./uz-latn.js": 329,
	"./uz.js": 330,
	"./vi": 331,
	"./vi.js": 331,
	"./x-pseudo": 332,
	"./x-pseudo.js": 332,
	"./yo": 333,
	"./yo.js": 333,
	"./zh-cn": 334,
	"./zh-cn.js": 334,
	"./zh-hk": 335,
	"./zh-hk.js": 335,
	"./zh-tw": 336,
	"./zh-tw.js": 336
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 502;


/***/ }),

/***/ 508:
/***/ (function(module, exports) {

module.exports = "\n<app-navbar></app-navbar>\n<router-outlet></router-outlet>\n<app-loader></app-loader>"

/***/ }),

/***/ 509:
/***/ (function(module, exports) {

module.exports = "<div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">\n  <div class=\"carousel-inner\" role=\"listbox\">\n    <div class=\"carousel-item active imgwrapper\">\n      <div class=\"container text-center\">\n        <div class=\"carousel-caption d-none d-md-block\">\n          <h1 class=\"caption\">FIND THE BEST</h1>\n          <h2 class=\"caption2\">OUT-THE-DOOR PRICING ON NEW CARS</h2>\n          <p class=\"rounded-circle btn btn-default padding\">\n            <a href=\"#down\" role=\"button\">\n              <span class=\"pageDownArrow\">\n                <i class=\"fa fa-chevron-down arrowFontSize\"></i>     \n            </span></a>\n          </p>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div class=\"div1\">\n  <h1 class=\"text-center heading\" *ngIf=\"searchbuttons\">FIND YOUR NEXT NEW CAR</h1>\n  <h1 class=\"text-center heading\" *ngIf=\"!searchbuttons\">{{ carNameAndModel }}</h1>\n  <div class=\"col-lg-10 col-md-12 seacrhingOptionsDiv col-sm-12 col-xs-12  search-fields\">\n\n    <div class=\"row userInputDiv\">\n\n      <div class=\"form-group field\">\n        <input class=\"form-control\" type=\"text\" placeholder=\"Zip Code\" [(ngModel)]=\"car.zip_code\">\n      </div>\n      <ng2-completer class=\"form-group field\" [(ngModel)]=\"car.brand\" (selected)=\"selectedBrand()\" [datasource]=\"brandsList\" [minSearchLength]=\"0\"\n        placeholder=\"Enter Make (Ex.Ford)\"></ng2-completer>\n      <ng2-completer class=\"form-group field\" [(ngModel)]=\"car.model\" (selected)=\"selectedModel()\" [datasource]=\"modelList\" [minSearchLength]=\"0\"\n        placeholder=\"Enter Make (Ex.Mustang)\"></ng2-completer>\n\n\n      <div class=\"form-group field\">\n        <div>\n          <select class=\"form-control\" [(ngModel)]=\"car.year\">\n           <option *ngFor=\"let years of modelYears\">{{ years.year }}</option>\n         </select>\n        </div>\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Trim\" [(ngModel)]=\"car.time\">\n      </div>\n    </div>\n    <div class=\"row\" *ngIf=\"showMoreOptions\" id=\"option\">\n      <div class=\"form-group field\">\n        <input class=\"form-control\" type=\"text\" placeholder=\"Exterior\" [(ngModel)]=\"car.exterior\">\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Interior\" [(ngModel)]=\"car.interior\">\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Transmission\" [(ngModel)]=\"car.transmission\">\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Engine\" [(ngModel)]=\"car.engine\">\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Body Style\" [(ngModel)]=\"car.body_style\">\n      </div>\n    </div>\n\n    <div class=\"moreFeaturesDiv\" *ngIf=\"features\">\n      <h4 class=\"col-sm-12 subHeading\">Options & Packages</h4>\n      <hr>\n      <div class=\"row col-sm-12\">\n        <h5 class=\"col-sm-12\">Equipments Groups</h5>\n        <ul class=\"row\">\n          <li class=\"col-sm-12 col-md-12 col-lg-4\" *ngFor=\"let item of equipGourp\">\n            <div class=\"checkbox abc-checkbox-circle\">\n              <input type=\"checkbox\" value=\"{{ item.name }}\" [(ngModel)]=\"item.status\">\n              <label>\n                    {{ item.name }}\n                 </label>\n            </div>\n          </li>\n        </ul>\n      </div>\n      <div class=\"row col-sm-12\">\n        <h5 class=\"col-sm-12\">More Features</h5>\n        <ul class=\"row\">\n          <li class=\"col-sm-12 col-md-12 col-lg-4\" *ngFor=\"let item of Features\">\n            <div class=\"checkbox abc-checkbox-circle\">\n              <input type=\"checkbox\" value=\"{{ item.name }}\" name=\"{{item.name}}\" [(ngModel)]=\"item.status\">\n              <label>\n                    {{ item.name }}\n                 </label>\n            </div>\n          </li>\n        </ul>\n      </div>\n    </div>\n    <div class=\"row searchbuttons\" *ngIf=\"searchbuttons\">\n      <div class=\"col-sm-12 col-md-12 col-lg-4 hidden-md-down hidden-sm-down\"></div>\n      <div class=\"col-sm-12 col-md-6 col-lg-4 text-center moreOptionsLinks\">\n        <p class=\"text-sm-center text-lg-right links\" *ngIf=\"addFeature\" (click)=\"moreOptions()\">MORE OPTIONS</p>\n        <p class=\"text-sm-center text-lg-right links\" *ngIf=\"addFeature2\" (click)=\"moreFeatures()\">ADD FEATURES AND OPTIONS</p>\n        <p class=\"text-sm-center text-lg-right links\" *ngIf=\"features\" (click)=\"hideFeatures()\">HIDE FEATURES</p>\n      </div>\n      <div class=\"col-sm-12 col-md-6 col-lg-4\">\n\n        <button class=\"btn theme-btn\" type=\"submit\" (click)=\"searchCar()\">\n            <div class=\"d-inline-block title\">SEARCH VEHICLE</div>\n            <span class=\"rounded-circle btn btn-primary pull-right\"> \n              <i class=\"fa fa-angle-right\"></i> \n            </span>\n        </button>\n\n      </div>\n    </div>\n  </div>\n\n</div>\n<div class=\"chooseCarDiv col-sm-12 col-md-12 col-lg-12\" *ngIf=\"hideBrands\">\n  <div class=\"row\">\n    <div class=\"col-lg-3 hidden-md-down brandDiv\">\n    </div>\n    <div class=\"hidden-md-down carDiv\">\n      <img src=\"./../../assets/Car.png\" class=\"img-responsive img-fluid brandCar\">\n    </div>\n    <div class=\"col-sm-12 col-md-12 col-lg-9 chooseABrandDiv\">\n      <h2 class=\"offset-md-3\">PRICE A NEW CAR. CHOOSE A BRAND.</h2>\n      <div class=\"col-sm-12 col-md-12 col-lg-7 offset-lg-5 carLinks\">\n        <div class=\"row\">\n          <ul class=\"row\">\n            <li class=\"col-xs-4 col-sm-4 col-md-6 col-lg-4 text-center\" *ngFor=\"let brand of brands\"><a (click)=\"getBrand(brand.name)\">{{ brand.name}} </a></li>\n          </ul>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n\n<app-news-letter id=\"down\"></app-news-letter>\n<app-footer-links></app-footer-links>"

/***/ }),

/***/ 510:
/***/ (function(module, exports) {

module.exports = "<div class=\"row footer\">\n    <div class=\"col-sm-12 col-md-6 col-lg-2 offset-lg-2  text-center\">\n        <h4 class=\"footerHeading\">About Enjoy A Car</h4>\n        <ul>\n            <li><a href=\"#\" class=\"footerLink\">FAQ</a></li>\n            <li><a href=\"#\" class=\"footerLink\">Our Story</a></li>\n            <li><a href=\"#\" class=\"footerLink\">How it Works</a></li>\n            <li><a href=\"#\" class=\"footerLink\">Why it Works</a></li>\n        </ul>\n    </div>\n    <div class=\"col-sm-12 col-md-6 col-lg-2 text-center \">\n        <h4 class=\"footerHeading\">Enjoy A Car Deallers</h4>\n        <ul>\n            <li><a  routerLink=\"/signup\">Request To Join</a></li>\n            <li><a href=\"#\" class=\"footerLink\">Dealer Portal</a></li>\n            <li><a href=\"#\" class=\"footerLink\">FAQ</a></li>\n        </ul>\n    </div>\n\n    <div class=\"clearfix hidden-sm-up\"></div>\n\n    <div class=\"col-sm-12 col-md-6 col-lg-2 text-center \">\n        <h4 class=\"footerHeading\">Media & Press</h4>\n        <ul>\n            <li><a href=\"#\" class=\"footerLink\">In The News</a></li>\n            <li><a href=\"#\" class=\"footerLink\">EnjoyACar Blog</a></li>\n            <li><a href=\"#\" class=\"footerLink\">Press KIT</a></li>\n        </ul>\n    </div>\n    <div class=\"col-sm-12 col-md-6 col-lg-2 text-center \">\n        <h4 class=\"footerHeading\">We Care</h4>\n        <ul>\n            <li><a href=\"#\" class=\"footerLink\">Your Anonymity</a></li>\n            <li><a href=\"#\" class=\"footerLink\">Contect Us</a></li>\n            <li><a href=\"#\" class=\"footerLink\">FeedBack</a></li>\n            <a hre=\"#\">\n                <li>\n                    <a hre=\"#\"><i class=\"fa fa-facebook social\" aria-hidden=\"true\"></i></a>\n                    <a hre=\"#\"><i class=\"fa fa-twitter social\" aria-hidden=\"true\"></i></a>\n                </li>\n            </a>\n        </ul>\n    </div>\n</div>\n\n\n<div class=\"row copyright \">\n    <div class=\"col-md-4 offset-lg-1 text-center\">Copyright &copy; EnjoyACar Inc, All rights reserved 2017.</div>\n    <div class=\"col-md-4 offset-md-2 text-center\">Terms of Service | <a routerLink=\"/policy\">Privacy and Policy</a></div>\n</div>"

/***/ }),

/***/ 511:
/***/ (function(module, exports) {

module.exports = "<div class=\"mainDiv col-sm-12 row imgwrapper\">\n  <div class=\"col-lg-4 hidden-md-down hidden-sm-down\"></div>\n  <div class=\"col-sm-12 col-md-12 col-lg-4\">\n    <div class=\"loginDiv\">\n      <h1>FORGET YOUR PASSWORD</h1>\n      <div>\n        <form class=\"loginForm\" [formGroup]=\"form\">\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"emai\" name=\"email\" placeholder=\"Enter your Email Address\" [formControl]=\"form.controls['email']\"\n              [(ngModel)]=\"user.email\">\n          </div>\n          <div class=\"alert alert-danger text-center\" role=\"alert\" *ngIf=\"Msg\" style=\"border-radius: 50px \">\n            {{ Msg }}\n          </div>\n          <div class=\"row text-center\">\n            <div class=\"form-group col-lg-12\">\n              <button class=\"btn theme-btn\" [disabled]=\"!form.valid\" (click)=\"onSubmit()\">\n                  <div class=\"d-inline-block title\">SUBMIT</div>\n                  <span class=\"rounded-circle btn btn-primary pull-right\"> \n                    <i class=\"fa fa-angle-right\"></i> \n                  </span>\n              </button>\n            </div>\n          </div>\n          <div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-lg-4 hidden-md-down hidden-sm-down\"></div>\n</div>"

/***/ }),

/***/ 512:
/***/ (function(module, exports) {

module.exports = "<div class=\"loading\" *ngIf=\"active\" >\n    <img src=\"../../assets/loader.gif\" >\n</div>"

/***/ }),

/***/ 513:
/***/ (function(module, exports) {

module.exports = "<div class=\"mainDiv col-sm-12 row imgwrapper\">\n  <div class=\"col-lg-4 hidden-md-down hidden-sm-down\"></div>\n  <div class=\"col-sm-12 col-md-12 col-lg-4\">\n    <div class=\"loginDiv\">\n      <h1>LOGIN</h1>\n      <div>\n        <form class=\"loginForm\" [formGroup]=\"form\">\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"email\" [formControl]=\"form.controls['email']\" placeholder=\"Email Address\" [(ngModel)]=\"user.email\">\n          </div>\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"password\" [formControl]=\"form.controls['password']\" placeholder=\"Password\" name=\"password\"\n              [(ngModel)]=\"user.password\">\n          </div>\n          <div class=\"alert alert-danger text-center\" role=\"alert\" *ngIf=\"Msg\" style=\"border-radius: 50px \">\n            {{ Msg }}\n          </div>\n          <div>\n            <p style=\"text-align: right;padding: 10px;\"> <a href=\"#\" style=\"color: silver\" routerLink=\"/forgetpassword\">Reset your password</a></p>\n          </div>\n          <div class=\"row text-center auth-buttons\">\n            <div class=\"form-group col-lg-6\">\n\n              <button class=\"btn theme-btn\" (click)=\"login()\">\n                  <div class=\"d-inline-block title\">LOGIN</div>\n                    <span class=\"rounded-circle btn btn-primary pull-right\"> \n                      <i class=\"fa fa-angle-right\"></i> \n                    </span>\n              </button>\n\n            </div>\n            <div class=\"form-group col-lg-6\">\n\n              <button class=\"btn theme-btn\" routerLink=\"/signup\">\n                  <div class=\"d-inline-block title\">SIGN UP</div>\n                    <span class=\"rounded-circle btn btn-primary pull-right\"> \n                      <i class=\"fa fa-angle-right\"></i> \n                    </span>\n              </button>\n\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-lg-4 hidden-md-down hidden-sm-down\"></div>\n</div>"

/***/ }),

/***/ 514:
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse\" style=\"opacity: 0.7\" *ngIf=\"variableService.getNav()\">\n  <button class=\"navbar-toggler navbar-toggler-right\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\" aria-controls=\"navbarCollapse\"\n    aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n        <span class=\"navbar-toggler-icon\"></span>\n      </button>\n  <a class=\"navbar-brand\" routerLink=\"/\">EnjoyACar<sup style=\"font-size: 10px\">TM</sup></a>\n  <div class=\"collapse navbar-collapse\" id=\"navbarCollapse\">\n    <ul class=\"navbar-nav mr-auto\">\n      <li class=\"nav-item hidden-lg-up text-center\" *ngIf=\"!variableService.getIsLogin()\">\n        <a class=\"nav-link\" routerLink=\"/login\">SIGN IN</a>\n      </li>\n      <li class=\"nav-item hidden-lg-up text-center\" *ngIf=\"!variableService.getIsLogin()\">\n        <a class=\"nav-link\" routerLink=\"/signup\">REGISTER</a>\n      </li>\n      <li class=\"nav-item hidden-lg-up text-center\" *ngIf=\"variableService.getIsLogin()\">\n        <a class=\"nav-link\" routerLink=\"/logout\">Logout</a>\n      </li>\n\n      <li class=\"nav-item hidden-lg-up text-center\" *ngIf=\"variableService.getIsLogin()\">\n        <a class=\"nav-link\" routerLink=\"/notification\">Notifications  <span class=\"badge badge-danger\">{{ this.variableService.notificationsCount }}</span></a>\n      </li>\n\n    </ul>\n    <form class=\"form-inline mt-2 mt-md-0\" *ngIf=\"!variableService.getIsLogin()\">\n      <button class=\"btn btn-outline-secondary my-2 my-sm-0 hidden-md-down\" type=\"submit\" routerLink=\"/login\">SIGN IN</button>\n      <button class=\"btn btn-primary my-2 my-sm-0 hidden-md-down\" type=\"submit\" routerLink=\"/signup\">REGISTER</button>\n    </form>\n\n    <form class=\"form-inline mt-2 mt-md-0\" *ngIf=\"variableService.getIsLogin()\">\n      <button class=\"btn btn-outline-danger my-2 my-sm-0 hidden-md-down\" id=\"logout\" type=\"submit\" (click)=\"logout()\">LOGOUT</button>\n      <button class=\"btn btn-outline-primary my-2 my-sm-0  hidden-md-down\" routerLink=\"/notification\">NOTIFICATIONS <span class=\"badge badge-danger\">{{ this.variableService.notificationsCount }}</span></button>\n\n    </form>\n\n  </div>\n</nav>"

/***/ }),

/***/ 515:
/***/ (function(module, exports) {

module.exports = "<!--<Already Subscribe Email Modal>-->\n\n<modal #alreadySubscribeModal>\n  <modal-body class=\"modal-body\">\n    <div>\n      This Email is Already Subcribe for Our NewsLetter\n    </div>\n  </modal-body>\n  <modal-footer>\n    <button class=\"btn btn-outline-secondary btn-lg\" (click)=\"alreadySubscribeModal.close()\">OK</button>\n  </modal-footer>\n</modal>\n\n\n<!--<Already Subscribe Email Modal>-->\n\n\n<!--<Success On Submit Modal>-->\n\n<modal #success>\n <modal-header style=\"background-color: #3498db\">\n   <div class=\"modal-title\" style=\"font-size: 20px;color: white\">Successfully Subscribe !</div>\n </modal-header>\n <modal-body class=\"modal-body\">\n   <div>\n     Your Have Successfully Subscribe to EnjoyACar NewsLetter\n   </div>\n </modal-body>\n <modal-footer>\n     <button class=\"btn btn-outline-primary\" (click)=\"success.close()\">OK</button>\n </modal-footer>\n</modal>\n\n<!--<Success On Submit Modal>-->\n\n\n<div class=\"row newsLetter\">\n    <div class=\"col-lg-4 col-sm-12 col-md-12 text-center offset-lg-1\">\n        <span class=\"fa fa-envelope envelope-icon\" aria-hidden=\"true\"></span>\n        <h3 class=\"newsLetterHeading\"> GET OUR NEWSLETTER</h3>\n        <div class=\"dumy-text\">Lorem Ipsum is simply dummy text </div>\n    </div>\n\n    <div class=\"col-lg-6 col-sm-9 offset-sm-3 offset-lg-1\" id=\"formSection\">\n        <form class=\"form-horizontal\" role=\"form\">\n            <div class=\"row\">\n                <div class=\"col-sm-12 col-lg-4 \">\n                    <div class=\"form-group name\">\n                        <div class=\"col-md-12\">\n                            <input type=\"email\" class=\"form-control newLetterInput\" placeholder=\"Name\"  [formControl]=\"form.controls['name']\" [(ngModel)]=\"user.name\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-sm-12 col-lg-4 \">\n                    <div class=\"form-group email\">\n                        <div class=\"col-md-12\">\n                            <input type=\"email\" class=\"form-control newLetterInput\"  [formControl]=\"form.controls['email']\" placeholder=\"Your email address\" [(ngModel)]=\"user.email\">\n                        </div>\n                    </div>\n                </div>\n                <div class=\"col-sm-12 col-lg-3 \">\n                    <div class=\"form-group\">\n                        <div class=\"col-md-12\">\n                            <input type=\"button\" class=\"form-control btn btn-primary subcribe-btn\" value=\"Subscribe\" [disabled]=\"!isEmailValid()\"\n                            (click)=\"onsubmit()\">\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <!-- /.row this actually does not appear to be needed with the form-horizontal -->\n        </form>\n    </div>\n</div>"

/***/ }),

/***/ 516:
/***/ (function(module, exports) {

module.exports = "<div class=\"mainDiv\">\n  <div class=\"notificationsDiv\" *ngIf=\"totalNotifications === 0\">\n    <h1>No New Notifications </h1>\n  </div>\n\n  <div class=\"notificationsDiv\" *ngIf=\"totalNotifications !== 0\">\n    <div class=\"row notificationHeaderDiv\">\n      <h1 class=\"col-sm-12 col-md-7 col-lg-5 offset-lg-1  notificationHeading\">Your Notifications </h1>\n      <div class=\"col-sm-12 col-md-4 col-lg-4 offset-md-1 offset-lg-1\"><button class=\"btn btn-outline-danger notificationRead\" (click)=\"allRead()\">Mark all as Read</button></div>\n    </div>\n    <div class=\"col-sm-12 col-md-12 col-lg-12 row listOfNotifications\" *ngFor=\"let num of notificationList ;let i = index\">\n      <div class=\"col-sm-12 col-md-12 col-lg-6 message\">{{num.message}}</div>\n      <div class=\"col-sm-12 col-md-12 col-lg-2 status\" *ngIf=\"num.is_read\"><button class=\"btn btn-success\">Read</button></div>\n      <div class=\"col-sm-12 col-md-12 col-lg-2 time\">{{num.created_at | amTimeAgo }} </div>\n      <div class=\"col-sm-12 col-md-12 col-lg-2 status\" *ngIf=\"!num.is_read\"><button class=\"btn btn-outline-danger\">Unread</button></div>\n    </div>\n  </div>\n</div>\n\n<div class=\"pagination\" *ngIf=\"totalServepage > 1\">\n  <nav class=\"text-sm-center text-md-right text-lg-right\" style=\"margin: auto\">\n    <ul class=\"pagination\">\n      <li class=\"page-item\">\n        <a class=\"page-link\" (click)=\"previousPage()\" tabindex=\"-1\" *ngIf=\"currentPage !== 1\">Previous</a>\n      </li>\n      <li class=\"page-item\" *ngFor=\"let page of totalPages\"><a class=\"page-link\" [ngClass]=\"{'PageActive': active(page.page)}\" (click)=\"getPage(page.page)\">{{ page.page }}</a></li>\n      <li class=\"page-item\">\n        <a class=\"page-link\" (click)=\"nextPage()\" *ngIf=\"currentPage !== totalPages.length\">Next</a>\n      </li>\n    </ul>\n  </nav>\n</div>\n\n<app-news-letter></app-news-letter>\n<app-footer-links></app-footer-links>"

/***/ }),

/***/ 517:
/***/ (function(module, exports) {

module.exports = "<div class=\"banner\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"col-lg-8 offset-lg-2 col-md-10 headingDiv\">\n        <div class=\"heading\"> Privacy Policy</div>\n        <div class=\"date\">Effective April 24, 2017</div>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"container\">\n  <div class=\"row bodyTextFont\">\n    <div class=\"col-lg-8 offset-lg-2 col-md-10 \">\n      <div class=\"col-lg-12 bodyText\">\n        <div class=\"privacyHeading\"> Privacy Notice </div>This privacy notice discloses the privacy practices for EnjoyACar, Inc. and our website; http://www.enjoyacar.com.\n        This privacy notice applies solely to information collected by this website, except where stated otherwise. It will\n        notify you of the following: What information we collect; With whom it is shared; How it can be corrected; How it\n        is secured; How policy changes will be communicated; and How to address concerns over misuse of personal data.\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Information Collection, Use, and Sharing We are the sole owners of the information collected on this site. We only have access\n        to/collect information that you voluntarily give us via email or other direct contact from you. We will not sell\n        or rent this information to anyone. We will use your information to respond to you, regarding the reason you contacted\n        us. We will not share your information with any third party outside of our organization, other than as necessary\n        to fulfill your request, e.g., to ship an order. Unless you ask us not to, we may contact you via email in the future\n        to tell you about specials, new products or services, or changes to this privacy policy.\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Your Access to and Control Over Information You may opt out of any future contacts from us at any time. You can do the following\n        at any time by contacting us via the email address or phone number provided on our website: See what data we have\n        about you, if any. Change/correct any data we have about you. Have us delete any data we have about you. Express\n        any concern you have about our use of your data\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Registration In order to use this website, a user must first complete the registration form. During registration a user is\n        required to give certain information (such as name and email address). This information is used to contact you about\n        the products/services on our site in which you have expressed interest. At your option, you may also provide demographic\n        information (such as gender or age) about yourself, but it is not required.\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Orders We request information from you on our order form. To buy from us, you must provide contact information (like name\n        and shipping address) and financial information (like credit card number, expiration date). This information is used\n        for billing purposes and to fill your orders. If we have trouble processing an order, we'll use this information\n        to contact you.\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Sharing We share aggregated demographic information with our partners and advertisers. This is not linked to any personal\n        information that can identify any individual person. And/or:\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        We use an outside shipping company to ship orders, and a credit card processing company to bill users for goods and services.\n        These companies do not retain, share, store or use personally identifiable information for any secondary purposes\n        beyond filling your order.\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        And/or: We partner with another party to provide specific services. When the user signs up for these services, we will share\n        names, or other contact information that is necessary for the third party to provide these services. These parties\n        are not allowed to use personally identifiable information except for the purpose of providing these services.\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Security We take precautions to protect your information. When you submit sensitive information via the website, your information\n        is protected both online and offline. Wherever we collect sensitive information (such as credit card data), that\n        information is encrypted and transmitted to us in a secure way. You can verify this by looking for a closed lock\n        icon at the bottom of your web browser, or looking for \"https\" at the beginning of the address of the web page. While\n        we use encryption to protect sensitive information transmitted online, we also protect your information offline.\n        Only employees who need the information to perform a specific job (e.g. billing or customer service) are granted\n        access to personally identifiable information. The computers/servers on which we store personally identifiable information\n        are kept in a secure environment.\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Cookies We use \"cookies\" on this site. A cookie is a piece of data stored on a site visitor's hard drive to help us improve\n        your access to our site and identify repeat visitors to our site. For instance, when we use a cookie to identify\n        you, you would not have to log in a password more than once, thereby saving time while on our site. Cookies can also\n        enable us to track and target the interests of our users to enhance their experience on our site. Usage of a cookie\n        is in no way linked to any personally identifiable information on our site. Some of our business partners may use\n        cookies on our site (e.g., advertisers). However, we have no access to or control over these cookies.\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Links This web site contains links to other sites. Please be aware that we are not responsible for the content or privacy\n        practices of such other sites. We encourage our users to be aware when they leave our site and to read the privacy\n        statements of any other site that collects personally identifiable information.\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Surveys & Contests From time-to-time our site requests information via surveys or contests. Participation in these surveys\n        or contests is completely voluntary and you may choose whether or not to participate and therefore disclose this\n        information. Information requested may include contact information (such as name and shipping address), and demographic\n        information (such as zip code, age level). Contact information will be used to notify the winners and award prizes.\n        Survey information will be used for purposes of monitoring or improving the use and satisfaction of this site. Notification\n        of Changes Whenever material changes are made to the privacy notice specify how you will notify consumers. Other\n        Provisions as Required by Law\n      </div>\n      <div class=\"col-lg-12 bodyText\">\n        Numerous other provisions and/or practices may be required as a result of laws, international treaties, or industry practices.\n        It is up to you to determine what additional practices must be followed and/or what additional disclosures are required.\n        Please take special notice of the California Online Privacy Protection Act (CalOPPA), which is frequently amended\n        and now includes a disclosure requirement for “Do Not Track” signals. If you feel that we are not abiding by this\n        privacy policy, you should contact us immediately via telephone at 626-498-7852 or via email at info@enjoyacar.com\n        .\n      </div>\n    </div>\n  </div>\n</div>\n<app-footer-links></app-footer-links>"

/***/ }),

/***/ 518:
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center\" style=\"font-size: 30px;margin-top: 100px;\"> Your Email Has Been Verify ! <br>\n    Redirecting .....\n</div>"

/***/ }),

/***/ 519:
/***/ (function(module, exports) {

module.exports = "<div class=\"mainDiv col-sm-12 row imgwrapper\">\n <div class=\"col-lg-4 hidden-md-down hidden-sm-down\"></div>\n <div class=\"col-sm-12 col-md-12 col-lg-4\">\n   <div class=\"loginDiv\">\n     <h1>RESET YOUR  PASSWORD</h1>\n     <div>\n       <form>\n         <div class=\"form-group\">\n           <input class=\"form-control\" type=\"password\" name=\"email\" placeholder=\"Enter your password\" [(ngModel)]=\"newPasswordObj.pass1\">\n         </div>\n         <div class=\"form-group\">\n           <input class=\"form-control\" type=\"password\" name=\"confirmPassword\" placeholder=\"Confirm password\" [(ngModel)]=\"newPasswordObj.pass2\">\n         </div>\n         <div class=\"row text-center\">\n           <div class=\"form-group col-lg-12\">\n             <button class=\"form-control\" class=\"btn btn-secondary button\" type=\"submit\" (click)=\"onChange()\">\n             SUBMIT<span class=\"rounded-circle btn btn-secondary\"> <i class=\"fa fa-chevron-right\"></i> </span></button>\n           </div>\n         </div>\n           <div class=\"alert alert-danger\" role=\"alert\" *ngIf=\"Msg\" style=\"border-radius: 50px \">\n             {{ Msg }}\n           </div>\n           <div>\n           </div>\n       </form>\n       </div>\n     </div>\n   </div>\n   <div class=\"col-lg-4 hidden-md-down hidden-sm-down\"></div>\n </div>"

/***/ }),

/***/ 520:
/***/ (function(module, exports) {

module.exports = "<!--<Connect Modal>-->\n<modal #connectModal>\n  <modal-header>\n    <h4 class=\"modal-title\">Please Enter the Following Details</h4>\n  </modal-header>\n  <modal-body class=\"modal-body\">\n    <div class=\"row col-sm-12 col-md-12 col-lg-12\">\n      <div class=\"form-group col-sm-12 col-md-6 col-lg-6\">\n        <sup>*</sup><label>Name on card</label>\n        <select class=\"form-control\" [formControl]=\"form.controls['card']\" [(ngModel)]=\"connectFormData.card\">\n             <option>Visa</option>\n             <option>Master</option>\n          </select>\n      </div>\n      <div class=\"form-group col-sm-12 col-md-6 col-lg-6\">\n        <sup>*</sup>\n        <label>Email</label>\n        <input type=\"email\" class=\"form-control\" placeholder=\"Email Address for Dealer info\" [formControl]=\"form.controls['email']\"\n          [(ngModel)]=\"connectFormData.email\">\n        <div class=\"text-center error\" *ngIf=\"emailError\">Invalid Email</div>\n      </div>\n    </div>\n    <div class=\"row col-sm-12 col-md-12 col-lg-12\">\n      <div class=\"form-group col-sm-12 col-md-6 col-lg-6\">\n        <sup>*</sup>\n        <label>Card Number</label>\n        <input class=\"form-control\" type=\"text\" placeholder=\"Card Number\" [formControl]=\"form.controls['card_number']\" [(ngModel)]=\"connectFormData.card_number\">\n        <div class=\"text-center error\" *ngIf=\"cardNumError\">Invalid card number</div>\n      </div>\n      <div class=\"form-group col-sm-12 col-md-6 col-lg-6\">\n        <sup>*</sup><label>Phone Number</label>\n        <input type=\"number\" class=\"form-control\" placeholder=\"Phone Number for Dealer info\" [formControl]=\"form.controls['phone']\"\n          [(ngModel)]=\"connectFormData.phone\">\n        <div class=\"text-center error\" *ngIf=\"phoneError\">Invalid Phone Number</div>\n      </div>\n    </div>\n    <div class=\"row col-sm-12 col-md-12 col-lg-12 row\">\n      <div class=\"form-group col-sm-12 col-md-6 col-lg-6 row\">\n        <div class=\"col-sm-6 row\">\n          <div class=\"col-sm-6 form-group\">\n            <sup>*</sup><label>MM</label>\n            <select class=\"form-control\" id=\"month\" [formControl]=\"form.controls['month']\" [(ngModel)]=\"expire.month\">\n             <option>1 </option>\n             <option>2 </option>\n             <option>3 </option>\n             <option>4 </option>\n             <option>5 </option>\n             <option>6 </option>\n             <option>7 </option>\n             <option>8 </option>\n             <option>9 </option>\n             <option>10 </option>\n             <option>11 </option>\n             <option>12 </option>\n           </select>\n          </div>\n          <div class=\"col-sm-6 form-group\">\n            <sup>*</sup><label>YY</label>\n            <select class=\"form-control\" id=\"years\" [formControl]=\"form.controls['year']\" [(ngModel)]=\"expire.year\">\n             <option *ngFor=\"let year of expireYears\">{{year}} </option>\n           </select>\n          </div>\n        </div>\n\n        <div class=\"col-sm-12 col-md-6 col-lg-6\">\n          <sup>*</sup><label>CVC Code</label>\n          <input type=\"password\" class=\"form-control\" placeholder=\"Security Code\" [formControl]=\"form.controls['pin_code']\" [(ngModel)]=\"connectFormData.pin_code\">\n          <div class=\"text-center error\" *ngIf=\"cvcError\">Invalid CVC</div>\n        </div>\n      </div>\n      <div class=\"form-group col-sm-12 col-md-6 col-lg-6\" id=\"nameInput\">\n        <sup>*</sup><label>Name</label>\n        <input class=\"form-control\" type=\"text\" placeholder=\"Name Dealer will Contact\" [formControl]=\"form.controls['name']\" [(ngModel)]=\"connectFormData.name\">\n      </div>\n    </div>\n    <div class=\"row col-sm-12 col-md-12 col-lg-12\">\n      <div class=\"form-group col-sm-12 col-md-6 col-lg-6\">\n        <sup>*</sup><label>ZIP/Postal Code</label>\n        <input type=\"number\" class=\"form-control\" placeholder=\"ZIP/Postal Code for card\" [formControl]=\"form.controls['postal_code']\"\n          [(ngModel)]=\"connectFormData.postal_code\">\n      </div>\n      <div class=\"form-group col-sm-12 col-md-6 col-lg-6\">\n        <sup>*</sup><label>Zip Code</label>\n        <input class=\"form-control\" type=\"number\" placeholder=\"Zip Code - Where you live\" [formControl]=\"form.controls['zip_code']\"\n          [(ngModel)]=\"connectFormData.zip_code\">\n      </div>\n    </div>\n    <hr>\n    <div class=\" col-sm-12 col-md-12 col-lg-12\">\n      <p style=\"text-align: center;text-transform: capitalize\">Will we send the salesperson at the dealership a notification. If they accept the connection you will br charged ${{\n        connectPrice }}. None of your personal information will be given to the dealer except your ZIP Code ,so that they\n        may verify the availibility of any discounts requested until they accept. By clicking connect you agree with our\n        <a href=\"#\">Terms and Service</a> and <a href=\"#\">Privacy Policy</a>\n      </p>\n    </div>\n  </modal-body>\n  <modal-footer class=\"modal-footer  col-sm-12 col-md-12 col-lg-12\">\n    <button type=\"button\" class=\"btn btn-outline-secondary btn-lg\" (click)=\"connectModal.close()\">Cancel</button>\n    <button type=\"button\" class=\"btn btn-outline-success btn-lg connectPayBtton\" (click)=\"connectPayBtton()\" [disabled]=\"!isCardNumValid()  || !isEmailValid() || !isPhoneNumValid() || !isCVCValid() || !connectFormData.card || !connectFormData.zip_code || !connectFormData.postal_code || !connectFormData.name\">\n           PAY\n    </button>\n\n  </modal-footer>\n</modal>\n\n<!--<Success On Connect Modal>-->\n\n<modal #success>\n  <modal-header class=\"successModal\">\n    <div class=\"modal-title successFullContected\">Successfully Connected !</div>\n  </modal-header>\n  <modal-body class=\"modal-body\">\n    <div>\n      Your Connect Has Been Sent To Dealer\n    </div>\n  </modal-body>\n  <modal-footer>\n    <button class=\"btn btn-outline-success btn-lg\" (click)=\"success.close()\">OK</button>\n  </modal-footer>\n</modal>\n\n\n<!--<Success On Request Modal>-->\n\n\n<modal #reqModal>\n  <modal-header class=\"requestModal\">\n    <h4 class=\"modal-title\">REQUEST FOR OFFERS</h4>\n  </modal-header>\n  <modal-body class=\"modal-body\">\n    <p class=\"modalContent\">Thank for your interest. It seems like there aren't enough options for you.Dealers want to make sure there's interest\n      for a car before posting them. We'll let dealers in your state know that you want them to post more. We wont give them\n      your email but will update you for 72 hours as dealers post them\n    </p> <br>\n    <input class=\"reqEmail\" type=\"email\" placeholder=\"Enter Email Address\" [formControl]=\"form.controls['reqEmail']\" [(ngModel)]=\"requestModalForm.email \">\n  </modal-body>\n  <modal-footer>\n    <div>\n      <button type=\"button\" class=\"btn btn-secondary btn-lg\" (click)=\"reqModal.close()\">Close</button>\n      <button type=\"button\" class=\"btn btn-primary btn-lg\" [disabled]=\"!isEmailRequestValid()\" (click)=\"reqOfferModal()\">  Request More Offers</button>\n      <div class=\"modalSlogan\">This doesn't sign up for any newsletter. Read our <a routerLink=\"/policy\">Privacy Policy</a></div>\n    </div>\n  </modal-footer>\n</modal>\n<!--</Req Modal>-->\n\n\n<!--<Success On Connect Modal>-->\n\n<modal #successRequest>\n  <modal-header class=\"successModal\">\n    <div class=\"modal-title successFullContected\">Successfully Request Offers !</div>\n  </modal-header>\n  <modal-body class=\"modal-body\">\n    <div>\n      Your Request Offers Has Been Sent To Dealer\n    </div>\n  </modal-body>\n  <modal-footer>\n    <button class=\"btn btn-outline-success btn-lg\" (click)=\"successRequest.close()\">OK</button>\n  </modal-footer>\n</modal>\n\n\n<!--<Success On Connect Modal>-->\n\n<!--<Already Subscribe Email Modal>-->\n\n<modal #alreadySubscribeModal>\n  <modal-body class=\"modal-body\">\n    <div>\n      This Email is Already Subcribe for more Offers\n    </div>\n  </modal-body>\n  <modal-footer>\n    <button class=\"btn btn-outline-secondary btn-lg\" (click)=\"alreadySubscribeModal.close()\">OK</button>\n  </modal-footer>\n</modal>\n\n\n<!--<Already Subscribe Email Modal>-->\n\n\n<div id=\"myCarousel\" class=\"carousel slide\" data-ride=\"carousel\">\n  <div class=\"carousel-inner\" role=\"listbox\">\n    <div class=\"carousel-item active imgwrapper\">\n      <img class=\"first-slide img-responsive img-fluid\" src=\"./../../assets/Car2.jpg\" alt=\"First slide\">\n      <div class=\"container text-center\">\n        <div class=\"carousel-caption d-none d-md-block\">\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n\n<!-- find new car-->\n<div class=\"div1\">\n  <h1 class=\"text-center heading\" *ngIf=\"searchQuery.zip_code\">\n    {{ searchQuery.brand }} {{ searchQuery.model }} - {{ searchQuery.zip_code }}</h1>\n  <h1 class=\"text-center heading\" *ngIf=\"!searchQuery.zip_code\">\n    {{ searchQuery.brand }} {{ searchQuery.model }}</h1>\n  <div class=\"offset-lg-1 col-lg-10 col-md-12 col-sm-12 col-xs-12 search-fields\">\n\n\n    <div class=\"row\">\n\n      <div class=\"form-group field\">\n        <input class=\"form-control\" type=\"text\" placeholder=\"Zip Code\" [(ngModel)]=\"searchQuery.zip_code\">\n      </div>\n      <ng2-completer class=\"form-group field\" [(ngModel)]=\"searchQuery.brand\" (selected)=\"selectedBrand()\" [datasource]=\"brandsList\"\n        [minSearchLength]=\"0\" placeholder=\"Enter Make (Ex.Ford)\" *ngIf=\"isDataLoaded\"></ng2-completer>\n      <ng2-completer class=\"form-group field\" [(ngModel)]=\"searchQuery.model\" (selected)=\"selectedModel()\" [datasource]=\"modelList\"\n        [minSearchLength]=\"0\" placeholder=\"Enter Make (Ex.Mustang)\" *ngIf=\"isDataLoaded\"></ng2-completer>\n\n\n      <div class=\"form-group field\">\n        <div>\n          <select class=\"form-control\" [(ngModel)]=\"searchQuery.year\" *ngIf=\"modelYears\">\n           <option *ngFor=\"let years of modelYears\">{{ years.year }}</option>\n          </select>\n          <select class=\"form-control\" [(ngModel)]=\"searchQuery.year\" *ngIf=\"!modelYears\">\n           <option></option>\n           <option *ngIf=\"!modelYears\" [hidden]=\"!modelYears\">{{ searchQuery.year }}</option>\n         </select>\n        </div>\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Trim\" [(ngModel)]=\"searchQuery.time\">\n      </div>\n    </div>\n\n    <div class=\"row\" *ngIf=\"showMoreOptions\" id=\"option\">\n      <div class=\"form-group field\">\n        <input class=\"form-control\" type=\"text\" placeholder=\"Exterior\" [(ngModel)]=\"searchQuery.exterior\">\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Interior\" [(ngModel)]=\"searchQuery.interior\">\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Transmission\" [(ngModel)]=\"searchQuery.transmission\">\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Engine\" [(ngModel)]=\"searchQuery.engine\">\n      </div>\n      <div class=\"form-group field\">\n        <input type=\"text\" class=\"form-control\" placeholder=\"Body Style\" [(ngModel)]=\"searchQuery.body_style\">\n      </div>\n    </div>\n\n    <div class=\"moreFeaturesDiv\" *ngIf=\"features\">\n      <h4 class=\"col-sm-12 subHeading\">Options & Packages</h4>\n      <hr>\n      <div class=\"col-sm-12\">\n        <h5 class=\"col-sm-12 row\">Equipments Groups</h5>\n        <ul class=\"row\">\n          <li class=\"col-sm-12 col-md-12 col-lg-4\" *ngFor=\"let item of carEquipGourp\">\n            <div class=\"checkbox abc-checkbox-circle\">\n              <input type=\"checkbox\" value=\"{{ item.name }}\" [(ngModel)]=\"item.status\">\n              <label>\n                    {{ item.name }}\n                 </label>\n            </div>\n          </li>\n        </ul>\n      </div>\n      <div class=\"col-sm-12\">\n        <h5 class=\"col-sm-12 row\">More Features</h5>\n\n        <ul class=\"row\">\n          <li class=\"col-sm-12 col-md-12 col-lg-4\" *ngFor=\"let items of carFeatures\">\n            <div class=\"checkbox abc-checkbox-circle\">\n              <input type=\"checkbox\" value=\"{{ items.name }}\" [(ngModel)]=\"items.status\">\n              <label>\n                    {{ items.name }}\n                 </label>\n            </div>\n          </li>\n        </ul>\n\n\n      </div>\n\n      <div class=\"col-sm-12\">\n        <h5 class=\"col-sm-12 row\">Discounts - searcher is responsible for qualifing.</h5>\n        <ul class=\"row\">\n          <li class=\"col-sm-12 col-md-12 col-lg-4\" *ngFor=\"let item of carDiscounts\">\n            <div class=\"checkbox abc-checkbox-circle\">\n              <input type=\"checkbox\" value=\"{{ item.name }}\" [(ngModel)]=\"item.status\">\n              <label>\n                   {{ item.name }}\n                </label>\n            </div>\n          </li>\n        </ul>\n      </div>\n    </div>\n    <div class=\"row search-action-btn\">\n      <div class=\"col-sm-12 col-md-6 col-lg-4 text-md-center margin requestBtn\">\n        <button class=\"btn theme-btn\" (click)=\"reqModal.open()\">\n         <div class=\"d-inline-block title\">REQUEST MORE OFFERS</div>\n          <span class=\"rounded-circle btn btn-primary pull-right\"> \n            <i class=\"fa fa-angle-right\"></i> \n          </span>\n        </button>\n      </div>\n      <div class=\"col-sm-12 col-md-6 col-lg-4 text-md-center\">\n        <button class=\"btn theme-btn\" (click)=\"carSearchUpdate()\">\n         <div class=\"d-inline-block title\"> UPDATE</div>\n          <span class=\"rounded-circle btn btn-primary pull-right\"> \n            <i class=\"fa fa-angle-right\"></i> \n          </span>\n        </button>\n      </div>\n      <div class=\"col-sm-12 col-md-6 col-lg-4 text-md-center marginAuto\">\n        <p class=\"moreOptions\" (click)=\"showAllOptions()\">DISCOUNTS,FEATURES & OPTIONS</p>\n      </div>\n    </div>\n\n  </div>\n\n   <!--  Searched Car Result -->\n\n  <div class=\"row hidden-lg-down pageTopDiv\">\n\n    <div class=\"col-lg-3 col-md-4 col-sm-2 offset-lg-1\" *ngIf=\"serverCount === 0\"><strong> Page 0 0f 0 </strong></div>\n    <div class=\"col-lg-3 col-md-4 col-sm-2 offset-lg-1\" *ngIf=\"serverCount >= 1\"><strong> Page {{ currentPage }} 0f {{ totalServepage }} </strong></div>\n    <div class=\"col-lg-5 col-md-6 col-sm-6\" *ngIf=\"serverCount < 1\"><strong>Showing 0 of 0 Offers </strong></div>\n    <div class=\"col-lg-5 col-md-6 col-sm-6\" *ngIf=\"serverCount >= 1\"><strong>Showing {{ listOfCarSearch.length }} of {{ serverCount }} Offers </strong></div>\n    <div class=\"col-lg-2 col-md-2 col-sm-4\">\n      <select class=\"form-control\" *ngIf=\"serverCount === 0\">\n                <option selected=\"selected\">Sort</option>\n      </select>\n      <select class=\"form-control\" *ngIf=\"serverCount >= 1\">\n                <option selected=\"selected\">Sort</option>\n      </select>\n    </div>\n\n  </div>\n\n  <div *ngIf=\"serverCount === 0\" class=\"noRecord\">No Record Found</div>\n\n  <div class=\"datailsMainDiv col-sm-12 col-md-12 col-lg-10 row\" *ngFor=\"let car of listOfCarSearch; let i = index\">\n    <div class=\"detailsDiv1 col-sm-12 col-md-12 col-lg-12 text-sm-center text-md-center text-lg-left\" (click)=\"showDetails(i)\">\n      <div>\n        <p class=\"estimateTime\">{{ car.created_at | amTimeAgo}}</p>\n        <strong>{{ car.year }} {{ car.brand }}</strong> <br> {{ car.time }} <br> <strong> Ext:</strong> {{ car.exterior }}\n        &ensp; <strong> Int:</strong> {{ car.interior }}\n        <br> {{ car.transmission }},{{ car.body_style }}\n      </div>\n      <br>\n      <div class=\"details\">\n        <strong> Features:</strong><span *ngFor=\"let item of car.features\"><span *ngIf=\"item.status\"> {{ item.name }} | </span></span><br>\n        <strong> Packages:</strong><span *ngFor=\"let item of car.equipments\"><span *ngIf=\"item.status\"> {{ item.name }} | </span></span><br>\n        <strong> Discounts:</strong><span *ngFor=\"let item of car.discounts\"><span *ngIf=\"item.status\"> {{ item.name }} | </span></span>\n      </div>\n    </div>\n    <div class=\"detailsDiv2 col-sm-12 col-md-12 col-lg-3\" *ngIf=\"car.distance\">\n      <div class=\"carDistance\">\n        <div class=\"text-center carDistanceText\"> {{ car.distance }} mi </div>\n      </div>\n    </div>\n    <div class=\"text-center connectButtons\">\n\n      <div *ngIf=\"!car.isCollapse\">\n        <button class=\"btn btn-outline-danger cancelBtn\" (click)=\"cancelConnect(i)\">\n                  Cancel\n        </button>\n        <button class=\"btn btn-outline-success\" (click)=\"openPaypal(car._id,car.user)\">\n                  Connect- $\n        </button>\n      </div>\n    </div>\n  </div>\n\n      <!--  pagination-->\n  <div class=\"pagination\" *ngIf=\"totalServepage > 1\">\n    <nav class=\"text-sm-center text-md-right text-lg-right\" class=\"marginAuto\">\n      <ul class=\"pagination\">\n        <li class=\"page-item\">\n          <a class=\"page-link\" (click)=\"previousPage()\" tabindex=\"-1\" *ngIf=\"currentPage !== 1\">Previous</a>\n        </li>\n        <li class=\"page-item\" *ngFor=\"let page of totalPages\"><a class=\"page-link\" [ngClass]=\"{'PageActive': active(page.page)}\" (click)=\"getPage(page.page)\">{{ page.page }}</a></li>\n        <li class=\"page-item\">\n          <a class=\"page-link\" (click)=\"nextPage()\" *ngIf=\"currentPage !== totalPages.length\">Next</a>\n        </li>\n      </ul>\n    </nav>\n  </div>\n\n\n\n\n</div>\n\n<app-news-letter></app-news-letter>\n<app-footer-links></app-footer-links>"

/***/ }),

/***/ 521:
/***/ (function(module, exports) {

module.exports = "<div class=\"mainDiv col-sm-12 row imgwrapper\">\n  <div class=\"col-lg-4 hidden-md-down hidden-sm-down\"></div>\n  <div class=\"col-sm-12 col-md-12 col-lg-4\">\n    <div class=\"signupDiv\">\n      <h1>SIGN UP</h1>\n      <div>\n        <form class=\"loginForm\" [formGroup]=\"form\">\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"text\" placeholder=\"First Name\" (keypress)=\"isNameValid()\" name=\"firstname\" [formControl]=\"form.controls['firstname']\"\n              [(ngModel)]=\"user.firstname\">\n          </div>\n          <div class=\"form-group\">\n            <div class=\"text-center\" style=\"color:#F74222;\" *ngIf=\"FirstNameError\">First Name should only contain text</div>\n          </div>\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"text\" placeholder=\"Last Name\" name=\"lastname\" (keypress)=\"isLastNameValid()\" [formControl]=\"form.controls['lastname']\"\n              [(ngModel)]=\"user.lastname\">\n          </div>\n          <div class=\"form-group\">\n            <div class=\"text-center\" style=\"color:#F74222;\" *ngIf=\"LastNameError\">Last Name should only contain text</div>\n          </div>\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"email\" placeholder=\"Email Address\" name=\"email\" [formControl]=\"form.controls['email']\"\n              [(ngModel)]=\"user.email\">\n          </div>\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"text\" placeholder=\"Age\" name=\"age\" (keypress)=\"isAgeValid()\" [formControl]=\"form.controls['age']\"\n              [(ngModel)]=\"user.age\">\n          </div>\n          <div class=\"form-group\">\n            <div class=\"text-center\" style=\"color:#F74222;\" *ngIf=\"AgeError\">Age should only contain number</div>\n          </div>\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"text\" placeholder=\"Zip Code\" (keypress)=\"isZipCodeValid()\" name=\"zip_code\" [formControl]=\"form.controls['zip_code']\"\n              [(ngModel)]=\"user.zip_code\">\n          </div>\n          <div class=\"form-group\">\n            <div class=\"text-center\" style=\"color:#F74222;\" *ngIf=\"zipError\">Zip Code Must be Number</div>\n          </div>\n          <div class=\"form-group\">\n            <select class=\"form-control\" [formControl]=\"form.controls['gender']\" [(ngModel)]=\"user.gender\">\n              <option selected=\"selected\">Gender</option>\n              <option value=\"male\">Male</option>\n              <option value=\"female\">Female</option>\n            </select>\n          </div>\n          <div class=\"form-group\">\n            <input class=\"form-control\" type=\"password\" placeholder=\"Password\" name=\"password\" [formControl]=\"form.controls['password']\"\n              [(ngModel)]=\"user.password\">\n          </div>\n          <div class=\"alert alert-danger text-center\" role=\"alert\" *ngIf=\"Msg\" style=\"border-radius: 50px \">\n            {{ Msg }}\n          </div>\n          <div class=\"row text-center\">\n            <div class=\"form-group col-lg-12\">\n\n              <button class=\"btn theme-btn\" [disabled]=\"!form.valid || !isEmailValid()\" (click)=\"onSubmit()\">\n                  <div class=\"d-inline-block title\">SIGN UP</div>\n                    <span class=\"rounded-circle btn btn-primary pull-right\"> \n                      <i class=\"fa fa-angle-right\"></i> \n                    </span>\n              </button>\n\n            </div>\n          </div>\n        </form>\n      </div>\n    </div>\n  </div>\n  <div class=\"col-lg-4 hidden-md-down hidden-sm-down\"></div>\n</div>"

/***/ }),

/***/ 771:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "Mercedes-Benz.df787495df2c562c41cf.jpg";

/***/ }),

/***/ 772:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "carbg.1ef582cf1f2418f30f81.jpg";

/***/ }),

/***/ 773:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "footer.b6b9fe469edb844ed6e0.jpg";

/***/ }),

/***/ 776:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(397);


/***/ }),

/***/ 81:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "bg.3b8aab2ee91003dce011.jpg";

/***/ })

},[776]);
//# sourceMappingURL=main.bundle.js.map