export const authReducer = (state = [], action) => {
    switch (action.type) {
        case 'LOGIN_INITIALS':
            return [
                ...state,
                 //Object.assign({}, { isLogin:true ,payload:action.payload })
                 Object.assign({}, { isLogin:false })
            ];
            case 'LOGIN_FAILD':
            return [
                ...state,
                 //Object.assign({}, { isLogin:true ,payload:action.payload })
                 Object.assign({}, { isLogin:false,loginFaild:true })
            ];
            case 'LOGIN_SUCCESS':
            return [
                ...state,
                 Object.assign({}, { isLogin:true ,payload:action.payload })
            ];



            case 'SIGNUP_INIT':
            return [
                ...state,
                Object.assign({}, { isSignUp: false })
            ];
              case 'SIGNUP_FAILD':
            return [
                ...state,
                Object.assign({}, { isSignUp: false })
            ];
            case 'SIGNUP_SUCCESS':
            return [
                ...state,
                Object.assign({}, { isSignUp: true ,payload:action.payload  })
            ];



            case 'FORGETPASSWORD_INIT':
            return [
                ...state,
                Object.assign({}, { forgetPasswordCallSubmit: false })
            ];
              case 'FORGETPASSWORD_FAILD':
            return [
                ...state,
                Object.assign({}, { forgetPasswordCallSubmit: false })
            ];
            case 'FORGETPASSWORD_SUCCESS':
            return [
                ...state,
                Object.assign({}, { forgetPasswordCallSubmit: true ,payload:action.payload  })
            ];



            case 'RESETPASSWORD_INIT':
            return [
                ...state,
                Object.assign({}, { resetPassword: false })
            ];
              case 'RESETPASSWORD_FAILD':
            return [
                ...state,
                Object.assign({}, { resetPassword: false })
            ];
            case 'RESETPASSWORD_SUCCESS':
            return [
                ...state,
                Object.assign({}, { resetPassword: true ,payload:action.payload })
            ];




            
             case 'CONFIRMATION_INIT':
            return [
                ...state,
                Object.assign({}, { tokenConfirmation: false })
            ];
              case 'CONFIRMATION_FAILD':
            return [
                ...state,
                Object.assign({}, { tokenConfirmation: false })
            ];
            case 'CONFIRMATION_SUCCESS':
            return [
                ...state,
                Object.assign({}, { tokenConfirmation: true ,payload:action.payload })
            ];


        default:
            return state;
    }
};
