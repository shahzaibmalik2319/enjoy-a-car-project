export const projectReducer = (state = [], action) => {
    switch (action.type) {
            
            case 'SEARCH-CAR_INIT':
            return [
                ...state,
                Object.assign({}, { findcar:false })
            ];
              case 'SEARCH-CAR_FAILD':
            return [
                ...state,
                Object.assign({}, { findcar:false })
            ];
            case 'SEARCH-CAR_SUCCESS':
            return [
                ...state,
                Object.assign({}, { findcar:true,payload:action.payload  })
            ];

             case 'SHOW_ALL_OPTIONS':
            return [
                ...state,
                Object.assign({}, { Features:false,Equipments:false  })
            ];


            case 'HIDE_OPTIONS':
            return [
                ...state,
                Object.assign({}, { ShowFeatures:false,ShowEquipments:false })
            ];


            case 'GET_BRAND_NAME_INIT':
            return [
                ...state,
                Object.assign({}, { getBrandName:false })
            ];
              case 'GET_BRAND_NAME_FAILD':
            return [
                ...state,
                Object.assign({}, { getBrandName:false })
            ];
            case 'GET_BRAND_NAME_SUCCESS':
            return [
                ...state,
                Object.assign({}, { getBrandName:true,payload:action.payload  })
            ];



            case 'GET_BRAND_MODEL_INIT':
            return [
                ...state,
                Object.assign({}, { getBrandModel:false })
            ];
              case 'GET_BRAND_MODEL_FAILD':
            return [
                ...state,
                Object.assign({}, { getBrandModel:false })
            ];
            case 'GET_BRAND_MODEL_SUCCESS':
            return [
                ...state,
                Object.assign({}, { getBrandModel:true,payload:action.payload  })
            ];


             case 'GET_BRAND_YEAR_INIT':
            return [
                ...state,
                Object.assign({}, { getBrandYear:false })
            ];
              case 'GET_BRAND_YEAR_FAILD':
            return [
                ...state,
                Object.assign({}, { getBrandYear:false })
            ];
            case 'GET_BRAND_YEAR_SUCCESS':
            return [
                ...state,
                Object.assign({}, { getBrandYear:true,payload:action.payload  })
            ];


            //searchCar Component


              case 'SEARCH-CAR_UPDATE_FAILD':
            return [
                ...state,
                Object.assign({}, { updateFindcar:false })
            ];
            case 'SEARCH-CAR_UPDATE_SUCCESS':
            return [
                ...state,
                Object.assign({}, { updateFindcar:true,payload:action.payload  })
            ];


            case 'CONNECT_CAR_INT':
            return [
                ...state,
                Object.assign({}, { connectToCar:false })
            ];
              case 'CONNECT_CAR_FAILD':
            return [
                ...state,
                Object.assign({}, { connectToCar:false })
            ];
            case 'CONNECT_CAR_SUCCESS':
            return [
                ...state,
                Object.assign({}, { connectToCar:true,payload:action.payload  })
            ];



             case 'GET_STRIP_FORM_DATA_INIT':
            return [
                ...state,
                Object.assign({}, { getStripFormData:false })
            ];
              case 'GET_STRIP_FORM_DATA_FAILD':
            return [
                ...state,
                Object.assign({}, { getStripFormData:false })
            ];
            case 'GET_STRIP_FORM_DATA_SUCCESS':
            return [
                ...state,
                Object.assign({}, { getStripFormData:true  })
            ];



              case 'GET_FEATURES_FAILD':
            return [
                ...state,
                Object.assign({}, { getFeatures:false })
            ];
            case 'GET_FEATURES_SUCCESS':
            return [
                ...state,
                Object.assign({}, { getFeatures:true,payload:action.payload  })
                
            ];

              case 'GET_EQUIPMENTS_FAILD':
            return [
                ...state,
                Object.assign({}, { getEquipments:false })
            ];
            case 'GET_EQUIPMENTS_SUCCESS':
            return [
                ...state,
                Object.assign({}, { getEquipments:true,payload:action.payload  })
            ];


              case 'GET_DISCOUNTS_FAILD':
            return [
                ...state,
                Object.assign({}, { getDiscounts:false })
            ];
            case 'GET_DISCOUNTS_SUCCESS':
            return [
                ...state,
                Object.assign({}, { getDiscounts:true,payload:action.payload  })
            ];


             case 'CONFIRMATION_INIT':
            return [
                ...state,
                Object.assign({}, { confirmation:false })
            ];
              case 'CONFIRMATION_FAILD':
            return [
                ...state,
                Object.assign({}, { confirmation:false })
            ];
            case 'CONFIRMATION_SUCCESS':
            return [
                ...state,
                Object.assign({}, { confirmation:true ,payload:action.payload })
            ];


            case 'GET_PAGE':
            return [
                ...state,
                Object.assign({}, { payload:action.payload })
            ];

            case 'NEXT_PAGE':
            return [
                ...state,
                Object.assign({}, { payload:action.payload })
            ];

            case 'PREVIOUS_PAGE':
            return [
                ...state,
                Object.assign({}, { payload:action.payload })
            ];










            


        default:
            return state;
    }
    
};
