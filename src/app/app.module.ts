import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './components/app.component/app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FindNewCarComponent } from './components/find-new-car/find-new-car.component';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { HttpInterceptorModule } from 'angular2-http-interceptor';
import { HttpInterceptor } from 'angular2-http-interceptor';
import {MomentModule} from 'angular2-moment';
import { Ng2CompleterModule } from "ng2-completer";

// Services
import { AuthGraudService } from './Services/auth-graud.service';
import { AuthInterceptorService } from './Services/auth-interceptor.service';
import { LoaderService } from './Services/loader.service';
import { ApiService } from'./Services/api.service';
import { PublicVariablesService } from './Services/public-variables.service'
import { GetNotificationsCountService } from './Services/get-notifications-count.service'


// Redux
import { StoreModule } from '@ngrx/store';
import { authReducer } from './reducers/authReducer';
import { projectReducer } from './reducers/projectReducer';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';


// Routing Module
import { AppRoutingModule } from './app.routing';


// Components
import { NewsLetterComponent } from './components/news-letter/news-letter.component';
import { FooterLinksComponent } from './components/footer-links/footer-links.component';
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ForgetpasswordComponent } from './components/forgetpassword/forgetpassword.component';
import { RedirectingComponent } from './components/redirecting/redirecting.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { SearchCarComponent } from './components/search-car/search-car.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FindNewCarComponent,
    NewsLetterComponent,
    FooterLinksComponent,
    LoginComponent,
    SignupComponent,
    ForgetpasswordComponent,
    RedirectingComponent,
    LoaderComponent,
    ResetPasswordComponent,
    SearchCarComponent,
    NotificationsComponent,
    PrivacyPolicyComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule,
    AppRoutingModule,
    Ng2Bs3ModalModule,
    ReactiveFormsModule,
    Ng2CompleterModule,
    HttpInterceptorModule.withInterceptors([{
      provide: HttpInterceptor,
      useClass: AuthInterceptorService,
      multi: true
    }]),
    StoreModule.provideStore({ authReducer, projectReducer }),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
    MomentModule
  ],
  providers: [AuthGraudService, AuthInterceptorService, LoaderService, ApiService, PublicVariablesService,GetNotificationsCountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
