import { TestBed, inject } from '@angular/core/testing';

import { PublicVariablesService } from './public-variables.service';

describe('PublicVariablesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PublicVariablesService]
    });
  });

  it('should ...', inject([PublicVariablesService], (service: PublicVariablesService) => {
    expect(service).toBeTruthy();
  }));
});
