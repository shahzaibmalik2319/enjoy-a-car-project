import { TestBed, inject } from '@angular/core/testing';

import { AuthGraudService } from './auth-graud.service';

describe('AuthGraudService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGraudService]
    });
  });

  it('should ...', inject([AuthGraudService], (service: AuthGraudService) => {
    expect(service).toBeTruthy();
  }));
});
