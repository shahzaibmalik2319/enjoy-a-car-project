import { TestBed, inject } from '@angular/core/testing';

import { GlobalApiAddressService } from './global-api-address.service';

describe('GlobalApiAddressService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GlobalApiAddressService]
    });
  });

  it('should ...', inject([GlobalApiAddressService], (service: GlobalApiAddressService) => {
    expect(service).toBeTruthy();
  }));
});
