import { Injectable } from '@angular/core';
import { PublicVariablesService } from './public-variables.service'
import { ApiService } from "./api.service";
import { LoaderService } from './loader.service'

//redux
import { Store } from '@ngrx/store';


@Injectable()
export class GetNotificationsCountService {

  constructor(public userService: ApiService,
    public loader: LoaderService, public variableService: PublicVariablesService,
    private _store: Store<any>) { }


  getNotificationsCount(){
    this.userService.getNotifications().subscribe((data)=>{
      this.loader.stop();
      this.variableService.notificationsCount = data.count;
      this._store.dispatch({type: 'GET_NOTIFICATIONS-SUCCESS', payload:data.data})
    },(error)=>{
      this.loader.stop();
      this._store.dispatch({type: 'GET_NOTIFICATIONS-FAILED'})
      console.log(error);
    })
  }

}