import { Injectable } from '@angular/core';
import {Request, Response,XHRConnection,XHRBackend} from '@angular/http';
import {Observable} from 'rxjs';
import {HttpInterceptor} from 'angular2-http-interceptor';
import {LoaderService} from './../Services/loader.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor{

constructor(public loader: LoaderService) { }

before(request: Request): Request {
   this.loader.start();
  //do something ...
  let currentUser = JSON.parse(localStorage.getItem('SearcherUser'));
      if (currentUser && currentUser.token_id) {
          request.headers.set('Authorization', currentUser.token_id );
      }
  console.log(request);
  return request;
}
}