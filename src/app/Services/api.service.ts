import { Injectable } from '@angular/core';
import { GlobalApiAddressService } from './global-api-address.service';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

//redux
import { Store } from '@ngrx/store';
import { login, signUp, forget, resetPass, tokenConfirmation } from '../model/authInterface';




@Injectable()
export class ApiService {
    loginApi = GlobalApiAddressService.API_ENDPOINT + 'users/login';
    signUpApi = GlobalApiAddressService.API_ENDPOINT + 'users';
    UpdateUserApi = GlobalApiAddressService.API_ENDPOINT + 'users/dealers';
    forgetPasswordApi = GlobalApiAddressService.API_ENDPOINT + 'users/forgotpassword'
    resetPasswordApi = GlobalApiAddressService.API_ENDPOINT + 'users/resetpassword/'
    confimationApi = GlobalApiAddressService.API_ENDPOINT + 'users/confirmation/'
    searchAPI = GlobalApiAddressService.API_ENDPOINT + 'all/cars'
    searchUpdateAPI = GlobalApiAddressService.API_ENDPOINT + 'all/cars'
    brandsNameApi = GlobalApiAddressService.API_ENDPOINT + 'brand'
    carListApi = GlobalApiAddressService.API_ENDPOINT + 'carlist'
    modelYearsListApi = GlobalApiAddressService.API_ENDPOINT + 'modelyear/'
    recentSearchAddApi = GlobalApiAddressService.API_ENDPOINT + 'recentsearchadd'
    payConnectApi = GlobalApiAddressService.API_ENDPOINT + 'connect/submitrequest'
    getFeaturesApi = GlobalApiAddressService.API_ENDPOINT + 'features'
    getDiscountsApi = GlobalApiAddressService.API_ENDPOINT + 'discounts'
    getEquipmentsApi = GlobalApiAddressService.API_ENDPOINT + 'equipments'
    notificationsApi = GlobalApiAddressService.API_ENDPOINT + 'notification/searcher'
    querySessionApi = GlobalApiAddressService.API_ENDPOINT + 'querySession'
    querySessionDataApi = GlobalApiAddressService.API_ENDPOINT + 'querySession'
    subscribeUserApi = GlobalApiAddressService.API_ENDPOINT + 'subscribeusers'
    guestApi = GlobalApiAddressService.API_ENDPOINT + 'connect/guest/submitrequest'
    readNotificationsApi = GlobalApiAddressService.API_ENDPOINT + 'readnotification/searcher'


    constructor(private _http: Http, private _store: Store<any>) {
    }
    doLogin(data: login) {

        return this._http.post(this.loginApi, data)
            .map(data => {
                data.json();
                console.log("I CAN SEE DATA HERE: ", data.json());
                return data.json();

            });


    }
    doSignUp(data: signUp) {
        return this._http.post(this.signUpApi, data)
            .map(data => {
                data.json();
                console.log("I CAN SEE DATA HERE: ", data.json());
                return data.json();
            });
    }

    doUpdateUser(data) {
        return this._http.post(this.UpdateUserApi, data)
            .map(data => {
                data.json();
                console.log("I CAN SEE DATA HERE: ", data.json());
                return data.json();
            });
    }
    forgetPassword(email: forget) {
        return this._http.post(this.forgetPasswordApi, { email: email }).map((data) => {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        })
    }
    restPassword(token: tokenConfirmation, data: resetPass) {
        return this._http.post(this.resetPasswordApi + token, data).map((data) => {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        })
    }
    confirmation(token: tokenConfirmation) {
        return this._http.get(this.confimationApi + token).map((data) => {
            data.json();
            console.log("I CAN SEE DATA HERE: ", data.json());
            return data.json();
        })
    }

     querySession(data) {
        var _data = {
            query: data
        }
        return this._http.post(this.querySessionApi, _data)
            .map(data => {
                data.json();
                console.log("I CAN SEE QUERY SESSION ID  HERE: ", data.json());
                return data.json();

            });
        
       
    }

      getQuerySession(cacheID) {
          
        return this._http.get(this.querySessionDataApi +'?cacheID=' + cacheID)
            .map(data => {
                data.json();
                console.log("I CAN SEE QUERY SESSION DATA  HERE: ", data.json());
                return data.json();

            });
        
       
    }

    searchCar(data) {
        var query = '?limit=5&page=1&'
        for(var key in data){
            if(data){
                if(data[key] && data[key] != null && data[key] != '' && data[key] != 'undefined'){
                    query +=key+'='+data[key]+'&';
                }
            }
        }
        return this._http.get(this.searchAPI+query)
            .map(data => {
                data.json();
                console.log("I CAN SEE DATA HERE: ", data.json());
                return data.json();
            });
    }
    searchCarUpdate(data) {
        var query = '?limit=5&page=1&'
        for(var key in data){
            if(data){
                if(data[key] && data[key] != null && data[key] != '' && data[key] != 'undefined'){
                    query +=key+'='+data[key]+'&';
                }
            }
        }
        return this._http.get(this.searchAPI+query)
            .map(data => {
                data.json();
                console.log("I CAN SEE DATA HERE: ", data.json());
                return data.json();
            });
    }
     userSubcribes(email,name) {
         return this._http.post(this.subscribeUserApi ,{ email:email, name: name })
            .map(data => {
                data.json();
                console.log("I CAN SEE QUERY SUBSCRIBE DATA  HERE: ", data.json());
                return data.json();

            });

    }
    recentSearch(id) {
        return this._http.post(this.recentSearchAddApi + "?id=" + id, id)
            .map(data => {
                data.json();
                console.log("I CAN SEE DATA HERE: ", data.json());
                return data.json();
            });
    }
    getBrand() {
        return this._http.get(this.brandsNameApi)
            .map(data => {
                data.json();
                console.log("I CAN SEE CARS HERE: ", data.json());
                return data.json();
            });
    }
    getCarList(carName) {
        return this._http.get(this.carListApi + '?search=' + carName)
            .map(data => {
                data.json();
                console.log("I CAN SEE MODELS HERE: ", data.json());
                return data.json();
            });
    }
    getModelYearsList(model) {
        return this._http.get(this.modelYearsListApi + '?search=' + model)
            .map(data => {
                data.json();
                console.log("I CAN SEE YEARS HERE: ", data.json());
                return data.json();
            });
    }
    payConnect(connect) {
        return this._http.post(this.payConnectApi,connect)
            .map(data => {
                data.json();
                console.log("I CAN SEE CONNECT HERE: ", data.json());
                return data.json();
            });
    }
    getFeaturesList() {
        return this._http.get(this.getFeaturesApi)
            .map(data => {
                data.json();
                console.log("I CAN SEE FEATURES HERE: ", data.json());
                return data.json();
            });
    }
    getDiscountsList() {
        return this._http.get(this.getDiscountsApi)
            .map(data => {
                data.json();
                console.log("I CAN SEE DISCOUNTS HERE: ", data.json());
                return data.json();
            });
    }
    getEquipmentsList() {
        return this._http.get(this.getEquipmentsApi)
            .map(data => {
                data.json();
                console.log("I CAN SEE EQUIPMENTS HERE: ", data.json());
                return data.json();
            });
    }
    getNotifications() {
        return this._http.get(this.notificationsApi)
            .map(data => {
                data.json();
                console.log("I CAN SEE NOTIFICATIONS HERE: ", data.json());
                return data.json();
            });
    }
     getNotificationsPage(page) {
        return this._http.get(this.notificationsApi + "?limit=10&page="+ page)
            .map(data => {
                data.json();
                console.log("I CAN SEE NOTIFICATIONS HERE: ", data.json());
                return data.json();
            });
    }
    getMyRecentCars() {
        return this._http.get(this.searchAPI + '?limit=5&page=1').map((data) => {
            data.json();
            console.log("I CAN SEE MY RECENT CARS: ", data.json());
            return data.json();
        })
    }
            getMyRecentCarsPage(page) {
        return this._http.get(this.searchAPI + '?limit=5&' + 'page=' + page).map((data) => {
            data.json();
            console.log("I CAN SEE MY RECENT CARS PAGE: ", data.json());
            return data.json();
        })
    }
    guest(data) {
        return this._http.post(this.guestApi,data).map((data) => {
            data.json();
            console.log("I CAN SEE GUEST CONNECT : ", data.json());
            return data.json();
        })
    }
      readNotifications() {
        return this._http.get(this.readNotificationsApi).map((data) => {
            data.json();
            console.log("I CAN SEE MY READ NOTIFICATIONS: ", data.json());
            return data.json();
        })
    }
}