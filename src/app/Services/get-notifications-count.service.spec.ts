import { TestBed, inject } from '@angular/core/testing';

import { GetNotificationsCountService } from './get-notifications-count.service';

describe('GetNotificationsCountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GetNotificationsCountService]
    });
  });

  it('should be created', inject([GetNotificationsCountService], (service: GetNotificationsCountService) => {
    expect(service).toBeTruthy();
  }));
});
