import { Injectable } from '@angular/core';

@Injectable()
export class PublicVariablesService {

  constructor() { }
   public searchRecord:any
   public navBar = true;
   public isLogin = false;
   public currentUser:any;
   public notificationsCount;
   public getIsLogin(){ 
     return this.isLogin;
   }
   public getNav (){
     return this.navBar;
   }
   public getUser(){
      return this.currentUser
   }
   public getSeacrhRecord(){
     return this.searchRecord;
   }
}
