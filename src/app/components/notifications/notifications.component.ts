import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { ApiService } from '../../Services/api.service';
import { LoaderService } from '../../Services/loader.service'
import { PublicVariablesService } from '../../Services/public-variables.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompleterService, CompleterData } from 'ng2-completer';
import { ActivatedRoute } from '@angular/router';
import { GetNotificationsCountService } from '../../Services/get-notifications-count.service';

//redux
import { Store } from '@ngrx/store';


@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html?v=${new Date().getTime()}',
  styleUrls: ['./notifications.component.css?v=${new Date().getTime()}']
})
export class NotificationsComponent implements OnInit {
  notificationList: any;
  totalNotifications: number;
  //pagination
  totalPages = [];
  totalRecentSearch;
  currentPage: number = 1;
  totalServepage;
  serverCount;
  currentCount;

  constructor(public route: ActivatedRoute, public userService: ApiService,
    public loader: LoaderService, public variableService: PublicVariablesService,
    private fb: FormBuilder,
    private connectFb: FormBuilder,
    private completerService: CompleterService,
    public routeToNavigate: Router,
    public notifications: GetNotificationsCountService,
    private _store: Store<any>) { }

  allNotifications() {
    this.userService.getNotifications().subscribe((data) => {
      this.loader.stop();
      this.notificationList = data.notification;
      this.totalNotifications = this.notificationList.length;
      this.totalServepage = data.totalPages;
      for (let i = 1; i <= data.totalPages; i++) {
        let page = { page: i }
        this.totalPages.push(page);
      }
      this.serverCount = data.count;
      console.log(data);
      this._store.dispatch({ type: 'GET_NOTIFICATIONS-SUCCESS', payload: data.data })
    }, (error) => {
      this.loader.stop();
      this._store.dispatch({ type: 'GET_NOTIFICATIONS-FAILED' })
      console.log(error);
    })
  }



  getPage(pageNum) {
    this.currentPage = pageNum;
    this.userService.getNotificationsPage(pageNum).subscribe((data) => {
      this.loader.stop();
      this.notificationList = data.notification;
      this.totalNotifications = this.notificationList.length;
      this.totalServepage = data.totalPages;
      this.serverCount = data.count;
      this.currentPage = data.page;
      this._store.dispatch({ type: 'GET_PAGE', payload: data });
      this.getOnTheTop()
      console.log(data)
    }, (error) => {
      this.loader.stop()
      console.log(error);

    })
  }
  nextPage() {
    this.currentPage += 1
    this.userService.getNotificationsPage(this.currentPage).subscribe((data) => {
      this.loader.stop();
      this.notificationList = data.notification;
      this.totalNotifications = this.notificationList.length;
      this.totalServepage = data.totalPages;
      this.serverCount = data.count;
      this._store.dispatch({ type: 'NEXT_PAGE', payload: data });
      this.getOnTheTop()
      console.log(data)
    }, (error) => {
      this.loader.stop()
      console.log(error);

    })
  }
  previousPage() {
    this.currentPage -= 1
    this.userService.getNotificationsPage(this.currentPage).subscribe((data) => {
      this.loader.stop();
      this.notificationList = data.notification;
      this.totalNotifications = this.notificationList.length;
      this.totalServepage = data.totalPages;
      this.serverCount = data.count;
      this._store.dispatch({ type: 'PREVIOUS_PAGE', payload: data });      
      this.getOnTheTop()
      console.log(data)
    }, (error) => {
      this.loader.stop()
      console.log(error);

    })
  }
  active(page) {
    if (this.currentPage === page) {
      return true;
    }
  }
   allRead(){
    this.userService.readNotifications().subscribe((data)=>{
      this.loader.stop();
      console.log(data);
      this.totalPages = [];
      this.ngOnInit();
    },(error)=>{
      this.loader.stop()
      console.log(error);
    })
  }
  getOnTheTop() {
   console.log('top')
   window.scrollTo(0, 0);
 }

  ngOnInit() {
    this.allNotifications();
    this.notifications.getNotificationsCount();
    this.getOnTheTop()
  }
}
