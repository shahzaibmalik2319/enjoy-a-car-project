import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../Services/api.service';
import { LoaderService } from '../../Services/loader.service'
import { PublicVariablesService } from '../../Services/public-variables.service';

//redux
import { Store } from '@ngrx/store';
import { login } from '../../model/authInterface'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  user: any = {};
  form: FormGroup;
  Msg: any;
  userData: login;




  constructor(public route: Router,
    public userService: ApiService,
    public loader: LoaderService,
    private fb: FormBuilder,
    public variableService: PublicVariablesService,
    private _store: Store<any>) {
    this.form = this.fb.group({
      'email': [this.user.email || null, Validators.required],
      'password': [this.user.password || null, Validators.required]
    })
    this.form.patchValue({
      email: this.user.email,
      password: this.user.password
    })

    this._store.dispatch({ type: 'LOGIN_INITIALS' });
  }
  login() {

    this.userData = {
      email: this.user.email,
      password: this.user.password,
      role: "buyers",
    }
    this.userService.doLogin(this.userData).subscribe((data) => {
      this.loader.stop();
      localStorage.setItem('searcherUserID', data.user.token_id);
      localStorage.setItem('SearcherUser', JSON.stringify(data.user));
      this.variableService.isLogin = true;
      this.route.navigate([''])
      this._store.dispatch({ type: 'LOGIN_SUCCESS', payload: data });
    }, (error) => {
      console.log(error);
      let err = JSON.parse(error._body);
      console.log(err);
      this._store.dispatch({ type: 'LOGIN_FAILD' });
      this.Msg = err.message;
      this.loader.stop();

    })

  }

  isEmailValid() {
    var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
    return EMAIL_REGEXP.test(this.user.email)
  }
  ngOnInit() {
  }

}
