import { Component, OnInit } from '@angular/core';
import { PublicVariablesService } from '../../Services/public-variables.service';
import { ApiService } from '../../Services/api.service';
import { Router, ActivatedRoute } from '@angular/router'
import { LoaderService } from '../../Services/loader.service';

//redux
import { Store } from '@ngrx/store';
import { login } from '../../model/authInterface';


@Component({
  selector: 'app-redirecting',
  templateUrl: './redirecting.component.html',
  styleUrls: ['./redirecting.component.css']
})
export class RedirectingComponent implements OnInit {
  sub;
  token;
  constructor(public variableService: PublicVariablesService, private userService: ApiService, private routerToNavigate: Router, private route: ActivatedRoute, private loader: LoaderService,
    private _store: Store<any>) {
    this.variableService.navBar = false;
    this._store.dispatch({ type: 'CONFIRMATION_INIT' });
  }
  subscribe() {
    this.sub = this.route
      .params
      .subscribe(params => {
        this.token = params['token'];
        console.log(this.token);
      });
  }

  ngOnInit() {
    this.subscribe();
    let self = this;
    setTimeout(function () {
      self.userService.confirmation(self.token).subscribe((data) => {
        console.log(data);
        localStorage.setItem('searcherUserID', data.user.token_id)
        localStorage.setItem('SearcherUser', JSON.stringify(data.user));
        self.loader.stop();
        self.variableService.currentUser = data.user;
        self.variableService.navBar = true;
        self.variableService.isLogin = true;
        self._store.dispatch({ type: 'CONFIRMATION_SUCCESS', payload: data });
        self.routerToNavigate.navigate([''])
      }, (error) => {
        this._store.dispatch({ type: 'CONFIRMATION_FAILD' });
        self.loader.stop()
        console.log(error);
      })
    }, 3000)
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}