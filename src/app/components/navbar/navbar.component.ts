import { Component, OnInit } from '@angular/core';
import { PublicVariablesService } from '../../Services/public-variables.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  user;
  isLogin: boolean = false;
  constructor(public variableService: PublicVariablesService, private route: Router) {
    if (!localStorage.getItem('searcherUserID')) {
      this.isLogin = this.variableService.getIsLogin();;
    }
    else {
      this.variableService.isLogin = true;
    }
    this.user = localStorage.getItem('SearcherUser');
    this.user = JSON.parse(this.user)
    console.log(this.user)
  }
  logout() {
    console.log('logout')
    localStorage.removeItem('SearcherUser');
    localStorage.removeItem('searcherUserID');
    this.variableService.isLogin = false;
    this.route.navigate(['/login']);
  }

  ngOnInit() { }

}