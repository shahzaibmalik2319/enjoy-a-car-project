import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { ApiService } from '../../Services/api.service';
import { LoaderService } from '../../Services/loader.service'

@Component({
  selector: 'app-news-letter',
  templateUrl: './news-letter.component.html',
  styleUrls: ['./news-letter.component.css']
})
export class NewsLetterComponent implements OnInit {
  @ViewChild('success')
  successModal: ModalComponent;
  @ViewChild('alreadySubscribeModal')
  alreadySubscribeModal: ModalComponent
  user: any = {};
  form: FormGroup;
  subscribeEmail;
  constructor(   private fb: FormBuilder,public userService: ApiService,public loader: LoaderService,) { 

    this.form = this.fb.group({
      'email': [this.user.email || null, Validators.required],
      'name': [this.user.name || null , Validators.required],
    })
  }
  isEmailValid() {
    var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
    if (EMAIL_REGEXP.test(this.user.email)) {

      return true;
    }
    else {
      return false;
    }

  }
  onsubmit(){

    this.userService.userSubcribes(this.user.email,this.user.name).subscribe((data) => {
        this.loader.stop();
        this.successModal.open()
        console.log(data);
        this.user = {};
      }, (error) => {
        console.log(error);
        this.loader.stop();
        let err = JSON.parse(error._body);
        console.log(err);
        if (err.error.code === 11000) {
            this.alreadySubscribeModal.open();
        }
        this.user = {}
      })
    
    
  }
  ngOnInit() {
  }

}
