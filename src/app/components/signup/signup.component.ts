import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ApiService } from '../../Services/api.service';
import { LoaderService } from '../../Services/loader.service';

//redux
import { Store } from '@ngrx/store';
import { signUp } from '../../model/authInterface';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
})
export class SignupComponent implements OnInit {

  user: any = {};
  form: FormGroup;
  Msg: any;
  userData: any = {};
  zipError;
  FirstNameError;
  LastNameError;
  AgeError;


  constructor(public route: Router, public userService: ApiService, public loader: LoaderService, private fb: FormBuilder, private _store: Store<any>) {
    this.form = this.fb.group({
      'firstname': [this.user.firstname || null, Validators.required],
      'lastname': [this.user.lastname || null, Validators.required],
      'email': [this.user.email || null, Validators.required],
      'age': [this.user.age || null, Validators.required],
      'gender': [this.user.gender || null, Validators.required],
      'password': [this.user.password || null, Validators.required],
      'zip_code': [this.user.zip_code || null],
    })
    this._store.dispatch({ type: 'SIGNUP_INIT' });
    this.zipError = false;
    this.FirstNameError = false;
    this.LastNameError = false;
    this.AgeError = false;
  }
  onSubmit() {
    this.userData = {
      firstName: this.user.firstname,
      lastName: this.user.lastname,
      email: this.user.email,
      age: this.user.age,
      gender: this.user.gender,
      password: this.user.password,
      role: 'buyers'
    }
    this.userService.doSignUp(this.userData)
      .subscribe((data) => {
        this.loader.stop();
        console.log(data);
        this.Msg = data.message;
        this._store.dispatch({ type: 'SIGNUP_SUCCESS', payload: data });
        setTimeout(() => {
          this.route.navigate(['/login'])
        }, 2000)
      }, (error) => {
        console.log(error);
        this._store.dispatch({ type: 'SIGNUP_FAILD' });
        let err = JSON.parse(error._body);
        console.log(err);
        this.Msg = err.error;
        this.loader.stop();
      })

  }
  isEmailValid() {
    var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
    return EMAIL_REGEXP.test(this.user.email)
  }
  isNameValid() {
    var NAME_REGEXP = new RegExp(/^[A-Za-z]+$/);
    if (NAME_REGEXP.test(this.user.firstname)) {
      this.FirstNameError = false;
    }
    else { this.FirstNameError = true; }
  }
  isLastNameValid() {
    var NAME_REGEXP = new RegExp(/^[A-Za-z]+$/);
    if (NAME_REGEXP.test(this.user.lastname)) {
      this.LastNameError = false;
    }
    else { this.LastNameError = true; }
  }
  isAgeValid() {

    var NAME_REGEXP = new RegExp(/^\d+$/);
    if (NAME_REGEXP.test(this.user.age)) {
      this.AgeError = false;
    }
    else { this.AgeError = true; }
  }

  isZipCodeValid() {
    var isValid = /^[0-9]{5}(?:-[0-9]{4})?$/.test(this.user.zip_code);
    if (isValid) {
      this.zipError = false;
    }
    else { this.zipError = true; }
  }

  ngOnInit() {
  }

}
