import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ApiService } from '../../Services/api.service';
import { LoaderService } from '../../Services/loader.service';

//redux
import { Store } from '@ngrx/store';
import { forget } from '../../model/authInterface';

@Component({
  selector: 'app-forgetpassword',
  templateUrl: './forgetpassword.component.html',
  styleUrls: ['./forgetpassword.component.css']
})
export class ForgetpasswordComponent implements OnInit {

  user: any = {};
  form: FormGroup;
  Msg: any;
  userData: forget;
  constructor(private fb: FormBuilder, public userService: ApiService, public loader: LoaderService, private _store: Store<any>) {
    this.form = this.fb.group({
      'email': [null, Validators.required],
    })
    this._store.dispatch({ type: 'FORGETPASSWORD_INIT' });
  }

  onSubmit() {
    this.userData = {
      email: this.user.email,
    }
    console.log(this.user.email)
    this.userService.forgetPassword(this.userData).subscribe((data) => {
      console.log(data)
      this.Msg = data.message;
      this._store.dispatch({ type: 'FORGETPASSWORD_SUCCESS', payload: data });
      this.user.email = ""
      this.loader.stop()

    }, (error) => {
      this._store.dispatch({ type: 'FORGETPASSWORD_FAILD' });
      console.log(JSON.parse(error._body))
      let msg = JSON.parse(error._body)
      this.Msg = msg.message;
      this.user.email = ""
      this.loader.stop()

    })
  }

  ngOnInit() {
  }

}
