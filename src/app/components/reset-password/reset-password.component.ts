import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms'
import { ApiService } from '../../Services/api.service';
import { LoaderService } from '../../Services/loader.service';
import { ActivatedRoute, Router } from '@angular/router';

//redux
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  newPasswordObj: any = {}
  passwordObj: any = {}
  sub;
  Msg;
  token;
  constructor(private fb: FormBuilder, public userService: ApiService, public loader: LoaderService, private route: ActivatedRoute, public router: Router,
    private _store: Store<any>) {
    this._store.dispatch({ type: 'RESETPASSWORD_INITIALS' });
  }
  subscribe() {
    this.sub = this.route
      .params
      .subscribe(params => {
        this.token = params['token'];
        console.log(params);
        console.log(this.token);
      });
  }

  onChange() {
    this.passwordObj = {
      newPassword: this.newPasswordObj.pass1,
      verifyPassword: this.newPasswordObj.pass2
    }
    this.userService.restPassword(this.token, this.passwordObj).subscribe((data) => {
      this.Msg = data.message;
      this._store.dispatch({ type: 'RESETPASSWORD_SUCCESS', payload: data });
      this.router.navigate[('/login')]
    }, (error) => {
      this._store.dispatch({ type: 'FORGETPASSWORD_FAILD' });
      this.Msg = error.message;
    })
  }
  ngOnInit() {
    this.subscribe();
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
