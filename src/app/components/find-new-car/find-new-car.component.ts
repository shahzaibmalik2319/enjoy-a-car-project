import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { FormControl, FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../Services/api.service';
import { LoaderService } from '../../Services/loader.service'
import { GetNotificationsCountService } from '../../Services/get-notifications-count.service';
import { PublicVariablesService } from '../../Services/public-variables.service';
import { CompleterService, CompleterData } from 'ng2-completer';

//redux
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-find-new-car',
  templateUrl: './find-new-car.component.html?v=${new Date().getTime()}',
  styleUrls: ['./find-new-car.component.css?v=${new Date().getTime()}']
})
export class FindNewCarComponent implements OnInit {


  connectPrice;
  modelYears;
  equipGourp: any = []
  Features: any = []
  discounts: any = []
  showMoreOptions: boolean = false;
  hideBrands: boolean = true;
  addFeature: boolean = true;
  addFeature2: boolean = false;
  features: boolean = false;
  searchbuttons: boolean = true;
  discountOptions: boolean = false;

  subscribeEmail;
  car: any = {};
  form: FormGroup;
  carData: any = {};
  brandsList;
  modelList;
  newStatus: any = [];
  querySessionId;
  brands: any = [
    { name: 'Alfa Romeo' },
    { name: 'Alpine' },
    { name: 'Aston Martin' },
    { name: 'Audi' },
    { name: 'Bently' },
    { name: 'BMW' },
    { name: 'Bugatti' },
    { name: 'Ferrari' },
    { name: 'Fiat' },
    { name: 'Limborghini' },
    { name: 'Lincia' },
    { name: 'Land Rover' },
    { name: 'Meserati' },
    { name: 'McLaren' },
    { name: 'Mercedes-Benz' },
    { name: 'Mini' },
    { name: 'Opel' },
    { name: 'Peugeot' },
    { name: 'Porsche' },
    { name: 'Renault' },
    { name: 'Rolls-Royce' },
    { name: 'Vauxhall' },
    { name: 'Volkswagen' },
  ]


  constructor(
    public route: Router,
    public userService: ApiService,
    public loader: LoaderService,
    private fb: FormBuilder,
    public variableService: PublicVariablesService,
    private _store: Store<any>,
    private completerService: CompleterService,
    public notifications: GetNotificationsCountService, ) {

    this.form = this.fb.group({
      'zip_code': [this.car.zip_code || null, Validators.required],
      'year': [this.car.year || null, Validators.required],
      'brand': [this.car.brand || null, Validators.required],
      'model': [this.car.model || null, Validators.required],
      'time': [this.car.time || null, Validators.required],
      'exterior': [this.car.exterior],
      'interior': [this.car.interior],
      'transmission': [this.car.transmission],
      'engine': [this.car.engine],
      'body_style': [this.car.body_style],
    })

    this._store.dispatch({ type: 'SEARCH-CAR_INIT' });
    this._store.dispatch({ type: 'GET_BRAND_NAME_INIT' });
    this._store.dispatch({ type: 'GET_BRAND_MODEL_INIT' });
    this._store.dispatch({ type: 'GET_BRAND_YEAR_INIT' });


  }

  moreOptions() {
    this.showMoreOptions = true;
    this.addFeature = false;
    this.hideBrands = false;
    this.addFeature2 = true;
  }
  addFeatures() {
    this.addFeature = false;
  }
  moreFeatures() {
    this.addFeature2 = false;
    this.features = true;
    this._store.dispatch({ type: 'SHOW_ALL_OPTIONS' });
  }
  hideFeatures() {
    this.features = false;
    this.showMoreOptions = false;
    this.hideBrands = true;
    this.addFeature = true;
    this._store.dispatch({ type: 'HIDE_OPTIONS' });
  }

  searchCar() {



    this.carData = {
      zip_code: this.car.zip_code,
      year: this.car.year,
      brand: this.car.brand,
      model: this.car.model,
      time: this.car.time,
      exterior: this.car.exterior,
      interior: this.car.interior,
      transmission: this.car.transmission,
      engine: this.car.engine,
      body_style: this.car.body_style,
    }

    let equips = []
    let features = []
    for (let i = 0; i < this.equipGourp.length; i++) {
      if (this.equipGourp[i].status) {
        let newItem = this.equipGourp[i].name;
        equips.push(newItem);
      }
    }

    for (let i = 0; i < this.Features.length; i++) {
      if (this.Features[i].status) {
        let newItem = this.Features[i].name;
        features.push(newItem);
      }
    }

    if (features.length !== 0) {
      this.carData.features = features;
    }
    if (equips.length !== 0) {
      this.carData.equipments = equips;
    }
    console.log("Car data", this.carData)
    let query = {};

    for (var i in this.carData) {
      if (this.carData[i] && this.carData[i].length > 0) {
        query[i] = this.carData[i];
      }
    }
    console.log("Query", query)
    this.userService.querySession(query).subscribe((data) => {

      this.loader.stop();
      console.log(data);
      this.querySessionId = data.data;
      console.log(this.querySessionId);
      this.route.navigate(['/search-car', { cacheID: this.querySessionId }]);

    }, (error) => {
      console.log(error);
      let err = JSON.parse(error._body);
      console.log(err);
      this.loader.stop();

    })


  }

  showAllOptions() {
    this.features = true;
    this.showMoreOptions = true;
    this.discountOptions = true;
  }
  getBrandNames() {
    this.userService.getBrand().subscribe((data) => {
      this.loader.stop();
      this.brandsList = data.data;
      this.brandsList = this.completerService.local(this.brandsList, 'make', 'make')
      this._store.dispatch({ type: 'GET_BRAND_NAME_SUCCESS', payload: this.brandsList });


      //console.log(this.brandsList);

    }, (error) => {
      this._store.dispatch({ type: 'GET_BRAND_NAME_FAILD' });

      this.loader.stop();
      console.log(error);
    })
  }
  selectedBrand() {
    this.userService.getCarList(this.car.brand).subscribe((data) => {
      this.loader.stop();
      this.modelList = data.data;
      this.modelList = this.completerService.local(this.modelList, 'model', 'model')
      this.car.model = "";
      this._store.dispatch({ type: 'GET_BRAND_MODEL_SUCCESS', payload: this.modelList });
      //console.log(this.modelList);

    }, (error) => {
      this._store.dispatch({ type: 'GET_BRAND_MODEL_FAILD' });
      this.loader.stop();
      console.log(error);
    })
  }
  selectedModel() {
    this.userService.getModelYearsList(this.car.model).subscribe((data) => {
      this.loader.stop();
      this.modelYears = data.data;
      console.log(this.modelYears)
      this._store.dispatch({ type: 'GET_BRAND_YEAR_SUCCESS', payload: this.modelYears });

    }, (error) => {
      this._store.dispatch({ type: 'GET_BRAND_YEAR_FAILD' });
      this.loader.stop();
      console.log(error);
    })
  }
  getBrand(brand) {
    let carName: any = { brandName: brand };
    console.log(carName);
    this.route.navigate(['search-car', carName]);
  }
  getFeatures() {

    this.userService.getFeaturesList().subscribe((data) => {
      this.loader.stop();
      this.Features = data.data;
      this._store.dispatch({ type: 'GET_FEATURES_SUCCESS', payload: data.data });

    }, (error) => {
      this._store.dispatch({ type: 'GET_FEATURES_FAILD' });
      console.log(error);
      let err = JSON.parse(error._body);
      console.log(err);
      this.loader.stop();

    })

  }

  getEquipments() {

    this.userService.getEquipmentsList().subscribe((data) => {
      this.loader.stop();
      this.equipGourp = data.data;
      this._store.dispatch({ type: 'GET_EQUIPMENTS_SUCCESS', payload: data.data });

    }, (error) => {
      this._store.dispatch({ type: 'GET_EQUIPMENTS_FAILD' });
      console.log(error);
      let err = JSON.parse(error._body);
      console.log(err);
      this.loader.stop();

    })

  }

  getOnTheTop() {
    window.scrollTo(0, 0);
  }


  ngOnInit() {
    this.getOnTheTop();
    this.notifications.getNotificationsCount();
    this.getBrandNames();
    this.getFeatures();
    this.getEquipments();

  }
}
