import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { ApiService } from '../../Services/api.service';
import { LoaderService } from '../../Services/loader.service'
import { PublicVariablesService } from '../../Services/public-variables.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CompleterService, CompleterData } from 'ng2-completer';
import { ActivatedRoute } from '@angular/router';


//redux
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-search-car',
  templateUrl: './search-car.component.html?v=${new Date().getTime()}',
  styleUrls: ['./search-car.component.css?v=${new Date().getTime()}']
})
export class SearchCarComponent implements OnInit {
  @ViewChild('connectModal')
  modal: ModalComponent;
  @ViewChild('success')
  successModal: ModalComponent;
  @ViewChild('alreadySubscribeModal')
  alreadySubscribeModal: ModalComponent
  @ViewChild('reqModal')
  reqModal: ModalComponent;
  @ViewChild('successRequest')
  successRequest: ModalComponent;


  carId;
  dealerId;
  connectFormData: any = {};
  connectData: any = {};
  form: FormGroup;
  connectPrice;
  showMoreOptions: boolean = false;
  hideBrands: boolean = true;
  addFeature: boolean = true;
  addFeature2: boolean = false;
  features: boolean = false;
  searchbuttons: boolean = true;
  carNameAndModel: string = "FORD MUSTANG - 78223"
  modelYears;
  discountOptions: boolean = false;
  searchQuery: any = {};
  searchResult: any = [{}]
  matchRadioBox: boolean = false;
  brandsList;
  modelList;
  currentYear = new Date().getFullYear() - 1;
  expireYears: Array<number> = [];
  expire: any = {};
  CardNumError;
  zipError;
  emailError;
  NameError;
  postalError;
  phoneError;
  cvcError;
  cardNumError;
  newcurrentYear;
  searchResultLenght;
  public isDataLoaded: boolean = false;
  carEquipGourp: any = [];
  carFeatures: any = [];
  carDiscounts: any = [];
  listOfCarSearchIndex;
  cacheID;
  requestModalForm: any = {};
  requetuserName = 'Guest';
  SeacherData: any = {};
  //pagination
  totalPages = [];
  totalRecentSearch;
  currentPage: number = 1;
  totalServepage;
  serverCount;
  currentCount;
  listOfCarSearch: any;




  constructor(public route: ActivatedRoute, public userService: ApiService,
    public loader: LoaderService, public variableService: PublicVariablesService,
    private fb: FormBuilder,
    private connectFb: FormBuilder,
    private requestFb: FormBuilder,
    private completerService: CompleterService,
    public routeToNavigate: Router,
    private _store: Store<any>
  ) {

    this.form = this.connectFb.group({
      'nameOnCard': [this.connectFormData.nameOnCard || null, Validators.required],
      'card': [this.connectFormData.card || null, Validators.required],
      'email': [this.connectFormData.email || null, Validators.required],
      'card_number': [this.connectFormData.card_number || null, Validators.required],
      'phone': [this.connectFormData.phone || null, Validators.required],
      'expire': [this.connectFormData.expire || null, Validators.required],
      'pin_code': [this.connectFormData.pin_code || null, Validators.required],
      'name': [this.connectFormData.name || null, Validators.required],
      'postal_code': [this.connectFormData.postal_code || null, Validators.required],
      'zip_code': [this.connectFormData.zip_code || null, Validators.required],
      'month': [this.connectFormData.month || null, Validators.required],
      'year': [this.connectFormData.year || null, Validators.required],
      'reqEmail': [this.requestModalForm.email || null, Validators.required],


    })

    this.newcurrentYear = this.currentYear.toString();
    this.newcurrentYear = this.newcurrentYear.slice(2, 4);
    for (let i = 0; i < 14; i++) {
      this.newcurrentYear = ++this.newcurrentYear;
      this.expireYears.push(this.newcurrentYear);

    }
    this._store.dispatch({ type: 'CONNECT_CAR_INT' });

    this.emailError = false;
    this.NameError = false;
    this.phoneError = false;
    this.cvcError = false;
    this.cardNumError = false;

  }

  connectPayBtton(id) {

    if (localStorage.getItem('searcherUserID')) {
      this.connectData = this.connectFormData;
      this.connectData.car = this.carId;
      this.connectData.dealer = this.dealerId;
      this.connectData.expire = this.expire;
      console.log(this.connectData);
      this.userService.payConnect(this.connectData).subscribe((data) => {
        this.loader.stop();
        this.modal.close().then(() => {
          this.successModal.open();
        });
        console.log(data)
        this._store.dispatch({ type: 'GET_STRIP_FORM_DATA_SUCCESS', payload: data.data });

      }, (error) => {
        this._store.dispatch({ type: 'GET_STRIP_FORM_DATA_FAILD' });
        this.loader.stop();
        console.log(error);
      })

    }
    else {
      this.connectData = this.connectFormData;
      this.connectData.expire = this.expire;
      this.connectData.car = this.carId;
      this.connectData.dealer = this.dealerId;
      console.log(this.connectData);
      this.userService.guest(this.connectData).subscribe((data) => {
        this.loader.stop();
        this.modal.close().then(() => {
          this.successModal.open();
        });
        console.log(data)
        this._store.dispatch({ type: 'GET_STRIP_FORM_DATA_SUCCESS', payload: data.data });

      }, (error) => {
        this._store.dispatch({ type: 'GET_STRIP_FORM_DATA_FAILD' });
        this.loader.stop();
        console.log(error);
      })


    }

  }

  moreOptions() {
    this.showMoreOptions = true;
    this.addFeature = false;
    this.hideBrands = false;
    this.addFeature2 = true;
  }
  addFeatures() {
    this.addFeature = false;
  }
  moreFeatures() {
    this.addFeature2 = false;
    this.features = true;
    this._store.dispatch({ type: 'SHOW_ALL_OPTIONS' });
  }
  hideFeatures() {
    this.features = false;
    this.showMoreOptions = false;
    this.hideBrands = true;
    this.addFeature = true;
    this._store.dispatch({ type: 'HIDE_OPTIONS' });
  }
  showAllOptions() {
    this.features = true;
    this.showMoreOptions = true;
    this.discountOptions = true;
  }
  showDetails(index) {
    this.listOfCarSearch[index].isCollapse = false;


  }
  cancelConnect(index) {

    console.log(index);
    this.listOfCarSearch[index].isCollapse = true;
  }

  openPaypal(id, dealer) {
    this.userService.recentSearch(id).subscribe((data) => {
      this.loader.stop();
      this.carId = id;
      this.dealerId = dealer;
      this.modal.open();
      console.log(data);
      this._store.dispatch({ type: 'CONNECT_CAR_SUCCESS', payload: data.data });
    }, (error) => {
      this._store.dispatch({ type: 'CONNECT_CAR_SUCCESS' });
      console.log(error);
      this.loader.stop();
      let err = JSON.parse(error._body);
      console.log(err);

    })
  }

  getSearchData() {

    this.route.params.subscribe(params => {
      if (params['brandName']) {
        this.searchQuery.brand = params['brandName']
        console.log("Brand Name", this.searchQuery)
        this.getSearchCars();
      }
      else if (params['cacheID']) {
        this.cacheID = params['cacheID']
        console.log(this.cacheID)
        this.userService.getQuerySession(this.cacheID).subscribe((data) => {
          this.loader.stop();
          this.searchQuery = data.data;
          console.log("Search Data", this.searchQuery);
          this.getSearchCars();

        }, (error) => {
          this.loader.stop();
          console.log(error)
        })



      }
    })

  }

  getSearchCars() {
    console.log("Serach Query", this.searchQuery);
    this.userService.searchCar(this.searchQuery).subscribe((_data) => {
      //console.log("search car", _data);

      this.loader.stop();
      this.totalServepage = _data.totalPages;
      this.serverCount = _data.count;
      this.listOfCarSearch = _data.data
      this.totalRecentSearch = this.listOfCarSearch.length;
      for (let i = 1; i <= _data.totalPages; i++) {
        let page = { page: i }
        this.totalPages.push(page);
      }
      for (let i = 0; i < this.listOfCarSearch.length; i++) {
        this.listOfCarSearch[i].isCollapse = true;
      }
      this.isDataLoaded = true;
      this._store.dispatch({ type: 'SEARCH-CAR_SUCCESS', payload: _data });



    }, (error) => {
      this._store.dispatch({ type: 'SEARCH-CAR_FAILD' });
      console.log(error);
      let err = JSON.parse(error._body);
      console.log(err);
      this.loader.stop();

    })

  }

  getBrandNames() {
    this.userService.getBrand().subscribe((data) => {
      this.loader.stop();
      this.brandsList = data.data;
      this.brandsList = this.completerService.local(this.brandsList, 'make', 'make')
      console.log(this.brandsList);
      this.isDataLoaded = true;
      this._store.dispatch({ type: 'GET_BRAND_NAME_SUCCESS', payload: this.brandsList });

    }, (error) => {
      this._store.dispatch({ type: 'GET_BRAND_NAME_FAILD' });
      this.loader.stop();
      console.log(error);
    })
  }

  selectedBrand() {
    this.userService.getCarList(this.searchQuery.brand).subscribe((data) => {
      this.loader.stop();
      this.modelList = data.data;
      this.modelList = this.completerService.local(this.modelList, 'model', 'model')
      this.searchQuery.model = "";
      console.log(this.modelList);
      this.isDataLoaded = true;
      this._store.dispatch({ type: 'GET_BRAND_MODEL_SUCCESS', payload: this.modelList });

    }, (error) => {
      this._store.dispatch({ type: 'GET_BRAND_MODEL_FAILD' });
      this.loader.stop();
      console.log(error);
    })
  }
  selectedModel() {
    this.userService.getModelYearsList(this.searchQuery.model).subscribe((data) => {
      this.loader.stop();
      this.modelYears = data.data;
      console.log(this.modelYears)
      this._store.dispatch({ type: 'GET_BRAND_YEAR_SUCCESS', payload: this.modelYears });

    }, (error) => {
      this._store.dispatch({ type: 'GET_BRAND_YEAR_FAILD' });
      this.loader.stop();
      console.log(error);
    })
  }
  carSearchUpdate() {
    let equips = []
    let features = []
    let discounts = []
    for (let i = 0; i < this.carEquipGourp.length; i++) {
      if (this.carEquipGourp[i].status) {
        let newItem = this.carEquipGourp[i].name;
        equips.push(newItem);
      }
    }

    for (let i = 0; i < this.carFeatures.length; i++) {
      if (this.carFeatures[i].status) {
        let newItem = this.carFeatures[i].name;
        features.push(newItem);
      }
    }
    for (let i = 0; i < this.carDiscounts.length; i++) {
      if (this.carDiscounts[i].status) {
        let newItem = this.carDiscounts[i].name;
        discounts.push(newItem);
      }
    }
    if (features.length !== 0) {
      this.searchQuery.features = features;
    }
    if (equips.length !== 0) {
      this.searchQuery.equipments = equips;
    }
    if (discounts.length !== 0) {
      this.searchQuery.discounts = discounts;
    }
    console.log(this.searchQuery);
    this.userService.searchCarUpdate(this.searchQuery).subscribe((data) => {
      this.loader.stop();
      this.totalServepage = data.totalPages;
      this.serverCount = data.count;

      this.listOfCarSearch = data.data
      this.totalRecentSearch = this.listOfCarSearch.length;
      this.totalPages = [];
      for (let i = 1; i <= data.totalPages; i++) {
        let page = { page: i }
        this.totalPages.push(page);
      }
      for (let i = 0; i < this.listOfCarSearch.length; i++) {
        this.listOfCarSearch[i].isCollapse = true;
      }

      console.log(this.totalRecentSearch);
      console.log(data)
      console.log('TotalPages', this.totalPages)

      this._store.dispatch({ type: 'SEARCH-CAR_UPDATE_SUCCESS', payload: data.data });

    }, (error) => {
      this._store.dispatch({ type: 'SEARCH-CAR_UPDATE_FAILD' });
      console.log(error);
      let err = JSON.parse(error._body);
      console.log(err);
      this.loader.stop();

    })

  }
  isEmailValid() {
    var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
    if (EMAIL_REGEXP.test(this.connectFormData.email)) {
      this.emailError = false;
      return true;
    }
    else {
      this.emailError = true;
      return false;
    }


  }
  isEmailRequestValid() {
    var EMAIL_REGEXP = new RegExp('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$', 'i');
    if (EMAIL_REGEXP.test(this.requestModalForm.email)) {
      return true;
    }
    else {
      return false;
    }


  }
  isCardNumValid() {

    var card_type = this.connectFormData.card;
    var isVisaValid = /^(?:4[0-9]{12}(?:[0-9]{3})?)$/.test(this.connectFormData.card_number);
    var isMasterValid = /^(?:5[1-5][0-9]{14})$/.test(this.connectFormData.card_number);


    if (card_type == "Master") {
      if (isMasterValid) {
        this.cardNumError = false;
        return true
      }
      else {
        this.cardNumError = true;
        return false
      }

    }
    if (card_type == "Visa") {
      if (isVisaValid) {
        this.cardNumError = false;
        return true;
      }
      else {
        this.cardNumError = true;
        return false;
      }

    }


  }
  isNameValid() {
    let NAME_REGEXP = new RegExp(/^[a-z]([-']?[a-z]+)*( [a-z]([-']?[a-z]+)*)+$/);
    if (NAME_REGEXP.test(this.connectFormData.name)) {
      this.NameError = false;
      return true;
    }
    else {
      this.NameError = true;
      return false;
    }
  }

  isPhoneNumValid() {
    let isValid = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/.test(this.connectFormData.phone);
    if (isValid) {
      this.phoneError = false;
      return true;

    }
    else {
      this.phoneError = true;
      return false;
    }
  }
  isCVCValid() {
    let isValid = /^[1-9][0-9]{2}$/.test(this.connectFormData.pin_code);
    if (isValid) {

      this.cvcError = false;
      return true;

    }
    else {
      this.cvcError = true;
      return false;
    }
  }

  getFeatures() {

    this.userService.getFeaturesList().subscribe((data) => {
      this.loader.stop();
      this.carFeatures = data.data;
      this._store.dispatch({ type: 'GET_FEATURES_SUCCESS', payload: data.data });


      console.log(this.searchQuery.features);
      console.log(this.carFeatures);

      for (let i = 0; i < this.carFeatures.length; i++) {
        if (this.searchQuery.features[i] === this.carFeatures[i].name) {

          this.carFeatures[i].status = true;
          console.log(this.carFeatures[i].name + "true")

        }
      }
      console.log(this.carFeatures);

    }, (error) => {
      console.log(error);
      this._store.dispatch({ type: 'GET_FEATURES_FAILD' });
      let err = JSON.parse(error._body);
      console.log(err);
      this.loader.stop();

    })

  }


  getEquipments() {

    this.userService.getEquipmentsList().subscribe((data) => {
      this.loader.stop();
      this.carEquipGourp = data.data;
      console.log(data);

      for (let i = 0; i < this.carEquipGourp.length; i++) {
        if (this.searchQuery.equipments[i] === this.carEquipGourp[i].name) {

          this.carEquipGourp[i].status = true;
        }

      }

      this._store.dispatch({ type: 'GET_EQUIPMENTS_SUCCESS', payload: data.data });

    }, (error) => {
      this._store.dispatch({ type: 'GET_EQUIPMENTS_FAILD' });
      console.log(error);
      let err = JSON.parse(error._body);
      console.log(err);
      this.loader.stop();

    })

  }

  getDiscounts() {

    this.userService.getDiscountsList().subscribe((data) => {
      this.loader.stop();
      this.carDiscounts = data.data;
      console.log(data);
      this._store.dispatch({ type: 'GET_DISCOUNTS_SUCCESS', payload: data.data });

    }, (error) => {
      this._store.dispatch({ type: 'GET_DISCOUNTS_FAILD' });
      console.log(error);
      let err = JSON.parse(error._body);
      console.log(err);
      this.loader.stop();

    })

  }




  getPage(pageNum) {
    this.currentPage = pageNum;
    this.userService.getMyRecentCarsPage(pageNum).subscribe((data) => {
      this.loader.stop();
      this.listOfCarSearch = data.data;
      this.totalRecentSearch = this.listOfCarSearch.length;
      for (let i = 0; i < this.listOfCarSearch.length; i++) {
        this.listOfCarSearch[i].isCollapse = true;
      }
      console.log(data)
      this._store.dispatch({ type: 'GET_PAGE', payload: data });
    }, (error) => {
      this.loader.stop()
      console.log(error);

    })
  }
  nextPage() {
    this.currentPage += 1
    this.userService.getMyRecentCarsPage(this.currentPage).subscribe((data) => {
      this.loader.stop();
      this.listOfCarSearch = data.data;
      this.totalRecentSearch = this.listOfCarSearch.length;
      for (let i = 0; i < this.listOfCarSearch.length; i++) {
        this.listOfCarSearch[i].isCollapse = true;
      }
      console.log(data)
      this._store.dispatch({ type: 'NEXT_PAGE', payload: data });
    }, (error) => {
      this.loader.stop()
      console.log(error);

    })
  }
  previousPage() {
    this.currentPage -= 1
    this.userService.getMyRecentCarsPage(this.currentPage).subscribe((data) => {
      this.loader.stop();
      this.listOfCarSearch = data.data;
      this.totalRecentSearch = this.listOfCarSearch.length;
      for (let i = 0; i < this.listOfCarSearch.length; i++) {
        this.listOfCarSearch[i].isCollapse = true;
      }
      console.log(data)
      this._store.dispatch({ type: 'PREVIOUS_PAGE', payload: data });      
    }, (error) => {
      this.loader.stop()
      console.log(error);

    })
  }
  active(page) {
    if (this.currentPage === page) {
      return true;
    }
  }

  reqOfferModal() {
    if (localStorage.getItem('searcherUserID')) {
      this.SeacherData = localStorage.getItem('SearcherUser');
      this.SeacherData = JSON.parse(this.SeacherData)
      let name = this.SeacherData.firstName + " " + this.SeacherData.lastName

      this.userService.userSubcribes(this.requestModalForm.email, name).subscribe((data) => {
        this.loader.stop();
        this.reqModal.close().then(() => {
          this.successRequest.open();
        });
        console.log(data);
        this.requestModalForm = {};
      }, (error) => {
        console.log(error);
        this.loader.stop();
        let err = JSON.parse(error._body);
        if (err.error.code === 11000) {
          this.reqModal.close().then(() => {
            this.alreadySubscribeModal.open();
          });
        }
      })

    }
    else {

      this.userService.userSubcribes(this.requestModalForm.email, this.requetuserName).subscribe((data) => {
        this.loader.stop();
        this.reqModal.close().then(() => {
          this.successRequest.open();
        });
        console.log(data);
        this.requestModalForm = {};
      }, (error) => {
        console.log(error);
        this.loader.stop();
        let err = JSON.parse(error._body);
        if (err.error.code === 11000) {
          this.reqModal.close().then(() => {
            this.alreadySubscribeModal.open();
          });
        }
      })

    }

  }

  getOnTheTop() {
    window.scrollTo(0, 0);
  }

  ngOnInit() {
    this.getOnTheTop();
    this.getSearchData();
    this.getBrandNames();
    this.getFeatures();
    this.getDiscounts();
    this.getEquipments();
  }

  ngOnDestroy() {
  }

}

