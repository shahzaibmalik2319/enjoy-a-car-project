import { Component, OnInit } from '@angular/core';
import { LoaderService } from './../../Services/loader.service';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  public active: boolean;
  public constructor(loader: LoaderService) {
    loader.status.subscribe((status: boolean) => {
      this.active = status;
    });
  }

  ngOnInit() {
  }

}
