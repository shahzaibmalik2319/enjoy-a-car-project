import { login,signUp } from './authInterface';

export interface AppStore {
    login: login[];
    signup: signUp[];
}