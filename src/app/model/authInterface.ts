export interface login {

  email: string;
  password: string;
  role:string;
};
export interface signUp {
  firstName: string,
  lastName: string,
  email: string,
  dob: string,
  age: number,
  gender: string,
  username: string,
  password: string,
  role: string
};

export interface forget {

  email: string;
}
export interface resetPass {

  email: string;
}

export interface resetPass {

  email: string;
}
export interface tokenConfirmation {

  email: string;
}