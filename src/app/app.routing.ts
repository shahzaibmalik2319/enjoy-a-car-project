import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { FindNewCarComponent } from './components/find-new-car/find-new-car.component'
import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { ForgetpasswordComponent } from './components/forgetpassword/forgetpassword.component';
import { RedirectingComponent } from './components/redirecting/redirecting.component';
import { ResetPasswordComponent } from './components/reset-password/reset-password.component';
import { SearchCarComponent } from './components/search-car/search-car.component';
import { NotificationsComponent } from './components/notifications/notifications.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';



//Auth Graud
import { AuthGraudService } from './Services/auth-graud.service';

// Routes
export const routes: Routes= [
    {path:'', component:FindNewCarComponent},
    {path: 'login', component: LoginComponent},
    {path: 'signup', component: SignupComponent},
    {path: 'forgetpassword', component: ForgetpasswordComponent},
    {path: 'confirmation', component: RedirectingComponent},
    {path: 'reset-password/:token', component: ResetPasswordComponent},
    {path: 'confirmation/:token', component: RedirectingComponent},
    {path: 'search-car', component: SearchCarComponent},
    {path: 'notification', component: NotificationsComponent, canActivate:[AuthGraudService]},
    {path: 'policy', component: PrivacyPolicyComponent},
    
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
